<?php
	if (session_status() == PHP_SESSION_NONE) session_start();
	require_once "../config/classes.php";
	verif_Session();
	$adm = '';
	if (isset($_SESSION['id'])) {
		$adm = $Admin->getAdminById($_SESSION['id']);
	}
?>
<!DOCTYPE html>
	<html>
	<head id="le_head">
		<title>Admin - GeoSeine</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
	  	<meta name="description" content="">
	  	<link rel="shortcut icon" href="../images/gs_icon.png" type="image/x-icon">
	  	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
	  	<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	  	<style type="text/css">
	  		#les_lieux, #les_cartes{
	  			height: 140px;
	  			overflow-y: auto;
	  			overflow-x: hidden;
	  		}
	  		.card-header{
	  			text-align: center;
	  		}
	  		#img-a{
	  			z-index: 1; 
	  			border: 2px solid blue; 
	  			position: absolute;
	  			cursor: crosshair;
	  		}
	  		#marker{
	  			z-index: 100; 
	  			position: absolute;
	  		}

	  	</style>
	</head>
	<body>
		<!-- NAVBAR-->
		<nav class="navbar sticky-top navbar-expand-lg py-3 navbar-dark bg-info shadow-sm">
			<div class="container">
				<a href="../" class="navbar-brand">
				<!-- Logo Image -->
				<img src="../../local/cache-vignettes/L224xH69/siteon0-4af59.png?1581935513" width="45" alt="" class="d-inline-block align-middle mr-2">
				<!-- Logo Text -->
				<span class="font-weight-bold">GEOSEINE ADMIN-SEINE</span>
				</a>

				<button type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler"><span class="navbar-toggler-icon"></span></button>

				<div id="navbarSupportedContent" class="collapse navbar-collapse text-center">
					<ul class="navbar-nav ml-auto">
						<?php if(isset($_SESSION['id'])){ ?> 
							<li class="nav-item"><a href="../" class="nav-link" ><i class="fa fa-map-o"></i> Cartes</a></li>&nbsp;
							<li class="active nav-item"><a href="../lieu" class="nav-link" ><i class="fa fa-map-pin"></i> Lieux</a></li>&nbsp;&nbsp;&nbsp;
							<li class="nav-item"><a href="#" class="nav-link" ><i class="fa fa-user"></i> <?php echo $adm['username']; ?></a></li>
							<li class="nav-item"><a href="../config/deconnexion.php" class="nav-link"><i class="fa fa-sign-out"></i> Déconnexion</a></li>
						<?php } ?>
					</ul>
				</div>
			</div>
		</nav>


		<section class="pt-3 text-center text-info">
			<h1><i class="fa fa-cogs"></i> Lieux</h1>
		</section>


		<section class="py-3">
			<div class="container-fluid">
				<div class="row  mt-2">
					<div class="col-3 h-100">
						<div class="h-75">
							<div class="card">
								<div class="card-header">
								    <strong><i class="fa fa-map-pin"></i> Liste des lieux</strong>
								</div>
								<div id="les_lieux" class="list-group list-group-flush">
									<?php
										if($NbLieux<1){
											echo "<small class='text-center'> aucun lieu ajouté </small>";
										}
									?>
									<?php foreach ($lesLieux as $_lieux): ?>
										<li class="list-group-item list-group-item-action list-group-item-light"><button class="px-2 py-0 float-right btn btn-danger" onclick='supprimerLieu("<?php echo $_lieux['id_lieux'];?>","<?php echo $_lieux['nom_lieux'];?>");'><i class="fa fa-trash-o"></i></button><?php echo $_lieux['nom_lieux'];?>  (<?php echo $_lieux['x'];?>; <?php echo $_lieux['y'];?>) </li>
									<?php endforeach ?>
								</div>
							</div>
							<div class="card">
								<div class="card-header">
								    <strong><i class="fa fa-map-o"></i> Liste des cartes</strong>
								</div>
								<div id="les_cartes" class="list-group list-group-flush">
									<?php foreach ($lesCartes as $cartes): ?>
										<a id="carte<?php echo $cartes['id_carte']; ?>" href="?Carte_Id=<?php echo $cartes['id_carte']; ?>" class="lescartes list-group-item list-group-item-action list-group-item-light"><?php echo $cartes['nom_carte']; ?></a>
									<?php endforeach ?>
									<?php if (isset($_GET['Carte_Id']) && $_GET['Carte_Id']!="") {?>
										<input type="hidden" id="val_carte_id" value="<?php echo $_GET['Carte_Id']; ?>">
									<?php }else{ ?>
										<input type="hidden" id="val_carte_id" value="0">
									<?php } ?>
								</div>
							</div>
						</div>
						<div class="h-25 m-3 ">
							<div class="card border-info bg-light">
								<small class="text-center text-info"><i class="fa fa-info-circle"></i> Sélectionner une carte pour ajouter un lieu</small>
								<form id="formulaire_lieux" class="p-3">
									<div class="form-group col">
										<label for="nom_lieu">Nom :</label>
										<input type="text" class="form-control" placeholder="Nom du lieu" id="nom_lieu" required>
									</div>
									<div class="row">
										<div class="form-group col">
											<label for="x">X :</label>
											<input type="number" min="0" class="form-control" placeholder="position X" id="posx" required>
										</div>
										<div class="form-group col">
											<label for="y">Y :</label>
											<input type="number" min="0" class="form-control" placeholder="position Y" id="posy" required>
										</div>
									</div>
									<div class="row justify-content-md-center" >
										<button type="submit" class="btn btn-primary ">Ajouter un lieu</button>
									</div>
								</form>
							</div>
						</div>
					</div>
					<div class="col-9">
						<div class="row">
			                <div class="col p-0 mr-5">
			                    <img src="http://moussa.ascmtsahara.fr/Carte%20axe%20seine-01.jpg" id="img-a" class="img-fluid">
			                    <i id="marker" class="h3 text-danger fa fa-circle" hidden></i>
			                </div>
			            </div>
					</div>
				</div>	
	  		</div>
		</section>


		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	  	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
	  	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
		<script type="text/javascript">
			$(function(){

				var 
				arr_x, arr_y, pos_x, pos_y,
				marker = $('#marker'),
				input_x = $('#posx'),
				input_y = $('#posy'),
				input_nomLieu = $('#nom_lieu');

				/*Clic sur l'image*/
				$('#img-a').on('click', function(event)
				{

					/*Affichage du marker*/
					$('#marker').removeAttr('hidden');

					/*Pour réperer les coordonnées de l'image*/
				  	var x = event.offsetX,
				  	y = event.offsetY;

				  	/*Arrondir les valeurs*/
				  	arr_x = (x/8.09);
				  	arr_y = (y/8.2);
				  	pos_x = arr_x.toFixed(0);
				  	pos_y = arr_y.toFixed(0);

				  	/*Modifications des valeurs des inputs X et Y*/
				  	input_x.val(pos_x);
				  	input_y.val(pos_y);

				  	/*Position du marker*/
				  	marker.animate({
				  		left: x-3,
				  		top: y-3
				  	});


				  	/*les valeurs des champs du formulaire*/
				  	var 
				  	x_form = x,
				  	y_form = y,
				  	val_x = $('#posx').val(),
					val_y = $('#posy').val();

					/*Modification du champ X */
					$('#posx').on('change',function(){
						var diff_x;
						if ($(this).val()>val_x) {
							diff_x = $(this).val()-val_x;
							console.log(diff_x)
							marker.animate({
						  		left: "+="+(8.09*diff_x),
						  	});
							val_x = $(this).val();
						}
						if ($(this).val()<val_x) {
							diff_x = val_x-$(this).val();
							marker.animate({
						  		left: "-="+(8.09*diff_x),
						  	});
							val_x = $(this).val();
						}
						console.log("X : "+val_x)
					})

					/*Modification du champ Y */
					$('#posy').on('change',function(){
						var diff_y;
						if ($(this).val()>val_y) {
							diff_y = $(this).val()-val_y;
							console.log(diff_y)
							marker.animate({
						  		top: "+="+(8.2*diff_y),
						  	});
							val_y = $(this).val();
						}
						if ($(this).val()<val_y) {
							diff_y = val_y-$(this).val();
							marker.animate({
						  		top: "-="+(8.2*diff_y),
						  	});
							val_y = $(this).val();
						}
						console.log("Y : "+val_y)
					})
				});
				
				/*Envoie du formulaire*/
				$('#formulaire_lieux').on('submit',function(event){

					event.preventDefault();
					<?php if (isset($_GET['Carte_Id']) && $_GET['Carte_Id']!="") {?>
						$.get(
							'../config/ajout_lieu.php',
							{
								ajouter : "",
								nom_lieu : input_nomLieu.val(),
								posX : input_x.val(),
								posY : input_y.val(),
								idCarte : "<?php echo $_GET['Carte_Id']; ?>"

							},
							function(data){
								swal("Ajouté !", "Le lieu a bien été ajouter !", "success");
								afficherLieux();
							}
						)
					<?php }else{ ?>
						swal("Oops...", "Veuillez sélectionner une carte !", "error");
					<?php } ?>
					$(this)[0].reset();
					$('#marker').attr('hidden','hidden');
				})


				/*Ajout un élément active sur une carte séléctionnée*/
				if ($('#val_carte_id').val()!=0) {

					var id_carte = $('#val_carte_id').val();

			    	$("#carte"+id_carte).addClass("active")
				}

				function afficherLieux(){
					$.get(
						'../config/lieux.php',
						function(data){
							$('#les_lieux').empty().append(data);
						}
					)
				}

				/*Supprimer un lieu*/
				function supprimerLieu(id_lieux, nom_lieux){
					var confirmation = confirm("Voulez-vous supprimer "+nom_lieux+" ?");
					if (confirmation == true) {
						$.get(
							'../config/supprimer.php',
							{
								id_lieu : id_lieux
							},
							function(data){
								afficherLieux();
							}
						)
					}else{
					    
					}
				}


			})

		</script>
		<script type="text/javascript">
			function afficherLieux(){
				$.get(
					'../config/lieux.php',
					function(data){
						$('#les_lieux').empty().append(data);
					}
				)
			}
			/*Supprimer un lieu*/
			function supprimerLieu(id_lieux, nom_lieux){
				var confirmation = confirm("Voulez-vous supprimer "+nom_lieux+" ?");
				if (confirmation == true) {
					$.get(
						'../config/supprimer.php',
						{
							id_lieu : id_lieux
						},
						function(data){
							afficherLieux();
						}
					)
				}else{
				    
				}
			}
		</script>
	</body>
</html>