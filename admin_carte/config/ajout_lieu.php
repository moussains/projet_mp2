<?php
	require_once "classes.php";

	if (isset($_GET['ajouter'])) {

		/*Insertion de la position*/
		$Position->setPosition($_GET['posX'],$_GET['posY']);

		/*Récupération de la dernière insertion d'une position*/
		$dernierPos = $Position->getPositionDerniere()['id_position'];

		/*Insertion du lieu*/
		$Lieux->setLieux($_GET['nom_lieu'],$_GET['idCarte'],$dernierPos);
	}