<?php
	require_once "classes.php";

	if($NbLieux<1){
		echo "<small class='text-center'> aucun lieu ajouté </small>";
	}

 	foreach ($lesLieux as $_lieux): ?>
		<li class="list-group-item list-group-item-action list-group-item-light"><button class="px-2 py-0 float-right btn btn-danger" onclick='supprimerLieu("<?php echo $_lieux['id_lieux'];?>","<?php echo $_lieux['nom_lieux'];?>");'><i class="fa fa-trash-o"></i></button><?php echo $_lieux['nom_lieux'];?></li>
<?php endforeach ?>