<?php 
require_once "connexionPDO.php";

/********** CLASSE lieux *************/
Class Lieux{

	/*Tous les lieux*/
	public function getLieux(){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare("SELECT * FROM lieux L INNER JOIN position P ON L.id_position = P.id_position ORDER BY id_lieux DESC");
		$req->execute(); 
		$donnees = $req->fetchAll();
		return $donnees;
	}

	/*NB les lieux*/
	public function getNbLieux(){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare("SELECT * FROM lieux");
		$req->execute(); 
		$nb = $req->rowCount();
		return $nb;
	}

	/*Insertion dans la base de données*/
	public function setLieux($nom_lieux,$id_carte,$id_position){

		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$insert_Lieux = $connect->prepare("INSERT INTO `lieux`(`nom_lieux`, `id_carte`, `id_position`) VALUES (:nom_lieux,:id_carte,:id_position)");
		$insert_Lieux->bindParam(':nom_lieux', $nom_lieux);
		$insert_Lieux->bindParam(':id_carte', $id_carte);
		$insert_Lieux->bindParam(':id_position', $id_position);
		$insert_Lieux->execute();
	}

	/*Supprimer un lieux*/
	public function deleteLieux($id_lieux){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare("DELETE FROM `lieux` WHERE id_lieux = ".$id_lieux);
		$req->execute(); 
	}
}
$Lieux = new Lieux();
$lesLieux = $Lieux->getLieux();
$NbLieux = $Lieux->getNbLieux();


/********** CLASSE position *************/
Class Position{
	/*Tous  les positions*/
	public function getPosition(){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare("SELECT * FROM position");
		$req->execute(); 
		$donnees = $req->fetchAll();
		return $donnees;
	}

	/*Récupère le dernier position*/
	public function getPositionDerniere(){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare("SELECT * FROM position ORDER BY id_position DESC LIMIT 1");
		$req->execute(); 
		$donnees = $req->fetch();
		return $donnees;
	}

	/*Insertion dans la base de données*/
	public function setPosition($x,$y){

		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$insert_Position = $connect->prepare("INSERT INTO `position`(`x`, `y`) VALUES (:x,:y)");
		$insert_Position->bindParam(':x', $x);
		$insert_Position->bindParam(':y', $y);
		$insert_Position->execute();
	}
}
$Position = new Position();


/********** CLASSE carte *************/
Class Carte{
	/*Tous  les cartes*/
	public function getCarte(){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare("SELECT * FROM carte");
		$req->execute(); 
		$donnees = $req->fetchAll();
		return $donnees;
	}


	/*une Carte*/
	public function getUneCarte($carte){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare("SELECT * FROM carte WHERE id_carte = ".$carte);
		$req->execute(); 
		$donnees = $req->fetch();
		return $donnees;
	}

	/*Insertion dans la base de données*/
	public function setCarte($nom_carte,$emplacement_carte){

		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$insert_Position = $connect->prepare("INSERT INTO `carte`(`nom_carte`, `emplacement_carte`) VALUES (:nom_carte,:emplacement_carte)");
		$insert_Position->bindParam(':nom_carte', $nom_carte);
		$insert_Position->bindParam(':emplacement_carte', $emplacement_carte);
		$insert_Position->execute();
	}
}
$Carte = new Carte();
$lesCartes = $Carte->getCarte();


/********** CLASSE admin *************/
Class Admin{
	/*Admin par Id*/
	public function getAdminById($id_admin){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare("SELECT * FROM admin WHERE id_admin = '".$id_admin."'");
		$req->execute(); 
		$donnees = $req->fetch();
		return $donnees;
	}

	/*Admin par username*/
	public function getAdminByUsername($username){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare("SELECT * FROM admin WHERE username = '".$username."'");
		$req->execute(); 
		$donnees = $req->fetch();
		return $donnees;
	}
}
$Admin = new Admin();




/************FONCTIONS QUI VÉRIFIENT LES SESSIONS*********/

function verif_Session(){
	if (!isset($_SESSION['id'])) {
		header('Location: ../');
		exit;
	}
}


/* ************* Connection admin ****************** */

$redirection = "";
$message = "";

if (isset($_POST['connecter'])) {
	$username = $_POST['username'];
	$mdp = md5($_POST['mdp']);
	$adm = $Admin->getAdminByUsername($username);

	if (isset($adm['username']) && $adm['username'] ==$username) {
		if ($adm['mdp'] == $mdp) {
			$message .= '<div class="p-3 mb-2 bg-success text-white">Vous êtes connecté !</div>';
			$redirection .='<meta http-equiv="refresh" content="1;URL=./">';
			session_start();
			$_SESSION['id'] = $adm['id_admin'];
		}
		else{
			$message .= '<div class="p-3 mb-2 bg-warning text-white">Votre mot de passe n\'est pas correcte !</div>';
		}
	}else{
		$message .= '<div class="p-3 mb-2 bg-danger text-white">Votre compte n\'existe pas dans notre base de données ! Veuillez contacter votre administrateur !</div>';
	}
}
