<?php
	require_once "config/classes.php";
	if (isset($_SESSION['id'])) {
		$admin = $Admin->getAdminById($_SESSION['id']);
	}
	
?>
<!DOCTYPE html>
	<html>
	<head id="le_head">
		<title>Tchat - JS</title>
		<meta charset="utf-8">
		<?php echo $redirection; ?>
		<meta name="viewport" content="width=device-width, initial-scale=1">
	  	<meta name="description" content="">
	  	<link rel="shortcut icon" href="images/gs_icon.png" type="image/x-icon">
	  	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
	  	<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	  	<style type="text/css">
	  		#liste_tchatter{
	  			height: 200px;
	  			overflow-y: auto;
	  			overflow-x: hidden;
	  		}
	  	</style>
	  	<?php 
	  		if (isset($_SESSION['id'])) {
				header("Location:carte/");
			}
	  	?>
	</head>
	<body>
		<!-- NAVBAR-->
		<nav class="navbar sticky-top navbar-expand-lg py-3 navbar-dark bg-info shadow-sm">
			<div class="container">
				<a href="./" class="navbar-brand">
				<!-- Logo Image -->
				<img src="../local/cache-vignettes/L224xH69/siteon0-4af59.png?1581935513" width="45" alt="" class="d-inline-block align-middle mr-2">
				<!-- Logo Text -->
				<span class="font-weight-bold">GEOSEINE</span>
				</a>

			</div>
		</nav>


		<section class="pt-5 text-center">
			<h1>Connexion au panel admin</h1>
			<?php echo $message; ?>
		</section>


		<section class="py-3">
			<div class="container">
				<div class="row justify-content-md-center mt-5">
					<div class="card border-info w-50 p-3 bg-light">
						<form action="login.php" method="post">
						  <div class="form-group">
						    <label for="username">Nom d'utilisateur :</label>
						    <input type="text" class="form-control" placeholder="Username" name="username" id="username" required>
						  </div>
						  <div class="form-group">
						    <label for="mdp">Mot de passe :</label>
						    <input type="password" class="form-control" placeholder="Votre mot de passe" name="mdp" id="mdp" required>
						  </div>
						  <div class="row justify-content-md-center" >
						  		<button type="submit" class="btn btn-primary " name="connecter">Se connecter</button>
						  </div>
						</form>
					</div>
				</div>	
	  		</div>
		</section>
		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
	</body>
</html>