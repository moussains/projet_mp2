<?php
	if (session_status() == PHP_SESSION_NONE) session_start();
	require_once "../config/classes.php";
	verif_Session();
	$adm = '';
	if (isset($_SESSION['id'])) {
		$adm = $Admin->getAdminById($_SESSION['id']);
	}
?>
<!DOCTYPE html>
	<html>
	<head id="le_head">
		<title>Admin - GeoSeine</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
	  	<meta name="description" content="">
	  	<link rel="shortcut icon" href="../images/gs_icon.png" type="image/x-icon">
	  	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
	  	<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	  	<style type="text/css">
	  		.lescartes{
	  			border: 2px solid blue; 
	  		}
	  	</style>
	</head>
	<body>
		<!-- NAVBAR-->
		<nav class="navbar sticky-top navbar-expand-lg py-3 navbar-dark bg-info shadow-sm">
			<div class="container">
				<a href="../" class="navbar-brand">
				<!-- Logo Image -->
				<img src="../../local/cache-vignettes/L224xH69/siteon0-4af59.png?1581935513" width="45" alt="" class="d-inline-block align-middle mr-2">
				<!-- Logo Text -->
				<span class="font-weight-bold">GEOSEINE ADMIN-SEINE</span>
				</a>

				<button type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler"><span class="navbar-toggler-icon"></span></button>

				<div id="navbarSupportedContent" class="collapse navbar-collapse text-center">
					<ul class="navbar-nav ml-auto">
						<?php if(isset($_SESSION['id'])){ ?> 
							<li class="active nav-item"><a href="./" class="nav-link" ><i class="fa fa-map-o"></i> Cartes</a></li>&nbsp;
							<li class="nav-item"><a href="../lieu" class="nav-link" ><i class="fa fa-map-pin"></i> Lieux</a></li>&nbsp;&nbsp;&nbsp;
							<li class="nav-item"><a href="#" class="nav-link" ><i class="fa fa-user"></i> <?php echo $adm['username']; ?></a></li>
							<li class="nav-item"><a href="../config/deconnexion.php" class="nav-link"><i class="fa fa-sign-out"></i> Déconnexion</a></li>
						<?php } ?>
					</ul>
				</div>
			</div>
		</nav>


		<section class="pt-3 text-center text-info">
			<h1><i class="fa fa-cogs"></i> Cartes</h1>
		</section>


		<section class="py-3">
			<div class="container">
			  	<button class="btn btn-primary" disabled><i class="fa fa-plus"></i> Ajouter une nouvelle carte</button>
				<ul class="list-group mt-3">
					<?php foreach ($lesCartes as $carte): ?>
						<li class="list-group-item list-group-item-info">
							<span class="float-right">
								<button class="px-2 py-0  btn btn-primary" title="Voir" disabled><i class="fa fa-eye"></i></button>
								<button class="px-2 py-0  btn btn-danger" title="Supprimer ?" disabled><i class="fa fa-trash-o"></i></button>
							</span>
							
							<img src="<?php echo $carte['nom_fichier']; ?>" class="img-fluid mr-3 lescartes" style="height: 100px;">
							<strong class="h3"><?php echo $carte['nom_carte']; ?></strong>
						</li>
					<?php endforeach ?>
				</ul>
			</div>
		</section>


		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	  	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
	  	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	</body>
</html>