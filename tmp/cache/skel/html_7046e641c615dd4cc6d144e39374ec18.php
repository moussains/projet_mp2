<?php

/*
 * Squelette : plugins/auto/scssphp/v2.2.0/prive/bouton/calculer_css.html
 * Date :      Fri, 20 Dec 2019 12:26:28 GMT
 * Compile :   Thu, 30 Jan 2020 22:35:22 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette plugins/auto/scssphp/v2.2.0/prive/bouton/calculer_css.html
// Temps de compilation total: 0.480 ms
//

function html_7046e641c615dd4cc6d144e39374ec18($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'<'.'?php header("X-Spip-Cache: 0"); ?'.'>'.'<'.'?php header("Cache-Control: no-cache, must-revalidate"); ?'.'><'.'?php header("Pragma: no-cache"); ?'.'>' .
(($t1 = strval(interdire_scripts(((filtre_info_plugin_dist("minibando", "est_actif")) ?'' :' '))))!=='' ?
		($t1 . (	'
	' .
	(($t2 = strval(parametre_url(self(),'var_mode','css')))!=='' ?
			('<a href="' . $t2 . (	'" class="spip-admin-boutons" id="scssphp_calculer_css">' .
		_T('scssphp:bouton_calculer_css') .
		'</a>')) :
			'') .
	'
')) :
		'') .
'
');

	return analyse_resultat_skel('html_7046e641c615dd4cc6d144e39374ec18', $Cache, $page, 'plugins/auto/scssphp/v2.2.0/prive/bouton/calculer_css.html');
}
?>