<?php

/*
 * Squelette : squelettes-dist/src/actualites.html
 * Date :      Wed, 22 Jan 2020 21:08:24 GMT
 * Compile :   Thu, 23 Jan 2020 15:26:07 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette squelettes-dist/src/actualites.html
// Temps de compilation total: 1.458 ms
//

function html_87e2f88af8d750eda84414f7cfe1bb36($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

 
<html dir="' .
lang_dir(@$Pile[0]['lang'], 'ltr','rtl') .
'" lang="' .
spip_htmlentities(@$Pile[0]['lang'] ? @$Pile[0]['lang'] : $GLOBALS['spip_lang']) .
'" xmlns="http://www.w3.org/1999/xhtml" xml:lang="' .
spip_htmlentities(@$Pile[0]['lang'] ? @$Pile[0]['lang'] : $GLOBALS['spip_lang']) .
'" class="' .
lang_dir(@$Pile[0]['lang'], 'ltr','rtl') .
(($t1 = strval(spip_htmlentities(@$Pile[0]['lang'] ? @$Pile[0]['lang'] : $GLOBALS['spip_lang'])))!=='' ?
		(' ' . $t1) :
		'') .
' no-js">

   	<head>
      	<script type=\'text/javascript\'>(function(H){H.className=H.className.replace(/\\bno-js\\b/,\'js\')})(document.documentElement);</script>
      	<title>' .
interdire_scripts(textebrut(typo($GLOBALS['meta']['nom_site'], "TYPO", $connect, $Pile[0]))) .
(($t1 = strval(interdire_scripts(textebrut(typo($GLOBALS['meta']['slogan_site'], "TYPO", $connect, $Pile[0])))))!=='' ?
		(' - ' . $t1) :
		'') .
'</title>
      	' .
(($t1 = strval(interdire_scripts(attribut_html(couper(propre($GLOBALS['meta']['descriptif_site'], $connect, $Pile[0]),'150')))))!=='' ?
		('<meta name="description" content="' . $t1 . '" />') :
		'') .
'
      	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
      	<link rel="stylesheet" type="text/css" href="squelettes-dist/css/style-create.css">
      	<style type="text/css">
      		/*La barre de manu GeoSeine, Actualités, Notre Seine, Espace pédagogique*/
      		.list-group-horizontal{width: 100%;}
			.list-group-item{width: 25%;}

			/*Coloration du header*/
			.le_header{background-color: #e9ecef;}

			/*espace entre les deux sections*/
			#slogan, .spip_logo_site{margin-bottom: 0px;}

			/*Alignement du bouton retour*/
			.btn-retour{margin-top: 30px; margin-bottom: 20px;}

			/*Alignement du text pour les catégories de filtres*/
			.card-header{text-align: center;}

			/*Overflow dans les filtres*/
			.card-body{overflow: auto; height: 100px;}

			/*Décoration de la barre de menu si on pointe la souris dessus*/
			.menu-items__lien:hover{ text-decoration: none; background-color: #007bff; color: #fff;}

			/*Espaces sur les côtés */
			.epaces_cotes{ padding: 15px; }

			/*Les boutons de la frise chronologique*/
			.btn-group{ width: 100%; }
			.btn-frise{ width: 14%; background-color: #c0d78f;}
			.btn-frise-dernier{border-radius: 0% 50% 50% 0%; width: 16%; background-color: #c0d78f; }
			.btn-frise, .btn-frise-dernier{border: 1px solid black;}
			/*Leurs décorations*/
			.btn-frise:hover, .btn-frise-dernier:hover, .btn-frise:focus, .btn-frise-dernier:focus{border: 3px solid black; color: #c0d78f; background-color: #fff;}


			/*La photo de la seine*/
			.la_seine{margin-top: 10px; padding: 0;}


			/*Les couleurs des deux blocks*/
			#cote_autre, #cote_seine{background-color: #e9ecef;}

			/*La ligne entre les deux blocks (La seine et cétéégories)*/
			#cote_seine{border-left: 1px solid;}

			/*Longueur et largeur du canvas*/
			#canvas{width:100%; height: 100%; border: 2px dashed black;  padding: 0;}


      	</style>
   	</head>

   	<body onload="afficheCanvas();">

      	<div class="container-fluid">
      		<section>
      			<div class="le_header">
      				' .

'<'.'?php echo recuperer_fond( ' . argumenter_squelette('inclure/header') . ', array(\'home\' => ' . argumenter_squelette('oui') . ',
	\'lang\' => ' . argumenter_squelette($GLOBALS["spip_lang"]) . '), array("compil"=>array(\'squelettes-dist/src/actualites.html\',\'html_87e2f88af8d750eda84414f7cfe1bb36\',\'\',69,$GLOBALS[\'spip_lang\'])), _request("connect"));
?'.'>
      			</div>
      		</section>
      		<section>
      			' .

'<'.'?php echo recuperer_fond( ' . argumenter_squelette('inclure/nav') . ', array_merge('.var_export($Pile[0],1).',array(\'lang\' => ' . argumenter_squelette($GLOBALS["spip_lang"]) . ')), array("compil"=>array(\'squelettes-dist/src/actualites.html\',\'html_87e2f88af8d750eda84414f7cfe1bb36\',\'\',73,$GLOBALS[\'spip_lang\'])), _request("connect"));
?'.'>
      		</section>
         	<div class="container-fluid">
	            <div class="row">

	            	<!--  -->
	               	<div class="col-3" id="cote_autre">
	               		<h2 class="text-center">Catégorie</h2>
						<div class="card">
							<div class="card-header">
							  	<h4>Genres</h4>
							</div>
							<div class="card-body">
								<div class="form-check">
									<input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
									<label class="form-check-label" for="defaultCheck1">
									Default checkbox
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input" type="checkbox" value="" id="defaultCheck2">
									<label class="form-check-label" for="defaultCheck2">
									Default checkbox
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input" type="checkbox" value="" id="defaultCheck3">
									<label class="form-check-label" for="defaultCheck3">
									Default checkbox
									</label>
								</div>
							</div>
							<div class="card-header">
							  	<h4>Strates</h4>
							</div>
							<div class="card-body">
								<div class="form-check">
									<input class="form-check-input" type="checkbox" value="" id="defaultCheck4">
									<label class="form-check-label" for="defaultCheck4">
									Default checkbox
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input" type="checkbox" value="" id="defaultCheck5">
									<label class="form-check-label" for="defaultCheck5">
									Default checkbox
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input" type="checkbox" value="" id="defaultCheck6">
									<label class="form-check-label" for="defaultCheck6">
									Default checkbox
									</label>
								</div>
							</div>
							<div class="card-header">
							  	<h4>Thématiques</h4>
							</div>
							<div class="card-body">
								<div class="form-check">
									<input class="form-check-input" type="checkbox" value="" id="defaultCheck7">
									<label class="form-check-label" for="defaultCheck7">
									Default checkbox
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input" type="checkbox" value="" id="defaultCheck8">
									<label class="form-check-label" for="defaultCheck8">
									Default checkbox
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input" type="checkbox" value="" id="defaultCheck9">
									<label class="form-check-label" for="defaultCheck9">
									Default checkbox
									</label>
								</div>
							</div>
						</div>
						<div class="btn-retour col-md-8 ml-auto mr-auto">
							<a class="btn btn-primary" href="#" role="button">Retour à la playlist</a>
						</div>
	               	</div>

	               	<!--  -->
	               	<div class="col-9" id="cote_seine">
	               		<div class="row epaces_cotes">
	               			<div class="col-sm-10">
	               				<div class="btn-group">
									<button type="button" class="btn btn-frise">ANTIQUITÉ</button>
									<button type="button" class="btn btn-frise">MOYEN ÂGE</button>
									<button type="button" class="btn btn-frise">XVI</button>
									<button type="button" class="btn btn-frise">XVII-XVIII</button>
									<button type="button" class="btn btn-frise">XIX</button>
									<button type="button" class="btn btn-frise">XX</button>
									<button type="button" class="btn btn-frise-dernier">XXI</button>
								</div>
	               			</div>
	               			<div class="col-sm-2">
	               				' .
executer_balise_dynamique('FORMULAIRE_RECHERCHE',
	array(),
	array('squelettes-dist/src/actualites.html','html_87e2f88af8d750eda84414f7cfe1bb36','',172,$GLOBALS['spip_lang'])) .
'
	               			</div>
	               			<div class="col-sm-12 la_seine">

	               				<canvas id="canvas" width="1300" height="600">
	               					<img id="source" src="squelettes-dist/img/maps1.png">
	               				</canvas>
               				</div>
	               		</div>
			               	   	

	               	</div>


	            </div>
         	</div>
      	</div>
    <!--.page-->
  	<!-- Les scripts Canvas -->
    <script type="text/javascript">
		function afficheCanvas() {
		  	var canvas = document.getElementById(\'canvas\');
		  	var ctx = canvas.getContext(\'2d\');
			var image = new Image();
			image = document.getElementById(\'source\');;
			ctx.drawImage(image,0,0,canvas.width, canvas.height);
			ctx.arc(95, 50, 10, 0, 2 * Math.PI);
			ctx.stroke();
		}
    </script>
   	</body>

</html>');

	return analyse_resultat_skel('html_87e2f88af8d750eda84414f7cfe1bb36', $Cache, $page, 'squelettes-dist/src/actualites.html');
}
?>