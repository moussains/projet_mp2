<?php

/*
 * Squelette : ../plugins-dist/medias/prive/squelettes/top/documents.html
 * Date :      Tue, 21 Jan 2020 17:16:46 GMT
 * Compile :   Tue, 21 Jan 2020 17:33:43 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette ../plugins-dist/medias/prive/squelettes/top/documents.html
// Temps de compilation total: 0.146 ms
//

function html_3bb95bf8dbf1a05e6375b7eff2f85f1a($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'<h1 class="grostitre">' .
_T('medias:documents') .
'</h1>
' .
interdire_scripts((is_string('pleine_largeur')?vide($GLOBALS['largeur_ecran']='pleine_largeur'):(isset($GLOBALS['largeur_ecran'])?$GLOBALS['largeur_ecran']:''))));

	return analyse_resultat_skel('html_3bb95bf8dbf1a05e6375b7eff2f85f1a', $Cache, $page, '../plugins-dist/medias/prive/squelettes/top/documents.html');
}
?>