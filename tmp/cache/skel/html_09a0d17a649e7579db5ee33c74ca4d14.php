<?php

/*
 * Squelette : ../prive/formulaires/selecteur/inc-sel-rubriques.html
 * Date :      Sun, 19 Jan 2020 16:26:13 GMT
 * Compile :   Sun, 19 Jan 2020 17:21:05 GMT
 * Boucles :   _sel, _pour
 */ 

function BOUCLE_selhtml_09a0d17a649e7579db5ee33c74ca4d14(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	$in = array();
	if (!(is_array($a = (@$Pile[0]['statut']))))
		$in[]= $a;
	else $in = array_merge($in, $a);
	if (!isset($command['table'])) {
		$command['table'] = 'rubriques';
		$command['id'] = '_sel';
		$command['from'] = array('rubriques' => 'spip_rubriques');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("rubriques.id_rubrique",
		"rubriques.lang",
		"rubriques.titre");
		$command['orderby'] = array();
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array(
			array('=', 'rubriques.id_rubrique', sql_quote(interdire_scripts($Pile[$SP]['valeur']), '', 'bigint(21) NOT NULL AUTO_INCREMENT')), (!(is_array(@$Pile[0]['statut'])?count(@$Pile[0]['statut']):strlen(@$Pile[0]['statut'])) ? '' : ((is_array(@$Pile[0]['statut'])) ? sql_in('rubriques.statut',sql_quote($in)) : 
			array('=', 'rubriques.statut', sql_quote(@$Pile[0]['statut'], '','varchar(10) NOT NULL DEFAULT \'0\'')))));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('../prive/formulaires/selecteur/inc-sel-rubriques.html','html_09a0d17a649e7579db5ee33c74ca4d14','_sel',2,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	lang_select($GLOBALS['spip_lang']);
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		lang_select_public($Pile[$SP]['lang'], '', $Pile[$SP]['titre']);
		$t0 .= (
'
<li class=\'rubrique\'><input type="hidden" name="' .
interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'name', null), 'id_item'),true)) .
'[]" value="rubrique|' .
$Pile[$SP]['id_rubrique'] .
'" />' .
(($t1 = strval(interdire_scripts(((entites_html(sinon(table_valeur(@$Pile[0], (string)'afficher_langue', null), '0'),true)) ?' ' :''))))!=='' ?
		($t1 . (	(($t2 = strval(spip_htmlentities($Pile[$SP]['lang'] ? $Pile[$SP]['lang'] : $GLOBALS['spip_lang'])))!=='' ?
			('&#91;' . $t2 . '&#93;') :
			'') .
	' ')) :
		'') .
interdire_scripts(supprimer_numero(typo($Pile[$SP]['titre']), "TYPO", $connect, $Pile[0])) .
(($t1 = strval(interdire_scripts(((entites_html(sinon(table_valeur(@$Pile[0], (string)'select', null), ''),true)) ?'' :' '))))!=='' ?
		($t1 . (	'
<a href=\'#\' onclick=\'jQuery(this).item_unpick();return false;\'><img src=\'' .
	interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'img_unpick', null),true)) .
	'\' alt=\'\' /></a>')) :
		'') .
'<span class="sep">, </span></li>');
		lang_select();
	}
	lang_select();
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_sel @ ../prive/formulaires/selecteur/inc-sel-rubriques.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}


function BOUCLE_pourhtml_09a0d17a649e7579db5ee33c74ca4d14(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	$command['source'] = array(interdire_scripts(picker_selected(entites_html(table_valeur(@$Pile[0], (string)'selected', null),true),'rubrique')));
	$command['sourcemode'] = 'table';
	if (!isset($command['table'])) {
		$command['table'] = '';
		$command['id'] = '_pour';
		$command['from'] = array();
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array(".valeur");
		$command['orderby'] = array();
		$command['where'] = 
			array();
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"DATA",
		$command,
		array('../prive/formulaires/selecteur/inc-sel-rubriques.html','html_09a0d17a649e7579db5ee33c74ca4d14','_pour',1,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	
	$l1 = _T('public|spip|ecrire:info_racine_site');$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$t0 .= (
'
' .
(($t1 = BOUCLE_selhtml_09a0d17a649e7579db5ee33c74ca4d14($Cache, $Pile, $doublons, $Numrows, $SP))!=='' ?
		$t1 :
		((($t2 = strval(interdire_scripts(((($Pile[$SP]['valeur'] == '0')) ?' ' :''))))!=='' ?
			('
' . $t2 . (	'
<li class=\'rubrique\'><input type="hidden" name="' .
		interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'name', null), 'id_item'),true)) .
		chr('91') .
		chr('93') .
		'" value="rubrique|' .
		interdire_scripts($Pile[$SP]['valeur']) .
		'" />' .
		$l1 .
		(($t3 = strval(interdire_scripts(((entites_html(sinon(table_valeur(@$Pile[0], (string)'select', null), ''),true)) ?'' :' '))))!=='' ?
				($t3 . (	'
<a href=\'#\' onclick=\'jQuery(this).item_unpick();return false;\'><img src=\'' .
			interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'img_unpick', null),true)) .
			'\' alt=\'\' /></a>')) :
				'') .
		'<span class="sep">, </span></li>
')) :
			''))));
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_pour @ ../prive/formulaires/selecteur/inc-sel-rubriques.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}

//
// Fonction principale du squelette ../prive/formulaires/selecteur/inc-sel-rubriques.html
// Temps de compilation total: 6.040 ms
//

function html_09a0d17a649e7579db5ee33c74ca4d14($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
BOUCLE_pourhtml_09a0d17a649e7579db5ee33c74ca4d14($Cache, $Pile, $doublons, $Numrows, $SP) .
'
');

	return analyse_resultat_skel('html_09a0d17a649e7579db5ee33c74ca4d14', $Cache, $page, '../prive/formulaires/selecteur/inc-sel-rubriques.html');
}
?>