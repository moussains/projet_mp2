<?php

/*
 * Squelette : squelettes/formulaires/recherche.html
 * Date :      Thu, 16 Apr 2020 18:17:21 GMT
 * Compile :   Thu, 16 Apr 2020 20:44:01 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette squelettes/formulaires/recherche.html
// Temps de compilation total: 0.146 ms
//

function html_8d1afcfe78e23cfe083dc7250ea2323d($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'<div class="formulaire_spip formulaire_recherche' .
(($t1 = strval(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'class', null),true))))!=='' ?
		(' ' . $t1) :
		'') .
'" id="formulaire_recherche">
	<form action="' .
interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'action', null),true)) .
'" method="get">
			<div class="editer-groupe">
			' .
interdire_scripts(form_hidden(entites_html(table_valeur(@$Pile[0], (string)'action', null),true))) .
'
			' .
(($t1 = strval(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'lang', null),true))))!=='' ?
		('<input type="hidden" name="lang" value="' . $t1 . '" />') :
		'') .
'
			<div class="input-group">
				<input type="' .
('' ? 'search':'text') .
'" class="search text form-control" size="10" name="recherche" id="' .
interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'_id_champ', null),true)) .
'"' .
(($t1 = strval(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'recherche', null),true))))!=='' ?
		(' value="' . $t1 . '"') :
		'') .
' accesskey="4" autocapitalize="off" autocorrect="off" placeholder="Rechercher..."/>
				<div class="input-group-append">
					<button type="submit" class="submit btn btn-info" title="' .
_T('public|spip|ecrire:info_rechercher') .
'"><i class="fas fa-search"></i></button>
				</div>
			</div>
		</div>
	</form>
</div>
');

	return analyse_resultat_skel('html_8d1afcfe78e23cfe083dc7250ea2323d', $Cache, $page, 'squelettes/formulaires/recherche.html');
}
?>