<?php

/*
 * Squelette : prive/informer_auteur.html
 * Date :      Wed, 26 Feb 2020 10:56:05 GMT
 * Compile :   Thu, 16 Apr 2020 13:50:57 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette prive/informer_auteur.html
// Temps de compilation total: 47.281 ms
//

function html_d5c6373d7217dd89e7d551c36dd226f0($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'<'.'?php header(' . _q('Content-Type: text/plain') . '); ?'.'>' .
'<'.'?php header("X-Spip-Cache: 0"); ?'.'>'.'<'.'?php header("Cache-Control: no-cache, must-revalidate"); ?'.'><'.'?php header("Pragma: no-cache"); ?'.'>' .
interdire_scripts(informer_auteur(normaliser_date(@$Pile[0]['date']))));

	return analyse_resultat_skel('html_d5c6373d7217dd89e7d551c36dd226f0', $Cache, $page, 'prive/informer_auteur.html');
}
?>