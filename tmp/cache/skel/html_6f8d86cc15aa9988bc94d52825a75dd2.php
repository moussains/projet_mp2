<?php

/*
 * Squelette : plugins/auto/bootstrap4/v4.1.9/bootstrap2spip/formulaires/recherche.html
 * Date :      Thu, 16 Jan 2020 20:04:18 GMT
 * Compile :   Wed, 22 Jan 2020 12:32:14 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette plugins/auto/bootstrap4/v4.1.9/bootstrap2spip/formulaires/recherche.html
// Temps de compilation total: 1.002 ms
//

function html_6f8d86cc15aa9988bc94d52825a75dd2($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'<div class="formulaire_spip formulaire_recherche form-search' .
(($t1 = strval(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'class', null),true))))!=='' ?
		(' ' . $t1) :
		'') .
'" id="formulaire_recherche">
<form action="' .
interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'action', null),true)) .
'" method="get"><div>
	' .
interdire_scripts(form_hidden(entites_html(table_valeur(@$Pile[0], (string)'action', null),true))) .
'
	' .
(($t1 = strval(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'lang', null),true))))!=='' ?
		('<input type="hidden" name="lang" value="' . $t1 . '" />') :
		'') .
'
	<label for="' .
interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'_id_champ', null),true)) .
'" class="text-muted">' .
_T('public|spip|ecrire:info_rechercher_02') .
'</label>
	<div class="input-group">
		<input type="' .
('' ? 'search':'text') .
'"
		       class="search text search-query form-control"
		       name="recherche"
		       id="' .
interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'_id_champ', null),true)) .
'"' .
(($t1 = strval(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'recherche', null),true))))!=='' ?
		('
		       value="' . $t1 . '"') :
		'') .
'
		       placeholder="' .
attribut_html(_T('public|spip|ecrire:info_rechercher')) .
'"
		       accesskey="4" autocapitalize="off" autocorrect="off" />
		<span class="input-group-append">
			<button type="submit" class="btn btn-outline-secondary" title="' .
attribut_html(_T('public|spip|ecrire:info_rechercher')) .
'" >&gt;&gt;</button>
		</span>
	</div>
</div></form>
</div>
');

	return analyse_resultat_skel('html_6f8d86cc15aa9988bc94d52825a75dd2', $Cache, $page, 'plugins/auto/bootstrap4/v4.1.9/bootstrap2spip/formulaires/recherche.html');
}
?>