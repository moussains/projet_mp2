<?php

/*
 * Squelette : ../plugins-dist/forum/formulaires/configurer_forums_contenu.html
 * Date :      Tue, 21 Jan 2020 17:16:37 GMT
 * Compile :   Wed, 22 Jan 2020 22:24:34 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette ../plugins-dist/forum/formulaires/configurer_forums_contenu.html
// Temps de compilation total: 3.941 ms
//

function html_83fdd1fe1b56c1f9d463f498a00eb5ec($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'<div class=\'formulaire_spip formulaire_configurer formulaire_configurer_forums_contenu\' id=\'formulaire_configurer_forums_contenu\'>
<h3 class=\'titrem\'>' .
interdire_scripts(filtre_balise_img_dist(chemin_image('forum-public-24.png'),'','cadre-icone')) .
_T('forum:titre_forum') .
'</h3>

' .
(($t1 = strval(interdire_scripts(table_valeur(@$Pile[0], (string)'message_ok', null))))!=='' ?
		('<p class="reponse_formulaire reponse_formulaire_ok">' . $t1 . '</p>') :
		'') .
'
' .
(($t1 = strval(interdire_scripts(table_valeur(@$Pile[0], (string)'message_erreur', null))))!=='' ?
		('<p class="reponse_formulaire reponse_formulaire_erreur">' . $t1 . '</p>') :
		'') .
'

	<form action="' .
interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'action', null),true)) .
'#formulaire_configurer_contenu_forums" method="post"><div>
		' .
	'<div>' .
	form_hidden(@$Pile[0]['action']) .
	'<input name=\'formulaire_action\' type=\'hidden\'
		value=\'' . @$Pile[0]['form'] . '\' />' .
	'<input name=\'formulaire_action_args\' type=\'hidden\'
		value=\'' . @$Pile[0]['formulaire_args']. '\' />' .
	(!empty($Pile[0]['_hidden']) ? @$Pile[0]['_hidden'] : '') .
	'</div>' .
'
		<div class="editer-groupe">
			<div class=\'editer configurer_champs_forums' .
((table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),'forums_titre'))  ?
		(' ' . ' ' . 'erreur') :
		'') .
'\'>
				<label>' .
_T('public|spip|ecrire:config_activer_champs') .
'</label>' .
(($t1 = strval(table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),'forums_titre')))!=='' ?
		('
				<span class=\'erreur_message\'>' . $t1 . '</span>
				') :
		'') .
'<div class=\'choix\'>
					<input class=\'checkbox\' type="checkbox" name="forums_titre" value=\'oui\' ' .
(($t1 = strval(interdire_scripts(((entites_html(table_valeur(@$Pile[0], (string)'forums_titre', null),true) == 'oui') ? 'checked':''))))!=='' ?
		('checked="' . $t1 . '"') :
		'') .
' id="forums_titre"/>
					<label for="forums_titre">' .
_T('public|spip|ecrire:info_titre') .
'</label>
				</div>
				<div class=\'choix\'>
					<input class=\'checkbox\' type="checkbox" name="forums_texte" value=\'oui\' ' .
(($t1 = strval(interdire_scripts(((entites_html(table_valeur(@$Pile[0], (string)'forums_texte', null),true) == 'oui') ? 'checked':''))))!=='' ?
		('checked="' . $t1 . '"') :
		'') .
' id="forums_texte"/>
					<label for="forums_texte">' .
_T('public|spip|ecrire:info_texte') .
'</label>
				</div>
				' .
(($t1 = strval(interdire_scripts(((((include_spip('inc/config')?lire_config('barre_outils_public',null,false):'') == 'oui')) ?' ' :''))))!=='' ?
		($t1 . (	'
				<div class=\'choix\'>
					<input class=\'checkbox\' type="checkbox" name="forums_afficher_barre" value=\'oui\' ' .
	(($t2 = strval(interdire_scripts(((entites_html(table_valeur(@$Pile[0], (string)'forums_afficher_barre', null),true) == 'oui') ? 'checked':''))))!=='' ?
			('checked="' . $t2 . '"') :
			'') .
	' id="forums_afficher_barre"/>
					<label for="forums_afficher_barre">' .
	_T('public|spip|ecrire:info_barre_outils') .
	'</label>
				</div>
				')) :
		'') .
'
				<div class=\'choix\'>
					<input class=\'checkbox\' type="checkbox" name="forums_urlref" value=\'oui\' ' .
(($t1 = strval(interdire_scripts(((entites_html(table_valeur(@$Pile[0], (string)'forums_urlref', null),true) == 'oui') ? 'checked':''))))!=='' ?
		('checked="' . $t1 . '"') :
		'') .
' id="forums_urlref"/>
					<label for="forums_urlref">' .
_T('public|spip|ecrire:info_urlref') .
'</label>
				</div>
			</div>
			<div class=\'editer configurer_previsu_forums' .
((table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),'forums_forcer_previsu'))  ?
		(' ' . ' ' . 'erreur') :
		'') .
'\'>
				<label>' .
_T('forum:info_forcer_previsualisation_court') .
'</label>' .
(($t1 = strval(table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),'forcer_previsu')))!=='' ?
		('
				<span class=\'erreur_message\'>' . $t1 . '</span>
				') :
		'') .
'
				<div class=\'choix\'>
					<input class=\'checkbox\' type="checkbox" name="forums_forcer_previsu" value=\'oui\' ' .
(($t1 = strval(interdire_scripts(((((entites_html(table_valeur(@$Pile[0], (string)'forums_forcer_previsu', null),true) == 'non')) ?'' :' ') ? 'checked':''))))!=='' ?
		('checked="' . $t1 . '"') :
		'') .
' id="forums_forcer_previsu"/>
					<label for="forums_forcer_previsu">' .
_T('forum:info_forcer_previsualisation_long') .
'</label>
				</div>
			</div>
			' .
(($t1 = strval(interdire_scripts(((filtre_info_plugin_dist("medias", "est_actif")) ?' ' :''))))!=='' ?
		($t1 . (	'
			<div class=\'editer configurer_documents_forums' .
	((table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),'formats_documents_forum'))  ?
			(' ' . ' ' . 'erreur') :
			'') .
	'\'>
				<label for="formats_documents_forum">' .
	_T('medias:titre_documents_joints') .
	'</label>
				<div class=\'explication\'>' .
	_T('forum:info_question_visiteur_ajout_document_forum') .
	'
				<div class=\'small\'>' .
	_T('forum:info_question_visiteur_ajout_document_forum_format') .
	'</div></div>
				' .
	(($t2 = strval(table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),'formats_documents_forum')))!=='' ?
			('<span class=\'erreur_message\'>' . $t2 . '</span>') :
			'') .
	'
				<input type="text" class="text" name="formats_documents_forum" id="formats_documents_forum"' .
	(($t2 = strval(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'formats_documents_forum', null),true))))!=='' ?
			(' value="' . $t2 . '"') :
			'') .
	' size="40" />
			</div>
			')) :
		'') .
'
		</div>
		<p class=\'boutons\'><input class=\'submit\' type="submit" name="ok" value="' .
_T('public|spip|ecrire:bouton_enregistrer') .
'"/></p>
	</div></form>

</div>
');

	return analyse_resultat_skel('html_83fdd1fe1b56c1f9d463f498a00eb5ec', $Cache, $page, '../plugins-dist/forum/formulaires/configurer_forums_contenu.html');
}
?>