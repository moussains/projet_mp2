<?php

/*
 * Squelette : ../prive/formulaires/selecteur/navigateur.html
 * Date :      Tue, 21 Jan 2020 17:16:31 GMT
 * Compile :   Sat, 01 Feb 2020 20:26:55 GMT
 * Boucles :   _rubriques_branches, _branche, _chemin, _contenu, _rub
 */ 

function BOUCLE_rubriques_brancheshtml_f7f32e8ef54ce8828401de49bc259172(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	$in = array();
	if (!(is_array($a = ($Pile[$SP]['statut']))))
		$in[]= $a;
	else $in = array_merge($in, $a);
	if (!isset($command['table'])) {
		$command['table'] = 'rubriques';
		$command['id'] = '_rubriques_branches';
		$command['from'] = array('rubriques' => 'spip_rubriques');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("rubriques.id_rubrique",
		"rubriques.lang",
		"rubriques.titre");
		$command['orderby'] = array();
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array(sql_in('rubriques.id_rubrique', calcul_branche_in(sql_quote($Pile[$SP]['id_rubrique'], '', 'int NOT NULL'))), 
			array('!=', 'rubriques.id_rubrique', sql_quote($Pile[$SP]['id_rubrique'], '', '')), (!(is_array($Pile[$SP]['statut'])?count($Pile[$SP]['statut']):strlen($Pile[$SP]['statut'])) ? '' : ((is_array($Pile[$SP]['statut'])) ? sql_in('rubriques.statut',sql_quote($in)) : 
			array('=', 'rubriques.statut', sql_quote($Pile[$SP]['statut'], '','varchar(10) NOT NULL DEFAULT \'0\'')))));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('../prive/formulaires/selecteur/navigateur.html','html_f7f32e8ef54ce8828401de49bc259172','_rubriques_branches',12,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	lang_select($GLOBALS['spip_lang']);
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		lang_select_public($Pile[$SP]['lang'], '', $Pile[$SP]['titre']);
		$t0 .= vide($Pile['vars'][$_zzz=(string)'rubriques_branche'] = filtre_push(table_valeur($Pile["vars"], (string)'rubriques_branche', null),$Pile[$SP]['id_rubrique']));
		lang_select();
	}
	lang_select();
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_rubriques_branches @ ../prive/formulaires/selecteur/navigateur.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}


function BOUCLE_branchehtml_f7f32e8ef54ce8828401de49bc259172(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	$in = array();
	if (!(is_array($a = (@$Pile[0]['statut']))))
		$in[]= $a;
	else $in = array_merge($in, $a);
	if (!isset($command['table'])) {
		$command['table'] = 'rubriques';
		$command['id'] = '_branche';
		$command['from'] = array('rubriques' => 'spip_rubriques');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("rubriques.id_rubrique",
		"rubriques.statut",
		"rubriques.titre",
		"rubriques.lang");
		$command['orderby'] = array();
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array(
			array('=', 'rubriques.id_rubrique', sql_quote(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'limite_branche', null),true)), '', 'bigint(21) NOT NULL AUTO_INCREMENT')), (!(is_array(@$Pile[0]['statut'])?count(@$Pile[0]['statut']):strlen(@$Pile[0]['statut'])) ? '' : ((is_array(@$Pile[0]['statut'])) ? sql_in('rubriques.statut',sql_quote($in)) : 
			array('=', 'rubriques.statut', sql_quote(@$Pile[0]['statut'], '','varchar(10) NOT NULL DEFAULT \'0\'')))));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('../prive/formulaires/selecteur/navigateur.html','html_f7f32e8ef54ce8828401de49bc259172','_branche',10,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	lang_select($GLOBALS['spip_lang']);
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		lang_select_public($Pile[$SP]['lang'], '', $Pile[$SP]['titre']);
		$t0 .= (
'
' .
vide($Pile['vars'][$_zzz=(string)'titre_branche'] = interdire_scripts(supprimer_numero(typo($Pile[$SP]['titre']), "TYPO", $connect, $Pile[0]))) .
'
' .
vide($Pile['vars'][$_zzz=(string)'rubriques_branche'] = array()) .
BOUCLE_rubriques_brancheshtml_f7f32e8ef54ce8828401de49bc259172($Cache, $Pile, $doublons, $Numrows, $SP) .
'
');
		lang_select();
	}
	lang_select();
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_branche @ ../prive/formulaires/selecteur/navigateur.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}


function BOUCLE_cheminhtml_f7f32e8ef54ce8828401de49bc259172(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!($id_rubrique = intval($Pile[$SP]['id_rubrique'])))
		return '';
	include_spip('inc/rubriques');
	$hierarchie = calcul_hierarchie_in($id_rubrique,false);
	if (!$hierarchie) return "";
	
	$in = array();
	if (!(is_array($a = ($Pile[$SP]['statut']))))
		$in[]= $a;
	else $in = array_merge($in, $a);
	if (!isset($command['table'])) {
		$command['table'] = 'rubriques';
		$command['id'] = '_chemin';
		$command['from'] = array('rubriques' => 'spip_rubriques');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("rubriques.id_rubrique",
		"rubriques.titre",
		"rubriques.lang");
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['orderby'] = array("FIELD(rubriques.id_rubrique, $hierarchie)");
	$command['where'] = 
			array(
			array('NOT', 
			array('=', 'rubriques.id_rubrique', sql_quote(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'id_r', null),true)), '', 'bigint(21) NOT NULL AUTO_INCREMENT'))), (!(is_array($Pile[$SP]['statut'])?count($Pile[$SP]['statut']):strlen($Pile[$SP]['statut'])) ? '' : ((is_array($Pile[$SP]['statut'])) ? sql_in('rubriques.statut',sql_quote($in)) : 
			array('=', 'rubriques.statut', sql_quote($Pile[$SP]['statut'], '','varchar(10) NOT NULL DEFAULT \'0\'')))), 
			array('IN', 'rubriques.id_rubrique', "($hierarchie)"));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('../prive/formulaires/selecteur/navigateur.html','html_f7f32e8ef54ce8828401de49bc259172','_chemin',17,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	
	// COMPTEUR
	$Numrows['_chemin']['compteur_boucle'] = 0;
	lang_select($GLOBALS['spip_lang']);
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$Numrows['_chemin']['compteur_boucle']++;
		lang_select_public($Pile[$SP]['lang'], '', $Pile[$SP]['titre']);
		$t0 .= (
interdire_scripts(((((entites_html(table_valeur(@$Pile[0], (string)'limite_branche', null),true)) AND (($Numrows['_chemin']['compteur_boucle'] == '1'))) ?' ' :'') ? '':'<span class="sep"> &gt; </span>')) .
'<a href=\'' .
parametre_url(self(),'id_r',$Pile[$SP]['id_rubrique']) .
'\' class=\'ajax\'>' .
interdire_scripts(supprimer_numero(typo($Pile[$SP]['titre']), "TYPO", $connect, $Pile[0])) .
'</a>');
		lang_select();
	}
	lang_select();
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_chemin @ ../prive/formulaires/selecteur/navigateur.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}


function BOUCLE_contenuhtml_f7f32e8ef54ce8828401de49bc259172(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!($id_rubrique = intval($Pile[$SP]['id_rubrique'])))
		return '';
	include_spip('inc/rubriques');
	$hierarchie = calcul_hierarchie_in($id_rubrique,true);
	if (!$hierarchie) return "";
	
	if (!isset($command['table'])) {
		$command['table'] = 'rubriques';
		$command['id'] = '_contenu';
		$command['from'] = array('rubriques' => 'spip_rubriques');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("rubriques.id_rubrique",
		"rubriques.titre",
		"rubriques.lang");
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['orderby'] = array("FIELD(rubriques.id_rubrique, $hierarchie)");
	$command['where'] = 
			array(
			array('IN', 'rubriques.id_rubrique', "($hierarchie)"));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('../prive/formulaires/selecteur/navigateur.html','html_f7f32e8ef54ce8828401de49bc259172','_contenu',19,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	
	// COMPTEUR
	$Numrows['_contenu']['compteur_boucle'] = 0;
	$Numrows['_contenu']['total'] = @intval($iter->count());
	$debut_boucle = $Numrows['_contenu']['total'] - 4;;
	$debut_boucle = intval($debut_boucle);
	$fin_boucle = min($debut_boucle + 3, $Numrows['_contenu']['total'] - 1);
	$Numrows['_contenu']['grand_total'] = $Numrows['_contenu']['total'];
	$Numrows['_contenu']["total"] = max(0,$fin_boucle - $debut_boucle + 1);
	if ($debut_boucle>0 AND $debut_boucle < $Numrows['_contenu']['grand_total'] AND $iter->seek($debut_boucle,'continue'))
		$Numrows['_contenu']['compteur_boucle'] = $debut_boucle;
	
	lang_select($GLOBALS['spip_lang']);
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$Numrows['_contenu']['compteur_boucle']++;
		if ($Numrows['_contenu']['compteur_boucle'] <= $debut_boucle) continue;
		if ($Numrows['_contenu']['compteur_boucle']-1 > $fin_boucle) break;
		lang_select_public($Pile[$SP]['lang'], '', $Pile[$SP]['titre']);
		$t0 .= (
vide($Pile['vars'][$_zzz=(string)'n'] = '0') .
'<div class=\'frame' .
(($t1 = strval(min((isset($Numrows['_contenu']['grand_total'])
			? $Numrows['_contenu']['grand_total'] : $Numrows['_contenu']['total']),moins($Numrows['_contenu']['total'],'1'))))!=='' ?
		(' total_' . $t1) :
		'') .
(($t1 = strval(plus(moins($Numrows['_contenu']['compteur_boucle'],max(plus((isset($Numrows['_contenu']['grand_total'])
			? $Numrows['_contenu']['grand_total'] : $Numrows['_contenu']['total']),'1'),$Numrows['_contenu']['total'])),$Numrows['_contenu']['total'])))!=='' ?
		(' frame_' . $t1) :
		'') .
'\'>' .
(!(($Pile[$SP]['id_rubrique'] == interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'limite_branche', null),true))))  ?
		(' ' . (	'<a
	href=\'' .
	parametre_url(self(),'id_r',($Pile[$SP-1]['id_parent'] ? $Pile[$SP-1]['id_parent']:'racine')) .
	'\' class=\'frame_close ajax\'><img src=\'' .
	interdire_scripts(chemin_image('fermer-16')) .
	'\' alt=\'fermer\' width="16" height="16" /></a>')) :
		'') .
'
	<h2>' .
(($Pile[$SP]['id_rubrique'] == interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'limite_branche', null),true))) ? interdire_scripts(supprimer_numero(typo($Pile[$SP]['titre']), "TYPO", $connect, $Pile[0])):(	'<a class=\'ajax\' href=\'' .
	parametre_url(self(),'id_r',$Pile[$SP]['id_rubrique']) .
	'\'>' .
	interdire_scripts(supprimer_numero(typo($Pile[$SP]['titre']), "TYPO", $connect, $Pile[0])) .
	'</a>')) .
'</h2>
	' .
recuperer_fond( 'formulaires/selecteur/inc-nav-rubriques' , array_merge($Pile[0],array('id_rubrique' => $Pile[$SP]['id_rubrique'] )), array('compil'=>array('../prive/formulaires/selecteur/navigateur.html','html_f7f32e8ef54ce8828401de49bc259172','_contenu',27,$GLOBALS['spip_lang'])), _request('connect')) .
'
	' .
(($t1 = strval(interdire_scripts(((entites_html(sinon(table_valeur(@$Pile[0], (string)'articles', null), '0'),true)) ?' ' :''))))!=='' ?
		($t1 . (	' ' .
	recuperer_fond( 'formulaires/selecteur/inc-nav-articles' , array_merge($Pile[0],array('id_rubrique' => $Pile[$SP]['id_rubrique'] ,
	'id_article' => @$Pile[0]['id_article'] )), array('compil'=>array('../prive/formulaires/selecteur/navigateur.html','html_f7f32e8ef54ce8828401de49bc259172','_contenu',27,$GLOBALS['spip_lang'])), _request('connect')) .
	' ')) :
		'') .
'
	</div>
	');
		lang_select();
	}
	lang_select();
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_contenu @ ../prive/formulaires/selecteur/navigateur.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}


function BOUCLE_rubhtml_f7f32e8ef54ce8828401de49bc259172(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	$in = array();
	if (!(is_array($a = (@$Pile[0]['statut']))))
		$in[]= $a;
	else $in = array_merge($in, $a);
	if (!isset($command['table'])) {
		$command['table'] = 'rubriques';
		$command['id'] = '_rub';
		$command['from'] = array('rubriques' => 'spip_rubriques');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("rubriques.statut",
		"rubriques.id_parent",
		"rubriques.id_rubrique",
		"rubriques.titre",
		"rubriques.lang");
		$command['orderby'] = array();
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array(
			array('=', 'rubriques.id_rubrique', sql_quote(interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'id_r', null), interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'id_rubrique', null),true))),true)), '', 'bigint(21) NOT NULL AUTO_INCREMENT')), (!(is_array(@$Pile[0]['statut'])?count(@$Pile[0]['statut']):strlen(@$Pile[0]['statut'])) ? '' : ((is_array(@$Pile[0]['statut'])) ? sql_in('rubriques.statut',sql_quote($in)) : 
			array('=', 'rubriques.statut', sql_quote(@$Pile[0]['statut'], '','varchar(10) NOT NULL DEFAULT \'0\'')))));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('../prive/formulaires/selecteur/navigateur.html','html_f7f32e8ef54ce8828401de49bc259172','_rub',14,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	lang_select($GLOBALS['spip_lang']);
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		lang_select_public($Pile[$SP]['lang'], '', $Pile[$SP]['titre']);
		$t0 .= (
'
	<div class=\'chemin\'>
		' .
(($t1 = strval(interdire_scripts(((entites_html(table_valeur(@$Pile[0], (string)'limite_branche', null),true)) ?'' :' '))))!=='' ?
		($t1 . (	'<a href=\'' .
	parametre_url(self(),'id_r','0') .
	'\' class=\'ajax\'>' .
	_T('public|spip|ecrire:info_racine_site') .
	'</a>')) :
		'') .
'
		' .
BOUCLE_cheminhtml_f7f32e8ef54ce8828401de49bc259172($Cache, $Pile, $doublons, $Numrows, $SP) .
interdire_scripts(((entites_html(table_valeur(@$Pile[0], (string)'limite_branche', null),true) == $Pile[$SP]['id_rubrique']) ? '':'<span class="sep"> &gt; </span>')) .
'<strong class=\'on\'>' .
interdire_scripts(supprimer_numero(typo($Pile[$SP]['titre']), "TYPO", $connect, $Pile[0])) .
'</strong>
	</div>' .
vide($Pile['vars'][$_zzz=(string)'n'] = '0') .
(($t1 = BOUCLE_contenuhtml_f7f32e8ef54ce8828401de49bc259172($Cache, $Pile, $doublons, $Numrows, $SP))!=='' ?
		((	'
	
	' .
		(($t3 = strval(interdire_scripts(((entites_html(table_valeur(@$Pile[0], (string)'limite_branche', null),true)) ?'' :' '))))!=='' ?
				($t3 . (	'
	' .
			(($t4 = strval(((((isset($Numrows['_contenu']['grand_total'])
			? $Numrows['_contenu']['grand_total'] : $Numrows['_contenu']['total']) < $Numrows['_contenu']['total'])) ?' ' :'')))!=='' ?
					($t4 . (	'
	<div class=\'frame' .
				(($t5 = strval(max((isset($Numrows['_contenu']['grand_total'])
			? $Numrows['_contenu']['grand_total'] : $Numrows['_contenu']['total']),moins($Numrows['_contenu']['total'],'1'))))!=='' ?
						(' total_' . $t5) :
						'') .
				' frame_0\'><h2>' .
				(($t5 = strval(interdire_scripts(((entites_html(sinon(table_valeur(@$Pile[0], (string)'rubriques', null), '0'),true)) ?' ' :''))))!=='' ?
						('<a' . $t5 . (	' href=\'#\' onclick="jQuery(this).item_pick(\'rubrique|0\',\'' .
					interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'name', null), 'id_item'),true)) .
					'\',\'' .
					attribut_html(texte_script(_T('public|spip|ecrire:info_racine_site'))) .
					'\',\'rubrique\');return false;"
><img class=\'add\' src=\'' .
					interdire_scripts(chemin_image('ajouter-16.png')) .
					'\' alt=\'\' /></a>')) :
						'') .
				_T('public|spip|ecrire:info_racine_site') .
				'</h2>
	' .
				recuperer_fond( 'formulaires/selecteur/inc-nav-rubriques' , array_merge($Pile[0],array('id_rubrique' => '0' )), array('compil'=>array('../prive/formulaires/selecteur/navigateur.html','html_f7f32e8ef54ce8828401de49bc259172','_rub',21,$GLOBALS['spip_lang'])), _request('connect')) .
				'</div>')) :
					''))) :
				'') .
		'
	') . $t1) :
		'') .
'
');
		lang_select();
	}
	lang_select();
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_rub @ ../prive/formulaires/selecteur/navigateur.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}

//
// Fonction principale du squelette ../prive/formulaires/selecteur/navigateur.html
// Temps de compilation total: 63.728 ms
//

function html_f7f32e8ef54ce8828401de49bc259172($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
vide($Pile['vars'][$_zzz=(string)'titre_branche'] = '') .
BOUCLE_branchehtml_f7f32e8ef54ce8828401de49bc259172($Cache, $Pile, $doublons, $Numrows, $SP) .
'
' .
(($t1 = BOUCLE_rubhtml_f7f32e8ef54ce8828401de49bc259172($Cache, $Pile, $doublons, $Numrows, $SP))!=='' ?
		$t1 :
		((	'
<div class=\'chemin\'><strong class=\'on\'>' .
	sinon(table_valeur($Pile["vars"], (string)'titre_branche', null), _T('public|spip|ecrire:info_racine_site')) .
	'</strong></div>
<div class=\'frame total_0 frame_0\'><h2>' .
	sinon(table_valeur($Pile["vars"], (string)'titre_branche', null), _T('public|spip|ecrire:info_racine_site')) .
	'</h2>' .
	recuperer_fond( 'formulaires/selecteur/inc-nav-rubriques' , array_merge($Pile[0],array('id_rubrique' => interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'limite_branche', null), '0'),true)) )), array('compil'=>array('../prive/formulaires/selecteur/navigateur.html','html_f7f32e8ef54ce8828401de49bc259172','',34,$GLOBALS['spip_lang'])), _request('connect')) .
	'</div>
'))) .
'
');

	return analyse_resultat_skel('html_f7f32e8ef54ce8828401de49bc259172', $Cache, $page, '../prive/formulaires/selecteur/navigateur.html');
}
?>