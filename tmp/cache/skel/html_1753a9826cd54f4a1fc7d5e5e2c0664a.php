<?php

/*
 * Squelette : plugins/auto/bootstrap3/v3.0.10/bootstrap2spip/formulaires/recherche.html
 * Date :      Wed, 25 Dec 2019 21:04:22 GMT
 * Compile :   Tue, 21 Jan 2020 22:09:32 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette plugins/auto/bootstrap3/v3.0.10/bootstrap2spip/formulaires/recherche.html
// Temps de compilation total: 0.833 ms
//

function html_1753a9826cd54f4a1fc7d5e5e2c0664a($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'<div class="formulaire_spip formulaire_recherche form-search' .
(($t1 = strval(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'class', null),true))))!=='' ?
		(' ' . $t1) :
		'') .
'" id="formulaire_recherche">
<form action="' .
interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'action', null),true)) .
'" method="get"><div>
	' .
interdire_scripts(form_hidden(entites_html(table_valeur(@$Pile[0], (string)'action', null),true))) .
'
	' .
(($t1 = strval(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'lang', null),true))))!=='' ?
		('<input type="hidden" name="lang" value="' . $t1 . '" />') :
		'') .
'
	<label for="' .
interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'_id_champ', null),true)) .
'" class="muted">' .
_T('public|spip|ecrire:info_rechercher_02') .
'</label>
	<div class="input-append input-group">
		<input type="' .
('' ? 'search':'text') .
'" class="search text search-query form-control" name="recherche" id="' .
interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'_id_champ', null),true)) .
'"' .
(($t1 = strval(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'recherche', null),true))))!=='' ?
		(' value="' . $t1 . '"') :
		'') .
' accesskey="4" autocapitalize="off" autocorrect="off" />
		<span class="btn-group input-group-btn">
			<button type="submit" class="btn btn-default" title="' .
attribut_html(_T('public|spip|ecrire:info_rechercher')) .
'" >&gt;&gt;</button>
		</span>
	</div>
</div></form>
</div>
');

	return analyse_resultat_skel('html_1753a9826cd54f4a1fc7d5e5e2c0664a', $Cache, $page, 'plugins/auto/bootstrap3/v3.0.10/bootstrap2spip/formulaires/recherche.html');
}
?>