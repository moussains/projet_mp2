<?php

/*
 * Squelette : prive/aa.html
 * Date :      Tue, 21 Jan 2020 09:09:45 GMT
 * Compile :   Tue, 21 Jan 2020 09:09:53 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette prive/aa.html
// Temps de compilation total: 0.072 ms
//

function html_b4d4d2fe0301adf65485308d1473c909($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = 'h';

	return analyse_resultat_skel('html_b4d4d2fe0301adf65485308d1473c909', $Cache, $page, 'prive/aa.html');
}
?>