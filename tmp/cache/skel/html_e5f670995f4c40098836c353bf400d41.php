<?php

/*
 * Squelette : ../plugins/auto/menus_1/formulaires/inc-nouvelle_entree-1.html
 * Date :      Fri, 20 Dec 2019 12:26:26 GMT
 * Compile :   Tue, 04 Feb 2020 13:26:11 GMT
 * Boucles :   _types_entrees
 */ 

function BOUCLE_types_entreeshtml_e5f670995f4c40098836c353bf400d41(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	$command['source'] = array(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'entrees', null),true)));
	$command['sourcemode'] = 'table';
	if (!isset($command['table'])) {
		$command['table'] = '';
		$command['id'] = '_types_entrees';
		$command['from'] = array();
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array(".cle",
		".valeur");
		$command['orderby'] = array('rang');
		$command['where'] = 
			array();
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"DATA",
		$command,
		array('../plugins/auto/menus_1/formulaires/inc-nouvelle_entree-1.html','html_e5f670995f4c40098836c353bf400d41','_types_entrees',6,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$t0 .= (
'
		<div class="choix menu_' .
interdire_scripts($Pile[$SP]['cle']) .
'" id="choix_menu_' .
interdire_scripts($Pile[$SP]['cle']) .
'" style="padding:3px;border-bottom:1px solid #eee;">
			' .
interdire_scripts(filtrer('image_graver',filtrer('image_reduire',table_valeur($Pile[$SP]['valeur'],'icone'),'24'))) .
'
			<input type="radio" class="radio" name="type_entree" id="' .
interdire_scripts($Pile[$SP]['cle']) .
'" value="' .
interdire_scripts($Pile[$SP]['cle']) .
'" style="vertical-align:top;" />
			<label for="' .
interdire_scripts($Pile[$SP]['cle']) .
'">' .
interdire_scripts(table_valeur($Pile[$SP]['valeur'],'nom')) .
'</label>
		</div>
		');
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_types_entrees @ ../plugins/auto/menus_1/formulaires/inc-nouvelle_entree-1.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}

//
// Fonction principale du squelette ../plugins/auto/menus_1/formulaires/inc-nouvelle_entree-1.html
// Temps de compilation total: 7.596 ms
//

function html_e5f670995f4c40098836c353bf400d41($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'
<ul class="editer-groupe">
	<li class="editer pleine_largeur editer_type' .
(($t1 = strval(interdire_scripts(((entites_html(table_valeur(@$Pile[0], (string)'type', null),true)) ?' ' :''))))!=='' ?
		($t1 . 'erreur') :
		'') .
'">
		<label>' .
_T('menus:entree_choisir') .
'</label>
		' .
(($t1 = strval(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'type', null),true))))!=='' ?
		('<span class=\'erreur_message\'>' . $t1 . '</span>') :
		'') .
'
		' .
(($t1 = BOUCLE_types_entreeshtml_e5f670995f4c40098836c353bf400d41($Cache, $Pile, $doublons, $Numrows, $SP))!=='' ?
		$t1 :
		((	'
		<span class=\'erreur_message\'>' .
	_T('menus:erreur_aucun_type') .
	'</span>
		'))) .
'
	</li>
</ul>

<p class=\'boutons\'>
	<span class=\'image_loading\'></span>
	<input type="hidden" name="demander_nouvelle_entree" value="' .
interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'id_menu_nouvelle_entree', null),true)) .
'" />
	<input type="submit" class="submit link" name="demander_nouvelle_entree" value="' .
_T('public|spip|ecrire:icone_retour') .
'" />
	<input type="submit" class="submit" name="suivant" value="' .
_T('public|spip|ecrire:bouton_suivant') .
'" />
</p>
');

	return analyse_resultat_skel('html_e5f670995f4c40098836c353bf400d41', $Cache, $page, '../plugins/auto/menus_1/formulaires/inc-nouvelle_entree-1.html');
}
?>