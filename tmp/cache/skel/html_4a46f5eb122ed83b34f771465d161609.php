<?php

/*
 * Squelette : ../plugins-dist/revisions/formulaires/configurer_revisions_objets.html
 * Date :      Sun, 19 Jan 2020 16:26:49 GMT
 * Compile :   Sun, 19 Jan 2020 17:12:29 GMT
 * Boucles :   _objets
 */ 

function BOUCLE_objetshtml_4a46f5eb122ed83b34f771465d161609(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	$command['source'] = array(lister_tables_objets_sql(''));
	$command['sourcemode'] = 'table';
	if (!isset($command['table'])) {
		$command['table'] = '';
		$command['id'] = '_objets';
		$command['from'] = array();
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array(".valeur",
		".cle");
		$command['orderby'] = array();
		$command['where'] = 
			array();
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"DATA",
		$command,
		array('../plugins-dist/revisions/formulaires/configurer_revisions_objets.html','html_4a46f5eb122ed83b34f771465d161609','_objets',14,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$t0 .= (($t1 = strval(interdire_scripts(test_objet_versionable($Pile[$SP]['valeur']))))!=='' ?
		($t1 . (	'
				' .
	vide($Pile['vars'][$_zzz=(string)'id'] = concat(replace(table_valeur($Pile["vars"], (string)'name', null),'\\W','_'),'_',interdire_scripts($Pile[$SP]['cle']))) .
	'<div class="choix choix_' .
	interdire_scripts($Pile[$SP]['cle']) .
	'">
					<input type="checkbox"  id="' .
	table_valeur($Pile["vars"], (string)'id', null) .
	'" name="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'[]" value="' .
	interdire_scripts($Pile[$SP]['cle']) .
	'"' .
	(($t2 = strval(interdire_scripts(in_any($Pile[$SP]['cle'],interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)table_valeur($Pile["vars"], (string)'name', null), null),true))))))!=='' ?
			($t2 . 'checked="checked"') :
			'') .
	' />
					<label for="' .
	table_valeur($Pile["vars"], (string)'id', null) .
	'">' .
	interdire_scripts(_T(table_valeur($Pile[$SP]['valeur'], 'texte_objets'))) .
	'</label>
				</div>
				')) :
		'');
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_objets @ ../plugins-dist/revisions/formulaires/configurer_revisions_objets.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}

//
// Fonction principale du squelette ../plugins-dist/revisions/formulaires/configurer_revisions_objets.html
// Temps de compilation total: 8.704 ms
//

function html_4a46f5eb122ed83b34f771465d161609($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'<div class=\'formulaire_spip formulaire_configurer formulaire_configurer_forums_contenu\' id=\'formulaire_configurer_forums_contenu\'>
<h3 class=\'titrem\'>' .
interdire_scripts(filtre_balise_img_dist(chemin_image('revision-24.png'),'','cadre-icone')) .
_T('revisions:titre_form_revisions_objets') .
'</h3>

' .
(($t1 = strval(interdire_scripts(table_valeur(@$Pile[0], (string)'message_ok', null))))!=='' ?
		('<p class="reponse_formulaire reponse_formulaire_ok">' . $t1 . '</p>') :
		'') .
'
' .
(($t1 = strval(interdire_scripts(table_valeur(@$Pile[0], (string)'message_erreur', null))))!=='' ?
		('<p class="reponse_formulaire reponse_formulaire_erreur">' . $t1 . '</p>') :
		'') .
'

	<form action="' .
interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'action', null),true)) .
'#formulaire_configurer_contenu_forums" method="post"><div>
		' .
	'<div>' .
	form_hidden(@$Pile[0]['action']) .
	'<input name=\'formulaire_action\' type=\'hidden\'
		value=\'' . @$Pile[0]['form'] . '\' />' .
	'<input name=\'formulaire_action_args\' type=\'hidden\'
		value=\'' . @$Pile[0]['formulaire_args']. '\' />' .
	(!empty($Pile[0]['_hidden']) ? @$Pile[0]['_hidden'] : '') .
	'</div>' .
'
		<div class="editer-groupe">
			<div class=\'editer long_label configurer_objets_versions' .
((table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),'objets_versions'))  ?
		(' ' . ' ' . 'erreur') :
		'') .
'\'>
				<label>' .
_T('revisions:label_config_revisions_objets') .
'</label>
				' .
(($t1 = strval(table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),'objets_versions')))!=='' ?
		('<span class=\'erreur_message\'>' . $t1 . '</span>') :
		'') .
'
				' .
vide($Pile['vars'][$_zzz=(string)'name'] = 'objets_versions') .
BOUCLE_objetshtml_4a46f5eb122ed83b34f771465d161609($Cache, $Pile, $doublons, $Numrows, $SP) .
'
				<input type="hidden" name="' .
interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'name', null),true)) .
'[]" value="" />
			</div>
		</div>
		<p class=\'boutons\'><span class="image_loading"></span><input class=\'submit\' type="submit" name="ok" value="' .
_T('public|spip|ecrire:bouton_enregistrer') .
'"/></p>
	</div></form>

</div>');

	return analyse_resultat_skel('html_4a46f5eb122ed83b34f771465d161609', $Cache, $page, '../plugins-dist/revisions/formulaires/configurer_revisions_objets.html');
}
?>