<?php

/*
 * Squelette : plugins/auto/player/v3.1.0/modeles/emb_mp3.html
 * Date :      Fri, 20 Dec 2019 12:26:28 GMT
 * Compile :   Sun, 09 Feb 2020 20:43:13 GMT
 * Boucles :   _doc
 */ 

function BOUCLE_dochtml_2d856507c5f70c1f756c3ef63a56ede7(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($command['table'])) {
		$command['table'] = 'documents';
		$command['id'] = '_doc';
		$command['from'] = array('documents' => 'spip_documents','L1' => 'spip_types_documents');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("documents.id_document",
		"documents.id_vignette",
		"L1.mime_type",
		"L1.titre AS type_document",
		"documents.taille",
		"documents.titre",
		"documents.descriptif");
		$command['orderby'] = array();
		$command['join'] = array('L1' => array('documents','extension'));
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array(
			array('(documents.taille > 0 OR documents.distant=\'oui\')'), 
			array('=', 'documents.id_document', sql_quote(@$Pile[0]['id_document'], '','bigint(21) NOT NULL AUTO_INCREMENT')));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('plugins/auto/player/v3.1.0/modeles/emb_mp3.html','html_2d856507c5f70c1f756c3ef63a56ede7','_doc',19,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$t0 .= (
'
' .
vide($Pile['vars'][$_zzz=(string)'player'] = interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'player', null), interdire_scripts((include_spip('inc/config')?lire_config('player/player_mp3','neoplayer',false):''))),true))) .
vide($Pile['vars'][$_zzz=(string)'my_width'] = interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'largeur', null), interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'width', null), '120'),true))),true))) .
(((table_valeur($Pile["vars"], (string)'player', null) == 'pixplayer'))  ?
		(' ' . vide($Pile['vars'][$_zzz=(string)'my_width'] = interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'largeur', null), interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'width', null), '290'),true))),true)))) :
		'') .
'
' .
(((table_valeur($Pile["vars"], (string)'player', null) == 'neoplayer'))  ?
		(' ' . vide($Pile['vars'][$_zzz=(string)'my_width'] = interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'largeur', null), interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'width', null), '200'),true))),true)))) :
		'') .
'
' .
(((table_valeur($Pile["vars"], (string)'player', null) == 'dewplayer'))  ?
		(' ' . vide($Pile['vars'][$_zzz=(string)'my_width'] = interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'largeur', null), interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'width', null), '200'),true))),true)))) :
		'') .
'
' .
(((table_valeur($Pile["vars"], (string)'player', null) == 'mejs'))  ?
		(' ' . vide($Pile['vars'][$_zzz=(string)'my_width'] = interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'largeur', null), interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'width', null), '400'),true))),true)))) :
		'') .
'
' .
vide($Pile['vars'][$_zzz=(string)'my_width'] = max(min(largeur(quete_logo_document(quete_document($Pile[$SP]['id_document'], ''), '', '', '', 0, 0, '')),'350'),'120',table_valeur($Pile["vars"], (string)'my_width', null))) .
vide($Pile['vars'][$_zzz=(string)'logo'] = filtrer('image_graver',filtrer('image_reduire',quete_logo_document(quete_document($Pile[$SP]['id_document'], ''), '', '', '', 0, 0, ''),table_valeur($Pile["vars"], (string)'my_width', null),'*'))) .
'<dl class=\'spip_document_' .
$Pile[$SP]['id_document'] .
' spip_documents spip_documents_player' .
(($t1 = strval(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'align', null),true))))!=='' ?
		(' spip_documents_' . $t1) :
		'') .
(($t1 = strval(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'class', null),true))))!=='' ?
		(' ' . $t1) :
		'') .
' spip_doc_player spip_lien_ok\'' .
(($t1 = strval(interdire_scripts(match(entites_html(table_valeur(@$Pile[0], (string)'align', null),true),'left|right'))))!=='' ?
		(' style=\'float:' . $t1 . ';\'') :
		'') .
'>
' .
(((($Pile[$SP]['id_vignette']) AND (interdire_scripts((entites_html(table_valeur(@$Pile[0], (string)'afficher_vignette', null),true) != 'non')))) ?' ' :'') ? (	'<dt><a href="' .
	interdire_scripts(((($a = entites_html(table_valeur(@$Pile[0], (string)'lien', null),true)) OR (is_string($a) AND strlen($a))) ? $a : vider_url(urlencode_1738(generer_url_entite($Pile[$SP]['id_document'], 'document', '', '', true))))) .
	'" type="' .
	interdire_scripts($Pile[$SP]['mime_type']) .
	'"' .
	(($t2 = strval(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'lien_class', null),true))))!=='' ?
			(' class="' . $t2 . '"') :
			'') .
	' title=\'' .
	interdire_scripts($Pile[$SP]['type_document']) .
	' - ' .
	interdire_scripts(texte_backend(taille_en_octets($Pile[$SP]['taille']))) .
	'\'>
	<img src=\'' .
	extraire_attribut(table_valeur($Pile["vars"], (string)'logo', null),'src') .
	'\' width=\'' .
	largeur(table_valeur($Pile["vars"], (string)'logo', null)) .
	'\' height=\'' .
	hauteur(table_valeur($Pile["vars"], (string)'logo', null)) .
	'\' alt=\'' .
	interdire_scripts($Pile[$SP]['type_document']) .
	' - ' .
	interdire_scripts(texte_backend(taille_en_octets($Pile[$SP]['taille']))) .
	'\' /></a></dt>
'):'') .
'
' .
recuperer_fond( (	'players/' .
	table_valeur($Pile["vars"], (string)'player', null) .
	'/player') , array_merge($Pile[0],array('player' => table_valeur($Pile["vars"], (string)'player', null) ,
	'my_width' => table_valeur($Pile["vars"], (string)'my_width', null) )), array('etoile'=>true,'compil'=>array('plugins/auto/player/v3.1.0/modeles/emb_mp3.html','html_2d856507c5f70c1f756c3ef63a56ede7','_doc',31,$GLOBALS['spip_lang'])), _request('connect')) .
(($t1 = strval(interdire_scripts(supprimer_numero(typo($Pile[$SP]['titre']), "TYPO", $connect, $Pile[0]))))!=='' ?
		((	'
<dt class=\'spip_doc_titre ' .
	'\'' .
	(($t2 = strval(table_valeur($Pile["vars"], (string)'my_width', null)))!=='' ?
			(' style=\'width:' . $t2 . 'px;\'') :
			'') .
	'><strong>') . $t1 . '</strong></dt>') :
		'') .
(($t1 = strval(interdire_scripts(PtoBR(propre($Pile[$SP]['descriptif'], $connect, $Pile[0])))))!=='' ?
		((	'
<dd class=\'spip_doc_descriptif ' .
	'\'' .
	(($t2 = strval(table_valeur($Pile["vars"], (string)'my_width', null)))!=='' ?
			(' style=\'margin-bottom:0;width:' . $t2 . 'px;\'') :
			'') .
	'>') . $t1 . (	interdire_scripts(PtoBR(calculer_notes())) .
	'</dd>')) :
		'') .
'
</dl>

');
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_doc @ plugins/auto/player/v3.1.0/modeles/emb_mp3.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}

//
// Fonction principale du squelette plugins/auto/player/v3.1.0/modeles/emb_mp3.html
// Temps de compilation total: 12.474 ms
//

function html_2d856507c5f70c1f756c3ef63a56ede7($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'
' .
BOUCLE_dochtml_2d856507c5f70c1f756c3ef63a56ede7($Cache, $Pile, $doublons, $Numrows, $SP) .
'
');

	return analyse_resultat_skel('html_2d856507c5f70c1f756c3ef63a56ede7', $Cache, $page, 'plugins/auto/player/v3.1.0/modeles/emb_mp3.html');
}
?>