<?php

/*
 * Squelette : ../prive/formulaires/selecteur/picker-ajax.html
 * Date :      Sun, 19 Jan 2020 16:26:13 GMT
 * Compile :   Sun, 19 Jan 2020 17:21:05 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette ../prive/formulaires/selecteur/picker-ajax.html
// Temps de compilation total: 4.826 ms
//

function html_b62b9cc5c69d9969ad1644a604668996($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
vide($Pile['vars'][$_zzz=(string)'bouton_modif'] = interdire_scripts(_T((entites_html(sinon(table_valeur(@$Pile[0], (string)'select', null), ''),true) ? 'bouton_modifier':'bouton_ajouter')))) .
(($t1 = strval(interdire_scripts(((entites_html(sinon(table_valeur(@$Pile[0], (string)'picker', null), ''),true)) ?'' :' '))))!=='' ?
		($t1 . (	'
<div class=\'picker_bouton\'>&#91;<a href=\'' .
	parametre_url(self(),'picker','1') .
	'\' class=\'ajax\'>' .
	table_valeur($Pile["vars"], (string)'bouton_modif', null) .
	'</a>&#93;</div>
')) :
		'') .
'
' .
(($t1 = strval(interdire_scripts(((entites_html(sinon(table_valeur(@$Pile[0], (string)'picker', null), ''),true)) ?' ' :''))))!=='' ?
		($t1 . (	'
<div class=\'picker_bouton\'>&#91;<a class=\'close\'
href=\'' .
	parametre_url(self(),'picker','0') .
	'\' 
onclick="jQuery(this).parent().picker_toggle();return false;"
>' .
	_T('public|spip|ecrire:bouton_fermer') .
	'</a><a class=\'edit\'
href=\'' .
	parametre_url(self(),'picker','1') .
	'\' 
onclick="jQuery(this).parent().picker_toggle();return false;"
style=\'display:none;\'>' .
	table_valeur($Pile["vars"], (string)'bouton_modif', null) .
	'</a>&#93;</div>

' .
	'
<div class=\'browser\'>
	<div class="choix_rapide">
		<label for="picker_id">' .
	_T('public|spip|ecrire:label_ajout_id_rapide') .
	'</label>
		<input type="text" value="" id="picker_id" size="10" />
		<a href="#"
			 onclick="jQuery.ajax({\'url\':\'' .
	interdire_scripts(generer_url_public('ajax_item_pick', (	'rubriques=' .
		interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'rubriques', null),true)) .
		'&articles=' .
		interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'articles', null),true)) .
		'&ref='))) .
	'\'+jQuery(\'#picker_id\').val(),
			 \'dataType\':\'text\',
			 \'success\':function(r){
					eval(\'r = \'+r); // JSON envoye par ajax_item_pick.html
					if (r){
						jQuery(\'#picker_id\').item_pick(r.id,\'' .
	interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'name', null), 'id_item'),true)) .
	'\',r.titre,r.type);
					}
					jQuery(\'#picker_id\').val(\'\');
			 }
		 });return false;">' .
	table_valeur($Pile["vars"], (string)'bouton_modif', null) .
	'</a>
	</div>
	' .
	
'<'.'?php echo recuperer_fond( ' . argumenter_squelette('formulaires/selecteur/navigateur') . ', array_merge('.var_export($Pile[0],1).',array(\'lang\' => ' . argumenter_squelette($GLOBALS["spip_lang"]) . ')), array("compil"=>array(\'../prive/formulaires/selecteur/picker-ajax.html\',\'html_b62b9cc5c69d9969ad1644a604668996\',\'\',29,$GLOBALS[\'spip_lang\']),\'ajax\' => ($v=( ' . argumenter_squelette(@$Pile[0]['ajax']) . '))?$v:true), _request("connect"));
?'.'>
</div>
')) :
		'') .
'
');

	return analyse_resultat_skel('html_b62b9cc5c69d9969ad1644a604668996', $Cache, $page, '../prive/formulaires/selecteur/picker-ajax.html');
}
?>