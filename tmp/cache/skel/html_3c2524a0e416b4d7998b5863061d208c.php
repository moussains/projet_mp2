<?php

/*
 * Squelette : ../prive/formulaires/selecteur/inc-nav-rubriques.html
 * Date :      Tue, 21 Jan 2020 17:16:31 GMT
 * Compile :   Sat, 01 Feb 2020 20:26:55 GMT
 * Boucles :   _enfants
 */ 

function BOUCLE_enfantshtml_3c2524a0e416b4d7998b5863061d208c(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	$in = array();
	if (!(is_array($a = (@$Pile[0]['statut']))))
		$in[]= $a;
	else $in = array_merge($in, $a);
	$command['pagination'] = array((isset($Pile[0]['debut'.table_valeur($Pile["vars"], (string)'p', null)]) ? $Pile[0]['debut'.table_valeur($Pile["vars"], (string)'p', null)] : null), 100);
	if (!isset($command['table'])) {
		$command['table'] = 'rubriques';
		$command['id'] = '_enfants';
		$command['from'] = array('rubriques' => 'spip_rubriques');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("rubriques.id_rubrique",
		"0+rubriques.titre AS num",
		"rubriques.titre",
		"rubriques.lang");
		$command['orderby'] = array('num', 'rubriques.titre');
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array(
			array('=', 'rubriques.id_parent', sql_quote(@$Pile[0]['id_rubrique'], '', 'bigint(21) NOT NULL DEFAULT 0')), (!(is_array(@$Pile[0]['statut'])?count(@$Pile[0]['statut']):strlen(@$Pile[0]['statut'])) ? '' : ((is_array(@$Pile[0]['statut'])) ? sql_in('rubriques.statut',sql_quote($in)) : 
			array('=', 'rubriques.statut', sql_quote(@$Pile[0]['statut'], '','varchar(10) NOT NULL DEFAULT \'0\'')))));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('../prive/formulaires/selecteur/inc-nav-rubriques.html','html_3c2524a0e416b4d7998b5863061d208c','_enfants',1,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	
	// COMPTEUR
	$Numrows['_enfants']['compteur_boucle'] = 0;
	$Numrows['_enfants']['total'] = @intval($iter->count());
	$debut_boucle = isset($Pile[0]['debut'.table_valeur($Pile["vars"], (string)'p', null)]) ? $Pile[0]['debut'.table_valeur($Pile["vars"], (string)'p', null)] : _request('debut'.table_valeur($Pile["vars"], (string)'p', null));
	if(substr($debut_boucle,0,1)=='@'){
		$debut_boucle = $Pile[0]['debut'.table_valeur($Pile["vars"], (string)'p', null)] = quete_debut_pagination('id_rubrique',$Pile[0]['@id_rubrique'] = substr($debut_boucle,1),100,$iter);
		$iter->seek(0);
	}
	$debut_boucle = intval($debut_boucle);
	$debut_boucle = (($tout=($debut_boucle == -1))?0:($debut_boucle));
	$debut_boucle = max(0,min($debut_boucle,floor(($Numrows['_enfants']['total']-1)/(100))*(100)));
	$debut_boucle = intval($debut_boucle);
	$fin_boucle = min(($tout ? $Numrows['_enfants']['total'] : $debut_boucle + 99), $Numrows['_enfants']['total'] - 1);
	$Numrows['_enfants']['grand_total'] = $Numrows['_enfants']['total'];
	$Numrows['_enfants']["total"] = max(0,$fin_boucle - $debut_boucle + 1);
	if ($debut_boucle>0 AND $debut_boucle < $Numrows['_enfants']['grand_total'] AND $iter->seek($debut_boucle,'continue'))
		$Numrows['_enfants']['compteur_boucle'] = $debut_boucle;
	
	lang_select($GLOBALS['spip_lang']);
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$Numrows['_enfants']['compteur_boucle']++;
		if ($Numrows['_enfants']['compteur_boucle'] <= $debut_boucle) continue;
		if ($Numrows['_enfants']['compteur_boucle']-1 > $fin_boucle) break;
		lang_select_public($Pile[$SP]['lang'], '', $Pile[$SP]['titre']);
		$t0 .= (
'<li
	class=\'rubrique\'>' .
(($t1 = strval(interdire_scripts(((entites_html(sinon(table_valeur(@$Pile[0], (string)'rubriques', null), '0'),true)) ?' ' :''))))!=='' ?
		($t1 . (	'<a
	href=\'#\' onclick="jQuery(this).item_pick(\'rubrique|' .
	$Pile[$SP]['id_rubrique'] .
	'\',\'' .
	interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'name', null), 'id_item'),true)) .
	'\',\'' .
	interdire_scripts(replace(texte_script(attribut_html(extraire_multi($Pile[$SP]['titre']))),'&#039;',concat('\\\\',interdire_scripts(eval('return '.'chr(39)'.';'))))) .
	'\',\'rubrique\');return false;"
><img
	class=\'add\'
	src=\'' .
	interdire_scripts(chemin_image('ajouter-16.png')) .
	'\'
	alt=\'ajouter\'
	width="16"
	height="16"
	/></a>' .
	interdire_scripts(supprimer_numero(typo($Pile[$SP]['titre']), "TYPO", $connect, $Pile[0])))) :
		'') .
(test_enfants_rubrique($Pile[$SP]['id_rubrique'],interdire_scripts((entites_html(sinon(table_valeur(@$Pile[0], (string)'articles', null), ''),true) ? array('article'):''))) ? (($t2 = strval(interdire_scripts(filtre_balise_img_dist(chemin_image('fleche-droite-16.png'),'','ouvrir'))))!=='' ?
			((	'
	<a
	class=\'ajax\'
	href=\'' .
		parametre_url(self(),'id_r',$Pile[$SP]['id_rubrique']) .
		'\'
		>' .
		(($t3 = strval(interdire_scripts(((entites_html(sinon(table_valeur(@$Pile[0], (string)'rubriques', null), '0'),true)) ?'' :' '))))!=='' ?
				(interdire_scripts(supprimer_numero(typo($Pile[$SP]['titre']), "TYPO", $connect, $Pile[0])) . $t3) :
				'')) . $t2 . '</a>') :
			''):(($t2 = strval(interdire_scripts(((entites_html(sinon(table_valeur(@$Pile[0], (string)'rubriques', null), '0'),true)) ?'' :' '))))!=='' ?
			(interdire_scripts(supprimer_numero(typo($Pile[$SP]['titre']), "TYPO", $connect, $Pile[0])) . $t2) :
			'')) .
'</li>
');
		lang_select();
	}
	lang_select();
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_enfants @ ../prive/formulaires/selecteur/inc-nav-rubriques.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}

//
// Fonction principale du squelette ../prive/formulaires/selecteur/inc-nav-rubriques.html
// Temps de compilation total: 8.296 ms
//

function html_3c2524a0e416b4d7998b5863061d208c($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
vide($Pile['vars'][$_zzz=(string)'p'] = (	'pr_' .
	@$Pile[0]['id_rubrique'])) .
(($t1 = BOUCLE_enfantshtml_3c2524a0e416b4d7998b5863061d208c($Cache, $Pile, $doublons, $Numrows, $SP))!=='' ?
		((	'
' .
		(($t3 = strval(filtre_pagination_dist($Numrows["_enfants"]["grand_total"],
 		table_valeur($Pile["vars"], (string)'p', null),
		isset($Pile[0]['debut'.table_valeur($Pile["vars"], (string)'p', null)])?$Pile[0]['debut'.table_valeur($Pile["vars"], (string)'p', null)]:intval(_request('debut'.table_valeur($Pile["vars"], (string)'p', null))),
		100, true, '', '', array())))!=='' ?
				('<p class=\'pagination\'>' . $t3 . '</p>') :
				'') .
		'
<ul class=\'items\'>
') . $t1 . '
</ul>
') :
		''));

	return analyse_resultat_skel('html_3c2524a0e416b4d7998b5863061d208c', $Cache, $page, '../prive/formulaires/selecteur/inc-nav-rubriques.html');
}
?>