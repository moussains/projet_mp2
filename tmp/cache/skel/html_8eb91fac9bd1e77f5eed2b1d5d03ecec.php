<?php

/*
 * Squelette : ../plugins-dist/medias/formulaires/illustrer_document.html
 * Date :      Tue, 21 Jan 2020 17:16:46 GMT
 * Compile :   Sat, 01 Feb 2020 20:26:21 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette ../plugins-dist/medias/formulaires/illustrer_document.html
// Temps de compilation total: 8.824 ms
//

function html_8eb91fac9bd1e77f5eed2b1d5d03ecec($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'<div class=\'formulaire_spip formulaire_illustrer_document\' id=\'formulaire_illustrer_document-' .
interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'id', null), 'new'),true)) .
'\'>
	' .
(($t1 = strval(interdire_scripts(table_valeur(@$Pile[0], (string)'message_ok', null))))!=='' ?
		('<p class="reponse_formulaire reponse_formulaire_ok">' . $t1 . '</p>') :
		'') .
'
	' .
(($t1 = strval(interdire_scripts(table_valeur(@$Pile[0], (string)'message_erreur', null))))!=='' ?
		('<p class="reponse_formulaire reponse_formulaire_erreur">' . $t1 . '</p>') :
		'') .
'
	' .
(($t1 = strval(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'editable', null),true))))!=='' ?
		($t1 . (	'
	<form action="' .
	interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'action', null),true)) .
	'#formulaire_illustrer_document-' .
	interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'id', null), 'new'),true)) .
	'" method="post" enctype=\'multipart/form-data\'><div>
		' .
		'<div>' .
	form_hidden(@$Pile[0]['action']) .
	'<input name=\'formulaire_action\' type=\'hidden\'
		value=\'' . @$Pile[0]['form'] . '\' />' .
	'<input name=\'formulaire_action_args\' type=\'hidden\'
		value=\'' . @$Pile[0]['formulaire_args']. '\' />' .
	(!empty($Pile[0]['_hidden']) ? @$Pile[0]['_hidden'] : '') .
	'</div>
		<div class="editer-groupe">
			<div class="editer editer_fichier' .
	((table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),'fichier'))  ?
			(' ' . ' ' . 'erreur') :
			'') .
	'">
				<label for="fichier">' .
	_T('medias:label_fichier_vignette') .
	'</label>' .
	(($t2 = strval(table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),'fichier')))!=='' ?
			('
				<span class=\'erreur_message\'>' . $t2 . '</span>
				') :
			'') .
	(($t2 = strval(interdire_scripts((intval((include_spip('inc/config')?lire_config('taille_preview',null,false):'')) ? interdire_scripts(filtrer('image_graver',filtrer('image_reduire',table_valeur(@$Pile[0], (string)'vignette', null),interdire_scripts((include_spip('inc/config')?lire_config('taille_preview',null,false):''))))):interdire_scripts(filtre_balise_img_dist(table_valeur(@$Pile[0], (string)'vignette', null)))))))!=='' ?
			('
				' . $t2 . '
				') :
			'') .
	(($t2 = strval(interdire_scripts(((table_valeur(@$Pile[0], (string)'vignette', null)) ?'' :' '))))!=='' ?
			($t2 . (	_T('medias:aucune_vignette') .
		'
				' .
		(($t3 = strval(interdire_scripts((((((include_spip('inc/config')?lire_config('creer_preview',null,false):'') == 'oui')) AND (interdire_scripts((entites_html(table_valeur(@$Pile[0], (string)'media', null),true) == 'image')))) ?' ' :''))))!=='' ?
				($t3 . (	'<br />' .
			_T('medias:miniature_automatique_active'))) :
				'') .
		'
				')) :
			'') .
	'
				<p class=\'actions\'>
				' .
	(($t2 = strval(interdire_scripts((((((entites_html(table_valeur(@$Pile[0], (string)'id_vignette', null),true)) ?' ' :'')) AND (invalideur_session($Cache, ((function_exists("autoriser")||include_spip("inc/autoriser"))&&autoriser('supprimer', 'document', interdire_scripts(invalideur_session($Cache, entites_html(table_valeur(@$Pile[0], (string)'id_vignette', null),true))))?" ":"")))) ?' ' :''))))!=='' ?
			($t2 . (	' <input type=\'submit\' class=\'submit\' name=\'supprimer\' value=\'' .
		attribut_html(_T('medias:bouton_supprimer')) .
		'\' />')) :
			'') .
	'
				' .
	(($t2 = strval(interdire_scripts((((((entites_html(table_valeur(@$Pile[0], (string)'id_vignette', null),true)) ?'' :' ')) OR (invalideur_session($Cache, ((function_exists("autoriser")||include_spip("inc/autoriser"))&&autoriser('modifier', 'document', interdire_scripts(invalideur_session($Cache, entites_html(table_valeur(@$Pile[0], (string)'id_vignette', null),true))))?" ":"")))) ?' ' :''))))!=='' ?
			($t2 . (	' &#91;<a href=\'#\' onclick=\'jQuery("#illustrer_document").toggle("fast");return false;\'>' .
		_T('public|spip|ecrire:bouton_changer') .
		'</a>&#93;')) :
			'') .
	'
				<span class=\'image_loading\'>&nbsp;</span>
				</p>
				<div id=\'illustrer_document\' style=\'display:none;\'>
				' .
	recuperer_fond( 'formulaires/inc-upload_document' , array_merge($Pile[0],array('joindre_upload' => 'oui' )), array('compil'=>array('../plugins-dist/medias/formulaires/illustrer_document.html','html_8eb91fac9bd1e77f5eed2b1d5d03ecec','',19,$GLOBALS['spip_lang'])), _request('connect')) .
	'
				</div>
			</div>
		</div>
	</div></form>')) :
		'') .
'
</div>');

	return analyse_resultat_skel('html_8eb91fac9bd1e77f5eed2b1d5d03ecec', $Cache, $page, '../plugins-dist/medias/formulaires/illustrer_document.html');
}
?>