<?php

/*
 * Squelette : ../prive/squelettes/top/dist.html
 * Date :      Tue, 21 Jan 2020 17:16:32 GMT
 * Compile :   Tue, 21 Jan 2020 17:22:23 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette ../prive/squelettes/top/dist.html
// Temps de compilation total: 0.052 ms
//

function html_9cb07bf23b653c924e0457493a27048b($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = '<!-- top -->';

	return analyse_resultat_skel('html_9cb07bf23b653c924e0457493a27048b', $Cache, $page, '../prive/squelettes/top/dist.html');
}
?>