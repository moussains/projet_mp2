<?php

/*
 * Squelette : ../plugins-dist/breves/prive/squelettes/contenu/breve.html
 * Date :      Wed, 26 Feb 2020 10:55:57 GMT
 * Compile :   Mon, 09 Mar 2020 18:39:17 GMT
 * Boucles :   _breve
 */ 

function BOUCLE_brevehtml_94bfa16bb2d188c0be70abf763344bb8(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	$in = array();
	if (!(is_array($a = (@$Pile[0]['statut']))))
		$in[]= $a;
	else $in = array_merge($in, $a);
	if (!isset($si_init)) { $command['si'] = array(); $si_init = true; }
	$command['si'][] = interdire_scripts((entites_html(table_valeur(@$Pile[0], (string)'exec', null),true) == 'breve'));

	if (!isset($command['table'])) {
		$command['table'] = 'breves';
		$command['id'] = '_breve';
		$command['from'] = array('breves' => 'spip_breves');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("breves.titre",
		"breves.id_breve",
		"breves.titre AS titre_rang",
		"breves.lang");
		$command['orderby'] = array();
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array(
			array('=', 'breves.id_breve', sql_quote(@$Pile[0]['id_breve'], '','bigint(21) NOT NULL AUTO_INCREMENT')), (!(is_array(@$Pile[0]['statut'])?count(@$Pile[0]['statut']):strlen(@$Pile[0]['statut'])) ? '' : ((is_array(@$Pile[0]['statut'])) ? sql_in('breves.statut',sql_quote($in)) : 
			array('=', 'breves.statut', sql_quote(@$Pile[0]['statut'], '','varchar(6) NOT NULL DEFAULT \'0\'')))));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('../plugins-dist/breves/prive/squelettes/contenu/breve.html','html_94bfa16bb2d188c0be70abf763344bb8','_breve',1,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	lang_select($GLOBALS['spip_lang']);
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		lang_select_public($Pile[$SP]['lang'], '', $Pile[$SP]['titre']);
		$t0 .= (
'
' .
boite_ouvrir((($t2 = strval(interdire_scripts(((($a = supprimer_numero(typo($Pile[$SP]['titre']), "TYPO", $connect, $Pile[0])) OR (is_string($a) AND strlen($a))) ? $a : _T('public|spip|ecrire:info_sans_titre')))))!=='' ?
			((	'
				
	' .
		(($t3 = strval(invalideur_session($Cache, ((function_exists("autoriser")||include_spip("inc/autoriser"))&&autoriser('modifier', 'breve', invalideur_session($Cache, $Pile[$SP]['id_breve']))?" ":""))))!=='' ?
				($t3 . (	'
				
		' .
			(!(afficher_qui_edite($Pile[$SP]['id_breve'],'breve'))  ?
					(' ' . (	'
			' .
				filtre_icone_verticale_dist(generer_url_ecrire('breve_edit',(	'id_breve=' .
					$Pile[$SP]['id_breve'])),_T('breves:icone_modifier_breve'),'breve','edit','right ajax preload') .
				'
		')) :
					'') .
			'
		' .
			((afficher_qui_edite($Pile[$SP]['id_breve'],'breve'))  ?
					(' ' . (	'
			' .
				filtre_icone_verticale_dist(generer_url_ecrire('breve_edit',(	'id_breve=' .
					$Pile[$SP]['id_breve'])),afficher_qui_edite($Pile[$SP]['id_breve'],'breve'),'warning-24','','right edition_deja ajax preload') .
				'
		')) :
					'') .
			'
	')) :
				'') .
		'
	<h1>' .
		(($t3 = strval(recuperer_numero($Pile[$SP]['titre_rang'])))!=='' ?
				($t3 . '. ') :
				'')) . $t2 . (	interdire_scripts(filtre_balise_img_dist(chemin_image('breve-24.png'),'breve','cadre-icone')) .
		'</h1>
')) :
			''), 'simple fiche_objet') .
'

<div class="ajax">
	' .
executer_balise_dynamique('FORMULAIRE_DATER',
	array('breve',$Pile[$SP]['id_breve']),
	array('../plugins-dist/breves/prive/squelettes/contenu/breve.html','html_94bfa16bb2d188c0be70abf763344bb8','_breve',5,$GLOBALS['spip_lang'])) .
'</div>

<div id="wysiwyg">
' .

'<'.'?php echo recuperer_fond( ' . argumenter_squelette('prive/objets/contenu/breve') . ', array(\'id\' => ' . argumenter_squelette($Pile[$SP]['id_breve']) . ',
	\'id_breve\' => ' . argumenter_squelette($Pile[$SP]['id_breve']) . ',
	\'wysiwyg\' => ' . argumenter_squelette('1') . ',
	\'lang\' => ' . argumenter_squelette($GLOBALS["spip_lang"]) . '), array("compil"=>array(\'../plugins-dist/breves/prive/squelettes/contenu/breve.html\',\'html_94bfa16bb2d188c0be70abf763344bb8\',\'\',9,$GLOBALS[\'spip_lang\'])), _request("connect"));
?'.'>
</div>

' .
pipeline( 'afficher_complement_objet' , array('args' => array('type' => 'breve', 'id' => $Pile[$SP]['id_breve']), 'data' => '<div class="nettoyeur"></div>') ) .
boite_fermer() .
'

' .
vide($Pile['vars'][$_zzz=(string)'enfants'] = '') .
pipeline( 'affiche_enfants' , array('args' => array('exec' => table_valeur(@$Pile[0], (string)'exec', null), 'objet' => 'breve', 'id_objet' => $Pile[$SP]['id_breve']), 'data' => table_valeur($Pile["vars"], (string)'enfants', null)) ) .
'

' .
(((defined('_AJAX')?constant('_AJAX'):''))  ?
		(' ' . '
<script type="text/javascript">if (window.jQuery)
jQuery(function(){jQuery(\'#navigation>div\').ajaxReload({args:{exec:\'breve\'}});})
</script>
') :
		'') .
'
');
		lang_select();
	}
	lang_select();
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_breve @ ../plugins-dist/breves/prive/squelettes/contenu/breve.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}

//
// Fonction principale du squelette ../plugins-dist/breves/prive/squelettes/contenu/breve.html
// Temps de compilation total: 22.415 ms
//

function html_94bfa16bb2d188c0be70abf763344bb8($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
(($t1 = BOUCLE_brevehtml_94bfa16bb2d188c0be70abf763344bb8($Cache, $Pile, $doublons, $Numrows, $SP))!=='' ?
		$t1 :
		((	'
' .
	((table_valeur(@$Pile[0], (string)'exec', null) == 'breve_edit') ? recuperer_fond( 'prive/squelettes/contenu/breve_edit' , array_merge($Pile[0],array('redirect' => '' ,
	'retourajax' => 'oui' )), array('compil'=>array('../plugins-dist/breves/prive/squelettes/contenu/breve.html','html_94bfa16bb2d188c0be70abf763344bb8','',0,$GLOBALS['spip_lang'])), _request('connect')):sinon_interdire_acces('')) .
	'
'))) .
'
');

	return analyse_resultat_skel('html_94bfa16bb2d188c0be70abf763344bb8', $Cache, $page, '../plugins-dist/breves/prive/squelettes/contenu/breve.html');
}
?>