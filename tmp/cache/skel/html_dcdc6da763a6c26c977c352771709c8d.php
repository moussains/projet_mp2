<?php

/*
 * Squelette : ../plugins-dist/breves/prive/squelettes/extra/breve.html
 * Date :      Wed, 26 Feb 2020 10:55:57 GMT
 * Compile :   Mon, 09 Mar 2020 18:39:17 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette ../plugins-dist/breves/prive/squelettes/extra/breve.html
// Temps de compilation total: 0.585 ms
//

function html_dcdc6da763a6c26c977c352771709c8d($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (($t1 = strval(interdire_scripts((((entites_html(table_valeur(@$Pile[0], (string)'exec', null),true) == 'breve_edit')) ?' ' :''))))!=='' ?
		($t1 . 
'<'.'?php echo recuperer_fond( ' . argumenter_squelette('prive/squelettes/extra/breve_edit') . ', array_merge('.var_export($Pile[0],1).',array(\'lang\' => ' . argumenter_squelette($GLOBALS["spip_lang"]) . ')), array("compil"=>array(\'../plugins-dist/breves/prive/squelettes/extra/breve.html\',\'html_dcdc6da763a6c26c977c352771709c8d\',\'\',1,$GLOBALS[\'spip_lang\'])), _request("connect"));
?'.'>') :
		'');

	return analyse_resultat_skel('html_dcdc6da763a6c26c977c352771709c8d', $Cache, $page, '../plugins-dist/breves/prive/squelettes/extra/breve.html');
}
?>