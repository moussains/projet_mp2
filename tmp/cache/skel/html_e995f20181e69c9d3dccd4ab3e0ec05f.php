<?php

/*
 * Squelette : plugins/auto/menus_1/inclure/nav.html
 * Date :      Fri, 20 Dec 2019 12:26:26 GMT
 * Compile :   Wed, 22 Jan 2020 13:55:51 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette plugins/auto/menus_1/inclure/nav.html
// Temps de compilation total: 0.241 ms
//

function html_e995f20181e69c9d3dccd4ab3e0ec05f($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'<div class="menu-conteneur navbar-inner">
' .
recuperer_fond( 'inclure/menu' , array_merge($Pile[0],array('identifiant' => 'barrenav' )), array('compil'=>array('plugins/auto/menus_1/inclure/nav.html','html_e995f20181e69c9d3dccd4ab3e0ec05f','',2,$GLOBALS['spip_lang'])), _request('connect')) .
'</div>
');

	return analyse_resultat_skel('html_e995f20181e69c9d3dccd4ab3e0ec05f', $Cache, $page, 'plugins/auto/menus_1/inclure/nav.html');
}
?>