<?php

/*
 * Squelette : ../plugins-dist/medias/prive/squelettes/navigation/document_edit.html
 * Date :      Tue, 21 Jan 2020 17:16:46 GMT
 * Compile :   Sat, 01 Feb 2020 20:26:20 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette ../plugins-dist/medias/prive/squelettes/navigation/document_edit.html
// Temps de compilation total: 0.451 ms
//

function html_9c27eaf992acf6d1608bd2325101671b($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = 
'<'.'?php echo recuperer_fond( ' . argumenter_squelette('prive/squelettes/inclure/document_infos') . ', array_merge('.var_export($Pile[0],1).',array(\'lang\' => ' . argumenter_squelette($GLOBALS["spip_lang"]) . ')), array("compil"=>array(\'../plugins-dist/medias/prive/squelettes/navigation/document_edit.html\',\'html_9c27eaf992acf6d1608bd2325101671b\',\'\',1,$GLOBALS[\'spip_lang\']),\'ajax\' => ($v=( ' . argumenter_squelette('document_infos') . '))?$v:true), _request("connect"));
?'.'>';

	return analyse_resultat_skel('html_9c27eaf992acf6d1608bd2325101671b', $Cache, $page, '../plugins-dist/medias/prive/squelettes/navigation/document_edit.html');
}
?>