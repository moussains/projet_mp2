<?php

/*
 * Squelette : ../plugins-dist/mots/prive/squelettes/inclure/administrer_mot.html
 * Date :      Wed, 26 Feb 2020 10:55:59 GMT
 * Compile :   Tue, 10 Mar 2020 21:00:40 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette ../plugins-dist/mots/prive/squelettes/inclure/administrer_mot.html
// Temps de compilation total: 5.414 ms
//

function html_4dc71c273f15fd27b8de03a6ab344bf3($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (($t1 = strval(invalideur_session($Cache, ((function_exists("autoriser")||include_spip("inc/autoriser"))&&autoriser('administrer', 'mot', invalideur_session($Cache, @$Pile[0]['id_mot']))?" ":""))))!=='' ?
		($t1 . (	'

	' .
	(($t2 = strval(interdire_scripts((((entites_html(table_valeur(@$Pile[0], (string)'administrer', null),true) == 'oui')) ?' ' :''))))!=='' ?
			($t2 . (	'
	<div class=\'nettoyeur\'></div>
	<a href=\'' .
		parametre_url(self(),'administrer','non') .
		'\' class=\'ajax bouton_fermer\'>' .
		interdire_scripts(filtre_balise_img_dist(chemin_image('fermer-16.png'))) .
		'</a>
	<div class="ajax">
		' .
		executer_balise_dynamique('FORMULAIRE_ADMINISTRER_MOT',
	array(@$Pile[0]['id_mot']),
	array('../plugins-dist/mots/prive/squelettes/inclure/administrer_mot.html','html_4dc71c273f15fd27b8de03a6ab344bf3','',7,$GLOBALS['spip_lang'])) .
		'</div>
	<div class=\'nettoyeur\'></div>
	')) :
			'') .
	'
	' .
	(($t2 = strval(interdire_scripts((((entites_html(table_valeur(@$Pile[0], (string)'administrer', null),true) == 'oui')) ?'' :' '))))!=='' ?
			($t2 . (	'
		' .
		filtre_icone_dist(parametre_url(self(),'administrer','oui'),_T('adminmots:icone_administrer_mot'),'mot-24.png',lang_dir(@$Pile[0]['lang'], 'right','left'),'edit','ajax') .
		'
	')) :
			'') .
	'

')) :
		'');

	return analyse_resultat_skel('html_4dc71c273f15fd27b8de03a6ab344bf3', $Cache, $page, '../plugins-dist/mots/prive/squelettes/inclure/administrer_mot.html');
}
?>