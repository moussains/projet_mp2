<?php

/*
 * Squelette : ../plugins-dist/medias/prive/squelettes/inclure/document_infos.html
 * Date :      Tue, 21 Jan 2020 17:16:46 GMT
 * Compile :   Sat, 01 Feb 2020 20:26:20 GMT
 * Boucles :   _liens
 */ 

function BOUCLE_lienshtml_8b48ef672e3b114547eef13ef43fa82a(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($command['table'])) {
		$command['table'] = 'documents_liens';
		$command['id'] = '_liens';
		$command['from'] = array('documents_liens' => 'spip_documents_liens');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("documents_liens.objet",
		"documents_liens.id_objet",
		"documents_liens.vu",
		"documents_liens.id_document");
		$command['orderby'] = array();
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array(
			array('=', 'documents_liens.id_document', sql_quote(@$Pile[0]['id_document'], '','bigint(21) NOT NULL DEFAULT 0')));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('../plugins-dist/medias/prive/squelettes/inclure/document_infos.html','html_8b48ef672e3b114547eef13ef43fa82a','_liens',5,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	$Numrows['_liens']['total'] = @intval($iter->count());
	
	$l1 = _T('medias:document_vu');
	$l2 = _T('medias:document_vu');
	$l3 = _T('medias:bouton_enlever_document');$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$t0 .= (
'
		<li class=\'item ' .
interdire_scripts($Pile[$SP]['objet']) .
'\'>' .
(($t1 = strval(interdire_scripts(filtre_balise_img_dist(chemin_image(interdire_scripts(concat(objet_info($Pile[$SP]['objet'],'icone_objet'),'-16.png')))))))!=='' ?
		($t1 . ' ') :
		'') .
generer_lien_entite($Pile[$SP]['id_objet'],interdire_scripts($Pile[$SP]['objet'])) .
'
		' .
(($t1 = strval(interdire_scripts(((($Pile[$SP]['vu'] == 'oui')) ?' ' :''))))!=='' ?
		($t1 . (	'
		' .
	(($t2 = strval(interdire_scripts(inserer_attribut(inserer_attribut(filtre_balise_img_dist(chemin_image('document-vu-24.png')),'title',$l1),'alt',$l1))))!=='' ?
			('<span class=\'vu\'>' . $t2 . '</span>') :
			'') .
	'
		')) :
		'') .
'
		' .
(($t1 = strval(invalideur_session($Cache, ((((function_exists("autoriser")||include_spip("inc/autoriser"))&&autoriser('dissocierdocuments', interdire_scripts(invalideur_session($Cache, $Pile[$SP]['objet'])), invalideur_session($Cache, $Pile[$SP]['id_objet']))?" ":"")) ?' ' :''))))!=='' ?
		($t1 . (	'
			' .
	filtre_bouton_action_dist($l3,invalideur_session($Cache, generer_action_auteur('dissocier_document',(	invalideur_session($Cache, $Pile[$SP]['id_objet']) .
			'-' .
			interdire_scripts(invalideur_session($Cache, $Pile[$SP]['objet'])) .
			'-' .
			invalideur_session($Cache, $Pile[$SP]['id_document']) .
			'--safe'),invalideur_session($Cache, self()))),'ajax') .
	'
		')) :
		'') .
'
		</li>
		');
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_liens @ ../plugins-dist/medias/prive/squelettes/inclure/document_infos.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}

//
// Fonction principale du squelette ../plugins-dist/medias/prive/squelettes/inclure/document_infos.html
// Temps de compilation total: 28.359 ms
//

function html_8b48ef672e3b114547eef13ef43fa82a($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
boite_ouvrir('', 'info') .
pipeline( 'boite_infos' , array('data' => '', 'args' => array('type' => 'document', 'id' => interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'id_document', null),true)))) ) .
boite_fermer() .
'
<div class=\'document_utilisations\'>
' .
(($t1 = BOUCLE_lienshtml_8b48ef672e3b114547eef13ef43fa82a($Cache, $Pile, $doublons, $Numrows, $SP))!=='' ?
		((	'
' .
		boite_ouvrir(singulier_ou_pluriel($Numrows['_liens']['total'],'medias:une_utilisation','medias:des_utilisations'), 'document utilisations simple') .
		'
	<ul class=\'liste_items\'>
		') . $t1 . (	'
	</ul>
' .
		boite_fermer() .
		'
')) :
		((	'
<h3>' .
	_T('medias:aucune_utilisation') .
	'</h3>
'))) .
'
</div>
');

	return analyse_resultat_skel('html_8b48ef672e3b114547eef13ef43fa82a', $Cache, $page, '../plugins-dist/medias/prive/squelettes/inclure/document_infos.html');
}
?>