<?php

/*
 * Squelette : ../plugins-dist/svp/prive/squelettes/navigation/charger_plugin.html
 * Date :      Tue, 21 Jan 2020 17:16:54 GMT
 * Compile :   Tue, 21 Jan 2020 21:43:08 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette ../plugins-dist/svp/prive/squelettes/navigation/charger_plugin.html
// Temps de compilation total: 0.714 ms
//

function html_ab670c9c949df8363e2af80eef8bac96($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'
' .
recuperer_fond( 'prive/squelettes/inclure/menu-navigation' , array_merge($Pile[0],array('menu' => 'menu_configuration' ,
	'bloc' => 'navigation' )), array('compil'=>array('../plugins-dist/svp/prive/squelettes/navigation/charger_plugin.html','html_ab670c9c949df8363e2af80eef8bac96','',2,$GLOBALS['spip_lang'])), _request('connect')) .
'
' .
boite_ouvrir('', 'info') .
_T('svp:info_boite_charger_plugin') .
'
' .
boite_fermer() .
'
');

	return analyse_resultat_skel('html_ab670c9c949df8363e2af80eef8bac96', $Cache, $page, '../plugins-dist/svp/prive/squelettes/navigation/charger_plugin.html');
}
?>