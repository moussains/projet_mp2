<?php

/*
 * Squelette : plugins/auto/bootstrap4/v4.1.9/bootstrap2spip/modeles/pagination.html
 * Date :      Wed, 25 Dec 2019 21:04:22 GMT
 * Compile :   Mon, 17 Feb 2020 09:22:33 GMT
 * Boucles :   _pages
 */ 

function BOUCLE_pageshtml_71f108a5bc331b56e134bac9bbf56953(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	$command['source'] = array(table_valeur($Pile["vars"], (string)'pages', null));
	$command['sourcemode'] = 'table';
	if (!isset($command['table'])) {
		$command['table'] = '';
		$command['id'] = '_pages';
		$command['from'] = array();
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array(".valeur");
		$command['orderby'] = array();
		$command['where'] = 
			array();
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"DATA",
		$command,
		array('plugins/auto/bootstrap4/v4.1.9/bootstrap2spip/modeles/pagination.html','html_71f108a5bc331b56e134bac9bbf56953','_pages',7,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$t0 .= (
(($t1 = strval(vide($Pile['vars'][$_zzz=(string)'item'] = interdire_scripts(mult(moins($Pile[$SP]['valeur'],'1'),interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'pas', null),true)))))))!=='' ?
		('
		' . $t1 . '
		') :
		'') .
(($t1 = strval(interdire_scripts(filtre_lien_ou_expose_dist(ancre_url(parametre_url(entites_html(table_valeur(@$Pile[0], (string)'url', null),true),interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'debut', null),true)),table_valeur($Pile["vars"], (string)'item', null)),interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'ancre', null),true))),table_valeur($Pile["vars"], (string)'item', null),interdire_scripts((($Pile[$SP]['valeur'] == interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'page_courante', null),true))) ? 'span.page-link':'')),'page-link lien_pagination','','nofollow'))))!=='' ?
		((	'<li class="page-item' .
	(($t2 = strval(interdire_scripts(((($Pile[$SP]['valeur'] == interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'page_courante', null),true)))) ?' ' :''))))!=='' ?
			($t2 . 'active') :
			'') .
	'">') . $t1 . '</li>') :
		''));
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_pages @ plugins/auto/bootstrap4/v4.1.9/bootstrap2spip/modeles/pagination.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}

//
// Fonction principale du squelette plugins/auto/bootstrap4/v4.1.9/bootstrap2spip/modeles/pagination.html
// Temps de compilation total: 39.605 ms
//

function html_71f108a5bc331b56e134bac9bbf56953($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
interdire_scripts(table_valeur(@$Pile[0], (string)'bloc_ancre', null)) .
vide($Pile['vars'][$_zzz=(string)'bornes'] = interdire_scripts(filtre_bornes_pagination_dist(entites_html(table_valeur(@$Pile[0], (string)'page_courante', null),true),interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'nombre_pages', null),true)),'5'))) .
vide($Pile['vars'][$_zzz=(string)'premiere'] = filtre_reset(table_valeur($Pile["vars"], (string)'bornes', null))) .
vide($Pile['vars'][$_zzz=(string)'derniere'] = filtre_end(table_valeur($Pile["vars"], (string)'bornes', null))) .
vide($Pile['vars'][$_zzz=(string)'pages'] = range(table_valeur($Pile["vars"], (string)'premiere', null),table_valeur($Pile["vars"], (string)'derniere', null))) .
vide($Pile['vars'][$_zzz=(string)'sep'] = interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'separateur', null), '-'),true))) .
(($t1 = BOUCLE_pageshtml_71f108a5bc331b56e134bac9bbf56953($Cache, $Pile, $doublons, $Numrows, $SP))!=='' ?
		((	'
	' .
		(($t3 = strval(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'label', null),true))))!=='' ?
				('<span class="pagination-label"><span class="label">' . $t3 . '</span></span>') :
				'') .
		'
	<ul>' .
		(($t3 = strval(((table_valeur($Pile["vars"], (string)'premiere', null) > '1') ? interdire_scripts(@$Pile[0]['icon'] . '{start,\'\',1}'):'')))!=='' ?
				((	'<li class="page-item"><a
	href=\'' .
			interdire_scripts(ancre_url(parametre_url(entites_html(table_valeur(@$Pile[0], (string)'url', null),true),interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'debut', null),true)),''),interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'ancre', null),true)))) .
			'\'
	class=\'page-link lien_pagination\'
	rel=\'nofollow\'
	title=\'1\'>') . $t3 . '</a></li><li
	class="page-item tbc disabled"><span class="page-link">...</span></li>') :
				'')) . $t1 . (	(($t3 = strval(((table_valeur($Pile["vars"], (string)'derniere', null) < interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'nombre_pages', null),true))) ? interdire_scripts(@$Pile[0]['icon'] . '{end,\'\',#ENV{nombre_pages}}'):'')))!=='' ?
				((	'<li

	class="page-item tbc disabled"><span class="page-link">...</span></li><li class="page-item"><a
	href=\'' .
			interdire_scripts(parametre_url(entites_html(table_valeur(@$Pile[0], (string)'url', null),true),interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'debut', null),true)),interdire_scripts(mult(moins(entites_html(table_valeur(@$Pile[0], (string)'nombre_pages', null),true),'1'),interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'pas', null),true)))))) .
			'#' .
			interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'ancre', null),true)) .
			'\'
	class=\'page-link lien_pagination\'
	title=\'' .
			interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'nombre_pages', null),true)) .
			'\'
	rel=\'nofollow\'>') . $t3 . '</a></li>') :
				'') .
		'
	</ul>
')) :
		''));

	return analyse_resultat_skel('html_71f108a5bc331b56e134bac9bbf56953', $Cache, $page, 'plugins/auto/bootstrap4/v4.1.9/bootstrap2spip/modeles/pagination.html');
}
?>