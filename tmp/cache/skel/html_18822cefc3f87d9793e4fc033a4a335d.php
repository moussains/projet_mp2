<?php

/*
 * Squelette : ../prive/squelettes/top/auteur.html
 * Date :      Tue, 21 Jan 2020 17:16:32 GMT
 * Compile :   Fri, 31 Jan 2020 13:45:16 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette ../prive/squelettes/top/auteur.html
// Temps de compilation total: 1.098 ms
//

function html_18822cefc3f87d9793e4fc033a4a335d($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (($t1 = strval(interdire_scripts((((entites_html(table_valeur(@$Pile[0], (string)'id_auteur', null),true) == interdire_scripts(invalideur_session($Cache, table_valeur($GLOBALS["visiteur_session"], (string)'id_auteur', null))))) ?' ' :''))))!=='' ?
		($t1 . (	'
' .
	barre_onglets('infos_perso','infos_perso') .
	'
<style>#chemin {display: none}</style>
')) :
		'');

	return analyse_resultat_skel('html_18822cefc3f87d9793e4fc033a4a335d', $Cache, $page, '../prive/squelettes/top/auteur.html');
}
?>