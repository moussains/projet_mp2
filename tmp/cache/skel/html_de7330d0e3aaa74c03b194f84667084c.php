<?php

/*
 * Squelette : ../plugins-dist/forum/formulaires/configurer_forums_notifications.html
 * Date :      Tue, 21 Jan 2020 17:16:37 GMT
 * Compile :   Wed, 22 Jan 2020 22:24:34 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette ../plugins-dist/forum/formulaires/configurer_forums_notifications.html
// Temps de compilation total: 1.903 ms
//

function html_de7330d0e3aaa74c03b194f84667084c($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'<div class=\'formulaire_spip formulaire_configurer formulaire_configurer_forums_notifications\' id=\'formulaire_configurer_forums_notifications\'>
<h3 class=\'titrem\'>' .
interdire_scripts(filtre_balise_img_dist(chemin_image('annonce-24.png'),'','cadre-icone')) .
_T('forum:info_envoi_forum') .
'</h3>

' .
(($t1 = strval(interdire_scripts(table_valeur(@$Pile[0], (string)'message_ok', null))))!=='' ?
		('<p class="reponse_formulaire reponse_formulaire_ok">' . $t1 . '</p>') :
		'') .
'
' .
(($t1 = strval(interdire_scripts(table_valeur(@$Pile[0], (string)'message_erreur', null))))!=='' ?
		('<p class="reponse_formulaire reponse_formulaire_erreur">' . $t1 . '</p>') :
		'') .
'

	<form action="' .
interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'action', null),true)) .
'#formulaire_configurer_forums_notifications" method="post"><div>
		' .
	'<div>' .
	form_hidden(@$Pile[0]['action']) .
	'<input name=\'formulaire_action\' type=\'hidden\'
		value=\'' . @$Pile[0]['form'] . '\' />' .
	'<input name=\'formulaire_action_args\' type=\'hidden\'
		value=\'' . @$Pile[0]['formulaire_args']. '\' />' .
	(!empty($Pile[0]['_hidden']) ? @$Pile[0]['_hidden'] : '') .
	'</div>' .
'
		<p class=\'explication\'>' .
_T('forum:info_option_email') .
'</p>
		<div class="editer-groupe">
			<div class=\'editer configurer_prevenir_auteurs_pos' .
((table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),'prevenir_auteurs_pos'))  ?
		(' ' . ' ' . 'erreur') :
		'') .
'\'>
				' .
(($t1 = strval(table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),'prevenir_auteurs_pos')))!=='' ?
		('
				<span class=\'erreur_message\'>' . $t1 . '</span>
				') :
		'') .
'<div class=\'choix\'>
					<input class=\'checkbox\' type="checkbox" name="prevenir_auteurs_pos" value=\'1\' ' .
(($t1 = strval(interdire_scripts((entites_html(table_valeur(@$Pile[0], (string)'prevenir_auteurs_pos', null),true) ? 'checked':''))))!=='' ?
		('checked="' . $t1 . '"') :
		'') .
' id="prevenir_auteurs_pos"/>
					<label for="prevenir_auteurs_pos">' .
_T('forum:bouton_radio_modere_posteriori') .
'</label>
				</div>
			</div>
			<div class=\'editer configurer_prevenir_auteurs_pri' .
((table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),'prevenir_auteurs_pri'))  ?
		(' ' . ' ' . 'erreur') :
		'') .
'\'>
				' .
(($t1 = strval(table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),'prevenir_auteurs_pri')))!=='' ?
		('
				<span class=\'erreur_message\'>' . $t1 . '</span>
				') :
		'') .
'<div class=\'choix\'>
					<input class=\'checkbox\' type="checkbox" name="prevenir_auteurs_pri" value=\'1\' ' .
(($t1 = strval(interdire_scripts((entites_html(table_valeur(@$Pile[0], (string)'prevenir_auteurs_pri', null),true) ? 'checked':''))))!=='' ?
		('checked="' . $t1 . '"') :
		'') .
' id="prevenir_auteurs_pri"/>
					<label for="prevenir_auteurs_pri">' .
_T('forum:bouton_radio_modere_priori') .
'</label>
				</div>
			</div>
			<div class=\'editer configurer_prevenir_auteurs_abo' .
((table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),'prevenir_auteurs_abo'))  ?
		(' ' . ' ' . 'erreur') :
		'') .
'\'>
				' .
(($t1 = strval(table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),'prevenir_auteurs_abo')))!=='' ?
		('
				<span class=\'erreur_message\'>' . $t1 . '</span>
				') :
		'') .
'<div class=\'choix\'>
					<input class=\'checkbox\' type="checkbox" name="prevenir_auteurs_abo" value=\'1\' ' .
(($t1 = strval(interdire_scripts((entites_html(table_valeur(@$Pile[0], (string)'prevenir_auteurs_abo', null),true) ? 'checked':''))))!=='' ?
		('checked="' . $t1 . '"') :
		'') .
' id="prevenir_auteurs_abo"/>
					<label for="prevenir_auteurs_abo">' .
_T('forum:bouton_radio_modere_abonnement') .
'</label>
				</div>
			</div>
		</div>
		<p class=\'boutons\'><input class=\'submit\' type="submit" name="ok" value="' .
_T('public|spip|ecrire:bouton_enregistrer') .
'"/></p>
	</div></form>

</div>');

	return analyse_resultat_skel('html_de7330d0e3aaa74c03b194f84667084c', $Cache, $page, '../plugins-dist/forum/formulaires/configurer_forums_notifications.html');
}
?>