<?php

/*
 * Squelette : ../plugins-dist/forum/formulaires/configurer_forums_prives.html
 * Date :      Tue, 21 Jan 2020 17:16:37 GMT
 * Compile :   Wed, 22 Jan 2020 22:24:34 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette ../plugins-dist/forum/formulaires/configurer_forums_prives.html
// Temps de compilation total: 3.896 ms
//

function html_6977c2ae20bf7a6df154b37be496bf7a($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'<div class=\'formulaire_spip formulaire_configurer formulaire_configurer_forums_prives\' id=\'formulaire_configurer_forums_prives\'>
<h3 class=\'titrem\'>' .
interdire_scripts(filtre_balise_img_dist(chemin_image('forum-interne-24.png'),'','cadre-icone')) .
_T('forum:titre_config_forums_prive') .
'</h3>

' .
(($t1 = strval(interdire_scripts(table_valeur(@$Pile[0], (string)'message_ok', null))))!=='' ?
		('<p class="reponse_formulaire reponse_formulaire_ok">' . $t1 . '</p>') :
		'') .
'
' .
(($t1 = strval(interdire_scripts(table_valeur(@$Pile[0], (string)'message_erreur', null))))!=='' ?
		('<p class="reponse_formulaire reponse_formulaire_erreur">' . $t1 . '</p>') :
		'') .
'

	<form action="' .
interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'action', null),true)) .
'#formulaire_configurer_forums_prives" method="post"><div>
		' .
	'<div>' .
	form_hidden(@$Pile[0]['action']) .
	'<input name=\'formulaire_action\' type=\'hidden\'
		value=\'' . @$Pile[0]['form'] . '\' />' .
	'<input name=\'formulaire_action_args\' type=\'hidden\'
		value=\'' . @$Pile[0]['formulaire_args']. '\' />' .
	(!empty($Pile[0]['_hidden']) ? @$Pile[0]['_hidden'] : '') .
	'</div>' .
'
		<p class=\'explication\'>' .
_T('forum:info_config_forums_prive') .
'</p>
		<div class="editer-groupe">
			' .
vide($Pile['vars'][$_zzz=(string)'name'] = 'forum_prive_objets') .
vide($Pile['vars'][$_zzz=(string)'erreurs'] = table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),table_valeur($Pile["vars"], (string)'name', null))) .
'<div class="editer long_label editer_' .
table_valeur($Pile["vars"], (string)'name', null) .
(($t1 = strval(table_valeur($Pile["vars"], (string)'obli', null)))!=='' ?
		(' ' . $t1) :
		'') .
((table_valeur($Pile["vars"], (string)'erreurs', null))  ?
		(' ' . ' ' . 'erreur') :
		'') .
'">
				<label>' .
_T('forum:item_config_forums_prive_objets') .
'</label>' .
(($t1 = strval(table_valeur($Pile["vars"], (string)'erreurs', null)))!=='' ?
		('
				<span class=\'erreur_message\'>' . $t1 . '</span>
				') :
		'') .
recuperer_fond( 'formulaires/inc-choisir-objets' , array('name' => table_valeur($Pile["vars"], (string)'name', null) ,
	'selected' => table_valeur(@$Pile[0], (string)table_valeur($Pile["vars"], (string)'name', null), null) ,
	'exclus' => 'spip_forum' ), array('compil'=>array('../plugins-dist/forum/formulaires/configurer_forums_prives.html','html_6977c2ae20bf7a6df154b37be496bf7a','',13,$GLOBALS['spip_lang'])), _request('connect')) .
'</div>
			<div class=\'editer long_label configurer_forum_prive' .
((table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),'forum_prive'))  ?
		(' ' . ' ' . 'erreur') :
		'') .
'\'>
				<label>' .
_T('forum:info_config_forums_prive_global') .
'</label>' .
(($t1 = strval(table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),'forum_prive')))!=='' ?
		('
				<span class=\'erreur_message\'>' . $t1 . '</span>
				') :
		'') .
'<div class=\'choix\'>
					<input class=\'radio\' type="radio" name="forum_prive" value=\'oui\' ' .
(($t1 = strval(interdire_scripts(((entites_html(table_valeur(@$Pile[0], (string)'forum_prive', null),true) == 'oui') ? 'checked':''))))!=='' ?
		('checked="' . $t1 . '"') :
		'') .
' id="forum_prive_oui"/>
					<label for="forum_prive_oui">' .
_T('forum:item_config_forums_prive_global') .
'</label>
				</div>
				<div class=\'choix\'>
					<input class=\'radio\' type="radio" name="forum_prive" value=\'non\' ' .
(($t1 = strval(interdire_scripts(((entites_html(table_valeur(@$Pile[0], (string)'forum_prive', null),true) == 'oui') ? '':'checked'))))!=='' ?
		('checked="' . $t1 . '"') :
		'') .
' id="forum_prive_non"/>
					<label for="forum_prive_non">' .
_T('forum:item_non_config_forums_prive_global') .
'</label>
				</div>
			</div>
			<div class=\'editer long_label configurer_forum_prive_admin' .
((table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),'forum_prive_admin'))  ?
		(' ' . ' ' . 'erreur') :
		'') .
'\'>
				<label>' .
_T('forum:info_config_forums_prive_admin') .
'</label>' .
(($t1 = strval(table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),'forum_prive_admin')))!=='' ?
		('
				<span class=\'erreur_message\'>' . $t1 . '</span>
				') :
		'') .
'<div class=\'choix\'>
					<input class=\'radio\' type="radio" name="forum_prive_admin" value=\'oui\' ' .
(($t1 = strval(interdire_scripts(((entites_html(table_valeur(@$Pile[0], (string)'forum_prive_admin', null),true) == 'oui') ? 'checked':''))))!=='' ?
		('checked="' . $t1 . '"') :
		'') .
' id="forum_prive_admin_oui"/>
					<label for="forum_prive_admin_oui">' .
_T('forum:item_activer_forum_administrateur') .
'</label>
				</div>
				<div class=\'choix\'>
					<input class=\'radio\' type="radio" name="forum_prive_admin" value=\'non\' ' .
(($t1 = strval(interdire_scripts(((entites_html(table_valeur(@$Pile[0], (string)'forum_prive_admin', null),true) == 'oui') ? '':'checked'))))!=='' ?
		('checked="' . $t1 . '"') :
		'') .
' id="forum_prive_admin_non"/>
					<label for="forum_prive_admin_non">' .
_T('forum:item_desactiver_forum_administrateur') .
'</label>
				</div>
			</div>
		</div>
		<p class=\'boutons\'><input class=\'submit\' type="submit" name="ok" value="' .
_T('public|spip|ecrire:bouton_enregistrer') .
'"/></p>
	</div></form>
	
</div>');

	return analyse_resultat_skel('html_6977c2ae20bf7a6df154b37be496bf7a', $Cache, $page, '../plugins-dist/forum/formulaires/configurer_forums_prives.html');
}
?>