<?php

/*
 * Squelette : ../plugins-dist/medias/prive/squelettes/top/documents.html
 * Date :      Tue, 21 Jan 2020 17:16:46 GMT
 * Compile :   Sat, 01 Feb 2020 20:13:58 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette ../plugins-dist/medias/prive/squelettes/top/documents.html
// Temps de compilation total: 0.388 ms
//

function html_2c2b6bba88785d08a55c06a23164fbf5($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'<h1 class="grostitre">' .
_T('medias:documents') .
'</h1>
' .
interdire_scripts((is_string('pleine_largeur')?vide($GLOBALS['largeur_ecran']='pleine_largeur'):(isset($GLOBALS['largeur_ecran'])?$GLOBALS['largeur_ecran']:''))));

	return analyse_resultat_skel('html_2c2b6bba88785d08a55c06a23164fbf5', $Cache, $page, '../plugins-dist/medias/prive/squelettes/top/documents.html');
}
?>