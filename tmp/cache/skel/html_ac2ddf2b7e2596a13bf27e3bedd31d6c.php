<?php

/*
 * Squelette : ../plugins-dist/medias/modeles/audio.html
 * Date :      Tue, 21 Jan 2020 17:16:47 GMT
 * Compile :   Sat, 01 Feb 2020 20:26:21 GMT
 * Boucles :   _tous
 */ 

function BOUCLE_toushtml_ac2ddf2b7e2596a13bf27e3bedd31d6c(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($command['table'])) {
		$command['table'] = 'documents';
		$command['id'] = '_tous';
		$command['from'] = array('documents' => 'spip_documents','L1' => 'spip_types_documents');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("documents.largeur",
		"documents.duree",
		"documents.id_document",
		"documents.fichier",
		"L1.mime_type",
		"documents.titre",
		"documents.descriptif");
		$command['orderby'] = array();
		$command['join'] = array('L1' => array('documents','extension'));
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array(
			array('(documents.taille > 0 OR documents.distant=\'oui\')'), 
			array('=', 'documents.id_document', sql_quote(interdire_scripts(@$Pile[0]['id']), '', 'bigint(21) NOT NULL AUTO_INCREMENT')));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('../plugins-dist/medias/modeles/audio.html','html_ac2ddf2b7e2596a13bf27e3bedd31d6c','_tous',1,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$t0 .= (
'
' .
vide($Pile['vars'][$_zzz=(string)'largeur'] = interdire_scripts(max(entites_html(sinon(table_valeur(@$Pile[0], (string)'largeur', null), interdire_scripts(($Pile[$SP]['largeur'] ? interdire_scripts($Pile[$SP]['largeur']):'400'))),true),'120'))) .
vide($Pile['vars'][$_zzz=(string)'duree'] = interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'duree', null), interdire_scripts($Pile[$SP]['duree'])),true))) .
vide($Pile['vars'][$_zzz=(string)'duree'] = (intval(table_valeur($Pile["vars"], (string)'duree', null)) ? intval(table_valeur($Pile["vars"], (string)'duree', null)):'')) .
'
<div	class=\'spip_document_' .
$Pile[$SP]['id_document'] .
' spip_document_audio spip_documents' .
(($t1 = strval(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'align', null),true))))!=='' ?
		(' spip_documents_' . $t1) :
		'') .
'\'
	style=\'' .
(($t1 = strval(interdire_scripts(((match(entites_html(table_valeur(@$Pile[0], (string)'align', null),true),'^(left|right)$')) ?' ' :''))))!=='' ?
		($t1 . (	'float:' .
	interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'align', null),true)) .
	';')) :
		'') .
(($t1 = strval(table_valeur($Pile["vars"], (string)'largeur', null)))!=='' ?
		('width:' . $t1 . 'px') :
		'') .
'\'>
	' .
vider_attribut(filtrer('image_graver', filtrer('image_reduire',quete_logo_document(quete_document($Pile[$SP]['id_document'], ''), '', '', 'vignette', 0, 0, ''),table_valeur($Pile["vars"], (string)'largeur', null),'0')),'class') .
'
<div class="audio-wrapper"' .
(($t1 = strval(table_valeur($Pile["vars"], (string)'largeur', null)))!=='' ?
		(' style=\'width:' . $t1 . 'px;max-width:100%;\'') :
		'') .
'>
	<audio class="mejs mejs-' .
$Pile[$SP]['id_document'] .
' ' .
(($t1 = strval(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'skin', null),true))))!=='' ?
		(' mejs-' . $t1) :
		'') .
'"
	       data-id="' .
md5(concat((	'mejs-' .
	$Pile[$SP]['id_document']),interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'skin', null),true)))) .
'"
	       src="' .
interdire_scripts(get_spip_doc($Pile[$SP]['fichier'])) .
'"
	       type="' .
interdire_scripts($Pile[$SP]['mime_type']) .
'"
		   preload="none"
	       data-mejsoptions=\'{"alwaysShowControls": true' .
(($t1 = strval(interdire_scripts((entites_html(table_valeur(@$Pile[0], (string)'loop', null),true) ? 'true':'false'))))!=='' ?
		(',"loop":' . $t1) :
		'') .
',"audioWidth":"100%"' .
(($t1 = strval(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'hauteur', null),true))))!=='' ?
		(',"audioHeight":"' . $t1 . '"') :
		'') .
(($t1 = strval(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'volume', null),true))))!=='' ?
		(',"startVolume":"' . $t1 . '"') :
		'') .
(($t1 = strval(table_valeur($Pile["vars"], (string)'duree', null)))!=='' ?
		(',"duration":' . $t1) :
		'') .
'}\'
	       controls="controls"
				 ' .
(($t1 = strval(interdire_scripts(((entites_html(table_valeur(@$Pile[0], (string)'autoplay', null),true)) ?' ' :''))))!=='' ?
		('autoplay="autoplay"' . $t1) :
		'') .
'></audio>
</div>
' .
(($t1 = strval(interdire_scripts(supprimer_numero(typo($Pile[$SP]['titre']), "TYPO", $connect, $Pile[0]))))!=='' ?
		((	'<div class=\'' .
	'spip_doc_titre\'><strong>') . $t1 . '</strong></div>') :
		'') .
'
' .
(($t1 = strval(interdire_scripts(PtoBR(propre($Pile[$SP]['descriptif'], $connect, $Pile[0])))))!=='' ?
		((	'<div class=\'' .
	'spip_doc_descriptif\'>') . $t1 . (	interdire_scripts(PtoBR(calculer_notes())) .
	'</div>')) :
		'') .
'
' .
(($t1 = strval(charge_scripts('javascript/mejs-init.min.js',false)))!=='' ?
		((	'<script>/*<![CDATA[*/var mejspath=\'' .
	timestamp(find_in_path('lib/mejs/mediaelement-and-player.min.js')) .
	'\',mejscss=\'' .
	timestamp(find_in_path('lib/mejs/mediaelementplayer.min.css')) .
	'\';
') . $t1 . '/*]]>*/</script>') :
		'') .
'
' .
(($t1 = strval(interdire_scripts(((entites_html(table_valeur(@$Pile[0], (string)'skin', null),true)) ?' ' :''))))!=='' ?
		($t1 . (($t2 = strval(charge_scripts(url_absolue_css(((($a = find_in_path((	'css/mejs-skin-' .
			interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'skin', null),true)) .
			'.css'))) OR (is_string($a) AND strlen($a))) ? $a : find_in_path('lib/mejs/mejs-skins.css'))),false)))!=='' ?
			('<style>' . $t2 . '</style>') :
			'')) :
		'') .
'
</div>
');
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_tous @ ../plugins-dist/medias/modeles/audio.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}

//
// Fonction principale du squelette ../plugins-dist/medias/modeles/audio.html
// Temps de compilation total: 21.020 ms
//

function html_ac2ddf2b7e2596a13bf27e3bedd31d6c($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = BOUCLE_toushtml_ac2ddf2b7e2596a13bf27e3bedd31d6c($Cache, $Pile, $doublons, $Numrows, $SP);

	return analyse_resultat_skel('html_ac2ddf2b7e2596a13bf27e3bedd31d6c', $Cache, $page, '../plugins-dist/medias/modeles/audio.html');
}
?>