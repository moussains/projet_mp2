<?php

/*
 * Squelette : ../plugins-dist/breves/prive/objets/infos/breve.html
 * Date :      Wed, 26 Feb 2020 10:55:57 GMT
 * Compile :   Mon, 09 Mar 2020 18:39:17 GMT
 * Boucles :   _publiee, _breve
 */ 

function BOUCLE_publieehtml_c39b7d358938d70c7d036b1a5b33024a(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($command['table'])) {
		$command['table'] = 'breves';
		$command['id'] = '_publiee';
		$command['from'] = array('breves' => 'spip_breves');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("breves.id_breve",
		"breves.lang",
		"breves.titre");
		$command['orderby'] = array();
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array(
quete_condition_statut('breves.statut','publie,prop','publie',''), 
			array('=', 'breves.id_breve', sql_quote($Pile[$SP]['id_breve'], '','bigint(21) NOT NULL AUTO_INCREMENT')));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('../plugins-dist/breves/prive/objets/infos/breve.html','html_c39b7d358938d70c7d036b1a5b33024a','_publiee',13,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	lang_select($GLOBALS['spip_lang']);
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		lang_select_public($Pile[$SP]['lang'], '', $Pile[$SP]['titre']);
		$t0 .= (
'
	' .
filtre_icone_horizontale_dist(parametre_url(generer_url_action('redirect',(	'type=breve&id=' .
	$Pile[$SP]['id_breve'])),'var_mode','calcul'),_T('public|spip|ecrire:icone_voir_en_ligne'),'racine') .
'
');
		lang_select();
	}
	lang_select();
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_publiee @ ../plugins-dist/breves/prive/objets/infos/breve.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}


function BOUCLE_brevehtml_c39b7d358938d70c7d036b1a5b33024a(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($command['table'])) {
		$command['table'] = 'breves';
		$command['id'] = '_breve';
		$command['from'] = array('breves' => 'spip_breves');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("breves.id_breve",
		"breves.statut");
		$command['orderby'] = array();
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array(
			array('=', 'breves.id_breve', sql_quote(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'id', null),true)), '', 'bigint(21) NOT NULL AUTO_INCREMENT')), 
			array('REGEXP', 'breves.statut', "'.*'"));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('../plugins-dist/breves/prive/objets/infos/breve.html','html_c39b7d358938d70c7d036b1a5b33024a','_breve',1,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	
	$l1 = _T('breves:info_gauche_numero_breve');
	$l2 = _T('public|spip|ecrire:previsualiser');$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$t0 .= (
'
<div class=\'infos\'>
<div class=\'numero\'>' .
$l1 .
'<p>' .
$Pile[$SP]['id_breve'] .
'</p></div>

' .
executer_balise_dynamique('FORMULAIRE_INSTITUER_OBJET',
	array('breve',$Pile[$SP]['id_breve']),
	array('../plugins-dist/breves/prive/objets/infos/breve.html','html_c39b7d358938d70c7d036b1a5b33024a','_breve',5,$GLOBALS['spip_lang'])) .
'
' .
(($t1 = BOUCLE_publieehtml_c39b7d358938d70c7d036b1a5b33024a($Cache, $Pile, $doublons, $Numrows, $SP))!=='' ?
		$t1 :
		((	'
	' .
	(($t2 = strval(invalideur_session($Cache, ((function_exists("autoriser")||include_spip("inc/autoriser"))&&autoriser('previsualiser', 'breve', invalideur_session($Cache, $Pile[$SP]['id_breve']), '', invalideur_session($Cache, array('statut' => interdire_scripts(invalideur_session($Cache, $Pile[$SP]['statut'])))))?" ":""))))!=='' ?
			($t2 . (	'
		' .
		filtre_icone_horizontale_dist(parametre_url(generer_url_action('redirect',(	'type=breve&id=' .
			$Pile[$SP]['id_breve'])),'var_mode','preview'),$l2,'preview') .
		'
	')) :
			'') .
	'
'))) .
'

</div>
');
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_breve @ ../plugins-dist/breves/prive/objets/infos/breve.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}

//
// Fonction principale du squelette ../plugins-dist/breves/prive/objets/infos/breve.html
// Temps de compilation total: 24.852 ms
//

function html_c39b7d358938d70c7d036b1a5b33024a($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = BOUCLE_brevehtml_c39b7d358938d70c7d036b1a5b33024a($Cache, $Pile, $doublons, $Numrows, $SP);

	return analyse_resultat_skel('html_c39b7d358938d70c7d036b1a5b33024a', $Cache, $page, '../plugins-dist/breves/prive/objets/infos/breve.html');
}
?>