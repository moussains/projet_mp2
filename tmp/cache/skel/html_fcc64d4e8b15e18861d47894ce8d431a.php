<?php

/*
 * Squelette : ../plugins/auto/menus_1/formulaires/editer_menu.html
 * Date :      Fri, 20 Dec 2019 12:26:26 GMT
 * Compile :   Mon, 20 Jan 2020 15:05:43 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette ../plugins/auto/menus_1/formulaires/editer_menu.html
// Temps de compilation total: 5.515 ms
//

function html_fcc64d4e8b15e18861d47894ce8d431a($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'<div class="formulaire_spip formulaire_editer ' .
(($t1 = strval(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'form', null),true))))!=='' ?
		('formulaire_' . $t1) :
		'') .
(($t1 = strval(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'form', null),true))))!=='' ?
		(' formulaire_' . $t1 . (	'-' .
	interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'id_menu', null), 'nouveau'),true)))) :
		'') .
'">
	' .
(($t1 = strval(interdire_scripts(table_valeur(@$Pile[0], (string)'message_ok', null))))!=='' ?
		('<p class="reponse_formulaire reponse_formulaire_ok">' . $t1 . '</p>') :
		'') .
'
	' .
(($t1 = strval(interdire_scripts(table_valeur(@$Pile[0], (string)'message_erreur', null))))!=='' ?
		('<p class="reponse_formulaire reponse_formulaire_erreur">' . $t1 . '</p>') :
		'') .
'

	' .
(($t1 = strval(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'editable', null),true))))!=='' ?
		($t1 . (	'
		<form method=\'post\' action=\'' .
	interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'action', null),true)) .
	'\' enctype=\'multipart/form-data\'><div>
			
			' .
		'<div>' .
	form_hidden(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'action', null),true))) .
	'<input name=\'formulaire_action\' type=\'hidden\'
		value=\'' . @$Pile[0]['form'] . '\' />' .
	'<input name=\'formulaire_action_args\' type=\'hidden\'
		value=\'' . @$Pile[0]['formulaire_args']. '\' />' .
	(!empty($Pile[0]['_hidden']) ? @$Pile[0]['_hidden'] : '') .
	'</div><ul class="editer-groupe">
				<li class="fieldset">
					<fieldset>
						<h3 class="legend">' .
	_T('menus:formulaire_partie_identification') .
	'</h3>
						<ul class="editer-groupe">
							' .
	vide($Pile['vars'][$_zzz=(string)'obli'] = 'obligatoire') .
	vide($Pile['vars'][$_zzz=(string)'name'] = 'titre') .
	vide($Pile['vars'][$_zzz=(string)'erreurs'] = table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),table_valeur($Pile["vars"], (string)'name', null))) .
	'<li class="editer editer_' .
	table_valeur($Pile["vars"], (string)'name', null) .
	(($t2 = strval(table_valeur($Pile["vars"], (string)'obli', null)))!=='' ?
			(' ' . $t2) :
			'') .
	((table_valeur($Pile["vars"], (string)'erreurs', null))  ?
			(' ' . ' ' . 'erreur') :
			'') .
	'">
								<label for="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'">' .
	_T('menus:formulaire_titre') .
	'</label>
								' .
	(($t2 = strval(table_valeur($Pile["vars"], (string)'erreurs', null)))!=='' ?
			('<span class=\'erreur_message\'>' . $t2 . '</span>') :
			'') .
	'
								<input type="text" class="text multilang" name="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'" id="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'" size="40" value="' .
	interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)table_valeur($Pile["vars"], (string)'name', null), null),true)) .
	'" />
							</li>

							' .
	vide($Pile['vars'][$_zzz=(string)'name'] = 'identifiant') .
	vide($Pile['vars'][$_zzz=(string)'erreurs'] = table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),table_valeur($Pile["vars"], (string)'name', null))) .
	'<li class="editer editer_' .
	table_valeur($Pile["vars"], (string)'name', null) .
	(($t2 = strval(table_valeur($Pile["vars"], (string)'obli', null)))!=='' ?
			(' ' . $t2) :
			'') .
	((table_valeur($Pile["vars"], (string)'erreurs', null))  ?
			(' ' . ' ' . 'erreur') :
			'') .
	'">
								<label for="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'">' .
	_T('menus:formulaire_identifiant') .
	'</label>
								<div class="explication">' .
	_T('menus:formulaire_identifiant_explication') .
	'</div>
								' .
	(($t2 = strval(table_valeur($Pile["vars"], (string)'erreurs', null)))!=='' ?
			('<span class=\'erreur_message\'>' . $t2 . '</span>') :
			'') .
	'
								<input type="text" class="text" name="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'" id="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'" size="40" value="' .
	interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)table_valeur($Pile["vars"], (string)'name', null), null),true)) .
	'" />
							</li>

							' .
	vide($Pile['vars'][$_zzz=(string)'name'] = 'css') .
	vide($Pile['vars'][$_zzz=(string)'erreurs'] = table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),table_valeur($Pile["vars"], (string)'name', null))) .
	'<li class="editer editer_' .
	table_valeur($Pile["vars"], (string)'name', null) .
	((table_valeur($Pile["vars"], (string)'erreurs', null))  ?
			(' ' . ' ' . 'erreur') :
			'') .
	'">
								<label for="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'">' .
	_T('menus:formulaire_css') .
	'</label>
								<div class="explication">' .
	_T('menus:formulaire_css_explication') .
	'</div>
								' .
	(($t2 = strval(table_valeur($Pile["vars"], (string)'erreurs', null)))!=='' ?
			('<span class=\'erreur_message\'>' . $t2 . '</span>') :
			'') .
	'
								<input type="text" class="text" name="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'" id="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'" size="40" value="' .
	interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)table_valeur($Pile["vars"], (string)'name', null), null),true)) .
	'" />
							</li>

							
							' .
	(($t2 = strval(interdire_scripts(((entites_html(table_valeur(@$Pile[0], (string)'nouveau', null),true)) ?' ' :''))))!=='' ?
			($t2 . (	'
								' .
		(($t3 = strval(interdire_scripts(((filtre_info_plugin_dist("yaml", "est_actif")) ?' ' :''))))!=='' ?
				($t3 . (	'
									' .
			vide($Pile['vars'][$_zzz=(string)'name'] = 'import') .
			vide($Pile['vars'][$_zzz=(string)'erreurs'] = table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),table_valeur($Pile["vars"], (string)'name', null))) .
			'<li class="editer editer_' .
			table_valeur($Pile["vars"], (string)'name', null) .
			((table_valeur($Pile["vars"], (string)'erreurs', null))  ?
					(' ' . ' ' . 'erreur') :
					'') .
			'">
										<label for="' .
			table_valeur($Pile["vars"], (string)'name', null) .
			'">' .
			_T('menus:formulaire_importer') .
			'</label>
										<div class="explication">' .
			_T('menus:formulaire_importer_explication') .
			'</div>
										' .
			(($t4 = strval(table_valeur($Pile["vars"], (string)'erreurs', null)))!=='' ?
					('<span class=\'erreur_message\'>' . $t4 . '</span>') :
					'') .
			'
										<input type="file" class="file" name="' .
			table_valeur($Pile["vars"], (string)'name', null) .
			'" id="' .
			table_valeur($Pile["vars"], (string)'name', null) .
			'" size="20" value="' .
			interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)table_valeur($Pile["vars"], (string)'name', null), null),true)) .
			'" />
									</li>
								')) :
				'') .
		'
							')) :
			'') .
	'
						</ul>

						<p class=\'boutons\'>
							<input type=\'submit\' class=\'submit\' value=\'' .
	_T('public|spip|ecrire:bouton_enregistrer') .
	'\' />
						</p>
					</fieldset>
				</li>
			</ul>
		</div></form>
		')) :
		'') .
'
</div>

' .
(($t1 = strval(interdire_scripts(((entites_html(table_valeur(@$Pile[0], (string)'nouveau', null),true)) ?'' :' '))))!=='' ?
		($t1 . (	'
<script type="text/javascript">
jQuery(function(){
	jQuery(\'.formulaire_' .
	interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'form', null),true)) .
	'\'+\' .boutons\').hide();
	jQuery(\'.formulaire_' .
	interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'form', null),true)) .
	'\'+\' input\')
		.change(function(){jQuery(\'.formulaire_' .
	interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'form', null),true)) .
	'\'+\' .boutons\').slideDown();})
		.keydown(function(){jQuery(\'.formulaire_' .
	interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'form', null),true)) .
	'\'+\' .boutons\').slideDown();});
});
</script>
')) :
		''));

	return analyse_resultat_skel('html_fcc64d4e8b15e18861d47894ce8d431a', $Cache, $page, '../plugins/auto/menus_1/formulaires/editer_menu.html');
}
?>