<?php

/*
 * Squelette : ../plugins/auto/menus_1/prive/squelettes/contenu/menu_edit.html
 * Date :      Fri, 20 Dec 2019 12:26:26 GMT
 * Compile :   Mon, 20 Jan 2020 15:05:43 GMT
 * Boucles :   _menu
 */ 

function BOUCLE_menuhtml_985b655a0710d5ebc78c5c16049a0cee(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($command['table'])) {
		$command['table'] = 'menus';
		$command['id'] = '_menu';
		$command['from'] = array('menus' => 'spip_menus');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("menus.titre");
		$command['orderby'] = array();
		$command['where'] = 
			array(
			array('=', 'menus.id_menu', sql_quote(_request('id_menu'), '', 'bigint(21) NOT NULL AUTO_INCREMENT')));
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('../plugins/auto/menus_1/prive/squelettes/contenu/menu_edit.html','html_985b655a0710d5ebc78c5c16049a0cee','_menu',7,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$t0 .= (
'
			' .
interdire_scripts(supprimer_numero(typo($Pile[$SP]['titre']), "TYPO", $connect, $Pile[0])) .
'
			');
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_menu @ ../plugins/auto/menus_1/prive/squelettes/contenu/menu_edit.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}

//
// Fonction principale du squelette ../plugins/auto/menus_1/prive/squelettes/contenu/menu_edit.html
// Temps de compilation total: 3.869 ms
//

function html_985b655a0710d5ebc78c5c16049a0cee($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'
<div class="cadre-formulaire-editer">
	<div class="entete-formulaire">
		' .
interdire_scripts(filtre_icone_verticale_dist(entites_html(sinon(table_valeur(@$Pile[0], (string)'redirect', null), generer_url_ecrire('menus')),true),_T('public|spip|ecrire:retour'),'menus-24','','left')) .
'
		' .
_T('menus:formulaire_modifier_menu') .
'
		<h1>
			' .
(($t1 = BOUCLE_menuhtml_985b655a0710d5ebc78c5c16049a0cee($Cache, $Pile, $doublons, $Numrows, $SP))!=='' ?
		$t1 :
		((	'
			' .
	interdire_scripts(((entites_html(table_valeur(@$Pile[0], (string)'id_menu', null),true) == 'new') ? _T('menus:formulaire_nouveau'):_request('id_menu'))) .
	'
			'))) .
'
		</h1>
	</div>
	' .
executer_balise_dynamique('FORMULAIRE_EDITER_MENU',
	array(interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'id_menu', null), ''),true)),interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'redirect', null),true)),interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'associer_objet', null),true))),
	array('../plugins/auto/menus_1/prive/squelettes/contenu/menu_edit.html','html_985b655a0710d5ebc78c5c16049a0cee','',14,$GLOBALS['spip_lang'])) .
(($t1 = strval(interdire_scripts(((intval(entites_html(sinon(table_valeur(@$Pile[0], (string)'id_menu', null), '0'),true))) ?' ' :''))))!=='' ?
		($t1 . (	'
		<div class="ajax">' .
	executer_balise_dynamique('FORMULAIRE_EDITER_MENUS_ENTREE',
	array(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'id_menu', null),true))),
	array('../plugins/auto/menus_1/prive/squelettes/contenu/menu_edit.html','html_985b655a0710d5ebc78c5c16049a0cee','',16,$GLOBALS['spip_lang'])) .
	'</div>
	')) :
		'') .
'

</div>');

	return analyse_resultat_skel('html_985b655a0710d5ebc78c5c16049a0cee', $Cache, $page, '../plugins/auto/menus_1/prive/squelettes/contenu/menu_edit.html');
}
?>