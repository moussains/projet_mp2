<?php

/*
 * Squelette : plugins/auto/menus_1/inclure/nav.html
 * Date :      Fri, 20 Dec 2019 12:26:26 GMT
 * Compile :   Wed, 22 Jan 2020 13:49:20 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette plugins/auto/menus_1/inclure/nav.html
// Temps de compilation total: 0.251 ms
//

function html_2f4de6e3bde6924686d40830abea328d($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'<div class="menu-conteneur navbar-inner">
' .
recuperer_fond( 'inclure/menu' , array_merge($Pile[0],array('identifiant' => 'barrenav' )), array('compil'=>array('plugins/auto/menus_1/inclure/nav.html','html_2f4de6e3bde6924686d40830abea328d','',2,$GLOBALS['spip_lang'])), _request('connect')) .
'</div>
');

	return analyse_resultat_skel('html_2f4de6e3bde6924686d40830abea328d', $Cache, $page, 'plugins/auto/menus_1/inclure/nav.html');
}
?>