<?php

/*
 * Squelette : squelettes/inclure/nav.html
 * Date :      Thu, 16 Apr 2020 18:17:41 GMT
 * Compile :   Thu, 16 Apr 2020 20:44:01 GMT
 * Boucles :   _nav
 */ 

function BOUCLE_navhtml_6af9a242c01fd8ede4fd4a03f74d3910(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($command['table'])) {
		$command['table'] = 'menus_entrees';
		$command['id'] = '_nav';
		$command['from'] = array('menus_entrees' => 'spip_menus_entrees');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("menus_entrees.id_menus_entree",
		"menus_entrees.parametres");
		$command['orderby'] = array('menus_entrees.id_menus_entree');
		$command['where'] = 
			array();
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('squelettes/inclure/nav.html','html_6af9a242c01fd8ede4fd4a03f74d3910','_nav',1,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	
	// COMPTEUR
	$Numrows['_nav']['compteur_boucle'] = 0;
	$Numrows['_nav']['total'] = @intval($iter->count());
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$Numrows['_nav']['compteur_boucle']++;
		$t0 .= (
'
        ' .
vide($Pile['vars'][$_zzz=(string)'un_tableau'] = interdire_scripts($Pile[$SP]['parametres'])) .
'<div class="col p-0 text-center">
            <a href="' .
vider_url(urlencode_1738(generer_url_entite(@$Pile[0]['id_rubrique'], 'rubrique', '', '', true))) .
'">
                <li class=" list-group-item nav-item' .
(calcul_exposer($Pile[$SP]['id_menus_entree'], 'id_menus_entree', $Pile[0], '', 'id_menus_entree', '')  ?
		(' ' . 'on') :
		'') .
((($Numrows['_nav']['compteur_boucle'] == '1'))  ?
		(' ' . ' ' . 'first') :
		'') .
((($Numrows['_nav']['compteur_boucle'] == $Numrows['_nav']['total']))  ?
		(' ' . ' ' . 'last') :
		'') .
'">
                  ' .
table_valeur(table_valeur($Pile["vars"], (string)'un_tableau', null),'titre') .
'
                </li>
            </a>
        </div>
  		');
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_nav @ squelettes/inclure/nav.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}

//
// Fonction principale du squelette squelettes/inclure/nav.html
// Temps de compilation total: 0.536 ms
//

function html_6af9a242c01fd8ede4fd4a03f74d3910($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
(($t1 = BOUCLE_navhtml_6af9a242c01fd8ede4fd4a03f74d3910($Cache, $Pile, $doublons, $Numrows, $SP))!=='' ?
		((	'
  <div class="row nav clearfix' .
		((($Numrows['_nav']['total'] == '1'))  ?
				(' ' . ' ' . 'none') :
				'') .
		' mb-2" id="nav">
  	<ul class="w-100 list-group list-group-horizontal">
  		') . $t1 . '
  	</ul>
  </div>
') :
		'') .
'
');

	return analyse_resultat_skel('html_6af9a242c01fd8ede4fd4a03f74d3910', $Cache, $page, 'squelettes/inclure/nav.html');
}
?>