<?php

/*
 * Squelette : squelettes/atlas.html
 * Date :      Tue, 10 Mar 2020 19:02:13 GMT
 * Compile :   Thu, 16 Apr 2020 16:20:41 GMT
 * Boucles :   _mots, _groupes_mots, _mots_strates, _groupes_mots_strates, _mots_thematiques, _mots_auteurs, _groupes_mots_auteurs
 */ 

function BOUCLE_motshtml_2affb1faea87cab8e441a4c2fcb0a65e(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($command['table'])) {
		$command['table'] = 'mots';
		$command['id'] = '_mots';
		$command['from'] = array('mots' => 'spip_mots');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("mots.titre",
		"mots.id_mot");
		$command['orderby'] = array('mots.titre');
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array((
'id_groupe=' .
$Pile[$SP]['id_groupe']));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('squelettes/atlas.html','html_2affb1faea87cab8e441a4c2fcb0a65e','_mots',87,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$t0 .= (
'
												<div class="form-check">
													<input class="form-check-input" name="motcle" type="checkbox" value="' .
interdire_scripts(supprimer_numero(typo($Pile[$SP]['titre']), "TYPO", $connect, $Pile[0])) .
'" id="Check' .
$Pile[$SP]['id_mot'] .
'">
													<label class="form-check-label" for="Check' .
$Pile[$SP]['id_mot'] .
'">' .
interdire_scripts(supprimer_numero(typo($Pile[$SP]['titre']), "TYPO", $connect, $Pile[0])) .
'</label>
												</div>
						                    ');
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_mots @ squelettes/atlas.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}


function BOUCLE_groupes_motshtml_2affb1faea87cab8e441a4c2fcb0a65e(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($command['table'])) {
		$command['table'] = 'groupes_mots';
		$command['id'] = '_groupes_mots';
		$command['from'] = array('groupes_mots' => 'spip_groupes_mots');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("groupes_mots.id_groupe",
		"groupes_mots.titre");
		$command['orderby'] = array('groupes_mots.titre');
		$command['where'] = 
			array('titre="Auteurs" OR titre="Époques" OR titre="Genres"');
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('squelettes/atlas.html','html_2affb1faea87cab8e441a4c2fcb0a65e','_groupes_mots',78,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$t0 .= (
'
								<div class="card">
									<div class="card-header">
										<a style="float:right;" class="btn-turn-icon card-link" data-toggle="collapse" href="#collapseOne' .
$Pile[$SP]['id_groupe'] .
'">
											<h3><i class="fas fa-arrow-circle-right rotate"></i></h3>
										</a><h4>' .
interdire_scripts(supprimer_numero(typo($Pile[$SP]['titre']), "TYPO", $connect, $Pile[0])) .
'</h4>
									</div>
									<div id="collapseOne' .
$Pile[$SP]['id_groupe'] .
'" class="collapse" data-parent="#accordion">
										<div class="card-body">
						                    ' .
BOUCLE_motshtml_2affb1faea87cab8e441a4c2fcb0a65e($Cache, $Pile, $doublons, $Numrows, $SP) .
'
										</div>
									</div>
								</div>
								');
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_groupes_mots @ squelettes/atlas.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}


function BOUCLE_mots_strateshtml_2affb1faea87cab8e441a4c2fcb0a65e(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($command['table'])) {
		$command['table'] = 'mots';
		$command['id'] = '_mots_strates';
		$command['from'] = array('mots' => 'spip_mots');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("mots.titre",
		"mots.id_mot");
		$command['orderby'] = array('mots.titre');
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array((
'id_groupe=' .
$Pile[$SP]['id_groupe']));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('squelettes/atlas.html','html_2affb1faea87cab8e441a4c2fcb0a65e','_mots_strates',122,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$t0 .= (
'
						                    	<option value="' .
$Pile[$SP]['id_mot'] .
'"><a class="dropdown-item" href="#">' .
interdire_scripts(supprimer_numero(typo($Pile[$SP]['titre']), "TYPO", $connect, $Pile[0])) .
'</a></option>
						                    ');
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_mots_strates @ squelettes/atlas.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}


function BOUCLE_groupes_mots_strateshtml_2affb1faea87cab8e441a4c2fcb0a65e(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($command['table'])) {
		$command['table'] = 'groupes_mots';
		$command['id'] = '_groupes_mots_strates';
		$command['from'] = array('groupes_mots' => 'spip_groupes_mots');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("groupes_mots.id_groupe");
		$command['orderby'] = array();
		$command['where'] = 
			array('titre="Strates"');
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('squelettes/atlas.html','html_2affb1faea87cab8e441a4c2fcb0a65e','_groupes_mots_strates',119,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$t0 .= (
'
									<div class="form-group">
										<select class="liste_strates form-control form-control-lg" multiple="multiple">
						                    ' .
BOUCLE_mots_strateshtml_2affb1faea87cab8e441a4c2fcb0a65e($Cache, $Pile, $doublons, $Numrows, $SP) .
'
						                </select>
									</div>
								');
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_groupes_mots_strates @ squelettes/atlas.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}


function BOUCLE_mots_thematiqueshtml_2affb1faea87cab8e441a4c2fcb0a65e(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($command['table'])) {
		$command['table'] = 'mots';
		$command['id'] = '_mots_thematiques';
		$command['from'] = array('mots' => 'spip_mots');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("mots.titre");
		$command['orderby'] = array();
		$command['where'] = 
			array('id_groupe="5"');
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('squelettes/atlas.html','html_2affb1faea87cab8e441a4c2fcb0a65e','_mots_thematiques',131,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$t0 .= (
'
					        		<div class="col">
					                	<a class="btn h6 btn-primary" href="#">' .
interdire_scripts(supprimer_numero(typo($Pile[$SP]['titre']), "TYPO", $connect, $Pile[0])) .
'</a>
						        	</div>
							');
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_mots_thematiques @ squelettes/atlas.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}


function BOUCLE_mots_auteurshtml_2affb1faea87cab8e441a4c2fcb0a65e(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($command['table'])) {
		$command['table'] = 'mots';
		$command['id'] = '_mots_auteurs';
		$command['from'] = array('mots' => 'spip_mots');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("mots.id_mot",
		"mots.titre");
		$command['orderby'] = array();
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array((
'id_groupe=' .
$Pile[$SP]['id_groupe']));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('squelettes/atlas.html','html_2affb1faea87cab8e441a4c2fcb0a65e','_mots_auteurs',245,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$t0 .= (
'\\
														<a href="?mot' .
$Pile[$SP]['id_mot'] .
'" class="list-group-item list-group-item-action list-group-item-dark">' .
interdire_scripts(supprimer_numero(typo($Pile[$SP]['titre']), "TYPO", $connect, $Pile[0])) .
'</a>\\
											  	');
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_mots_auteurs @ squelettes/atlas.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}


function BOUCLE_groupes_mots_auteurshtml_2affb1faea87cab8e441a4c2fcb0a65e(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($command['table'])) {
		$command['table'] = 'groupes_mots';
		$command['id'] = '_groupes_mots_auteurs';
		$command['from'] = array('groupes_mots' => 'spip_groupes_mots');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("groupes_mots.id_groupe");
		$command['orderby'] = array();
		$command['where'] = 
			array('titre="Auteurs"');
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('squelettes/atlas.html','html_2affb1faea87cab8e441a4c2fcb0a65e','_groupes_mots_auteurs',244,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$t0 .= (
'\\
											  	' .
BOUCLE_mots_auteurshtml_2affb1faea87cab8e441a4c2fcb0a65e($Cache, $Pile, $doublons, $Numrows, $SP) .
'\\
											');
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_groupes_mots_auteurs @ squelettes/atlas.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}

//
// Fonction principale du squelette squelettes/atlas.html
// Temps de compilation total: 16.031 ms
//

function html_2affb1faea87cab8e441a4c2fcb0a65e($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

 
<html dir="' .
lang_dir(@$Pile[0]['lang'], 'ltr','rtl') .
'" lang="' .
spip_htmlentities(@$Pile[0]['lang'] ? @$Pile[0]['lang'] : $GLOBALS['spip_lang']) .
'" xmlns="http://www.w3.org/1999/xhtml" xml:lang="' .
spip_htmlentities(@$Pile[0]['lang'] ? @$Pile[0]['lang'] : $GLOBALS['spip_lang']) .
'" class="' .
lang_dir(@$Pile[0]['lang'], 'ltr','rtl') .
(($t1 = strval(spip_htmlentities(@$Pile[0]['lang'] ? @$Pile[0]['lang'] : $GLOBALS['spip_lang'])))!=='' ?
		(' ' . $t1) :
		'') .
' no-js">

   	<head>
      	<script type=\'text/javascript\'>(function(H){H.className=H.className.replace(/\\bno-js\\b/,\'js\')})(document.documentElement);</script>
      	<title>' .
interdire_scripts(textebrut(typo($GLOBALS['meta']['nom_site'], "TYPO", $connect, $Pile[0]))) .
(($t1 = strval(interdire_scripts(textebrut(typo($GLOBALS['meta']['slogan_site'], "TYPO", $connect, $Pile[0])))))!=='' ?
		(' - ' . $t1) :
		'') .
'</title>
      	' .
(($t1 = strval(interdire_scripts(attribut_html(couper(propre($GLOBALS['meta']['descriptif_site'], $connect, $Pile[0]),'150')))))!=='' ?
		('<meta name="description" content="' . $t1 . '" />') :
		'') .
'
      	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
      	<link rel="stylesheet" type="text/css" href="squelettes-dist/css/style-create.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
		<!-- SELECT2 FRAMEWORK -->
		<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
		<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>

      	<style type="text/css">

			.marker{
				z-index: 1;
            	position: absolute;
            	padding-left: ;
            	padding-top: 0px;
	        }

	        #map{
	        	z-index: 0;
	        }

	        .popover{
	            background-color: white;
	        }


			.rotate{
			    -moz-transition: all 0.3s linear;
			    -webkit-transition: all 0.3s linear;
			    transition: all 0.3s linear;
			}

			.rotate.down{
			    -ms-transform: rotate(90deg);
			    -moz-transform: rotate(90deg);
			    -webkit-transform: rotate(90deg);
			    transform: rotate(90deg);
			}
			#img-a, #img-b{position: absolute;} #img-a{z-index: 1; border: 2px solid blue;} #img-b{z-index: 3;}


      	</style>
   	</head>

   	<body>

      	<div class="container-fluid">
      		<section>
      			<div class="le_header">
      				' .

'<'.'?php echo recuperer_fond( ' . argumenter_squelette('inclure/header') . ', array(\'home\' => ' . argumenter_squelette('oui') . ',
	\'lang\' => ' . argumenter_squelette($GLOBALS["spip_lang"]) . '), array("compil"=>array(\'squelettes/atlas.html\',\'html_2affb1faea87cab8e441a4c2fcb0a65e\',\'\',61,$GLOBALS[\'spip_lang\'])), _request("connect"));
?'.'>
      			</div>
      		</section>
      		<section>
      			' .

'<'.'?php echo recuperer_fond( ' . argumenter_squelette('inclure/nav') . ', array_merge('.var_export($Pile[0],1).',array(\'lang\' => ' . argumenter_squelette($GLOBALS["spip_lang"]) . ')), array("compil"=>array(\'squelettes/atlas.html\',\'html_2affb1faea87cab8e441a4c2fcb0a65e\',\'\',65,$GLOBALS[\'spip_lang\'])), _request("connect"));
?'.'>
      		</section>

         	<div>
	            <div class="row">

	            	<!--  -->
	               	<div class="col-3 h-100" >
	               		<div class="h-75">
		               		
		               		<h2 class="text-center">CATÉGORIE</h2>

							<div id="accordion">
								' .
BOUCLE_groupes_motshtml_2affb1faea87cab8e441a4c2fcb0a65e($Cache, $Pile, $doublons, $Numrows, $SP) .
'

							</div>
						</div>
						
						<div class="h-25">
							<div class="row text-center h-100">
								<div class="col">
									<a class="btn btn-primary w-100" href="./" role="button">Retour à la playlist</a>
								</div>
								<div class="col">
									<a class="btn btn-primary w-100" href="#" role="button" onclick="filtrage();">Filtrer</a>
								</div>
							</div>
						</div>
	               	</div>

	               	<!--  -->
	               	<div class="col-9 h-100">
	               		<div class="row">
							
	               			<div class="col">
	               				<!-- Boucle Strates -->
	               				' .
BOUCLE_groupes_mots_strateshtml_2affb1faea87cab8e441a4c2fcb0a65e($Cache, $Pile, $doublons, $Numrows, $SP) .
'

	               			</div>

	               			' .
BOUCLE_mots_thematiqueshtml_2affb1faea87cab8e441a4c2fcb0a65e($Cache, $Pile, $doublons, $Numrows, $SP) .
'
							
	               			<div class="col">
	               				' .
executer_balise_dynamique('FORMULAIRE_RECHERCHE',
	array(),
	array('squelettes/atlas.html','html_2affb1faea87cab8e441a4c2fcb0a65e','',138,$GLOBALS['spip_lang'])) .
'
	               			</div>
	               		</div>
	               		<div class="row">
	               			<div class="col p-0" id="carteSeine">
			                  	<img src="http://moussa.ascmtsahara.fr/Carte%20axe%20seine-01.jpg" id="img-a" class="img-fluid">
			                  	<svg viewBox="-1 -1 162 92" xmlns="http://www.w3.org/2000/svg" id="img-b">
				                    <defs>
				                      <path id="lobjet" d="M 100 100 L 300 100 L 200 300 z" fill="red"/>
				                    </defs>
				                    <!-- Ici les marker -->
			                  	</svg>
			                </div>
	               		</div>
	               		<span id="resultat_check"></span>
	               	</div>


	            </div>
         	</div>
      	</div>
    <!--.page-->
  	<!-- Les scripts  -->

    <!-- Script pour l\'arrow de categorie -->
    <script type="text/javascript">
    	$(document).ready(function(){
		    $(".btn-turn-icon").click(function(){

		    	if( $(this).find("i").hasClass("down") ){
			    	$(this).find("i").removeClass("down")
		    	} else{

			    	$(".btn-turn-icon").each(function(){
			    		$(this).find("i").removeClass("down")
			    	})

			    	$(this).find("i").addClass("down")
		    	}


		    })
		})
    </script>
    <!-- Select2 Script -->
    <script type="text/javascript">
    	$(".liste_strates").select2({
		    placeholder: "Strates",
		    maximumSelectionLength: 2,
		    width : "100%"
		});
    </script>

    <script type="text/javascript">
    	function filtrage(){
    		var valeurs = [];
    		var liste = "";
			$(\'input:checked[name=motcle]\').each(function() {
			  	valeurs.push($(this).val());
			});

			valeurs.forEach(function(item){
			  	liste += \'titre = "\'+item+\'" AND \';
			});

			$("#resultat_check").empty().append(liste);
    	}

    </script>

    <script type="text/javascript">
    	function getUrl(param) {
			var vars = {};
			window.location.href.replace( location.hash, \'\' ).replace( 
				/[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
				function( m, key, value ) { // callback
					vars[key] = value !== undefined ? value : \'\';
				}
			);

			if ( param ) {
				return vars[param] ? vars[param] : null;	
			}
			return vars;
		}
		if (getUrl(\'lieu_carte\')) {
			$(function(){
				
				$.get(
					\'squelettes/php_jquery/marker.php\',
					function(data){
						var arr = JSON.parse(data);
						$(\'#img-b\').attr("src",arr.carte);

						for (var i = 0; i < arr.lieux.length; i++){
							var obj = arr.lieux[i];
								$(\'#img-b\').append(
								\'<svg x="\'+obj.x+\'" y="\'+obj.y+\'" viewBox="0 0 400 400" width="3"  height="3">\\
								<a href="#" data-toggle="popover\'+getUrl(\'lieu_carte\')+\'"><use href="#lobjet" /></a>\\
								</svg>\'
							);//0 0 100 100
							
							$(\'[data-toggle="popover\'+getUrl(\'lieu_carte\')+\'"]\').popover({
								html : true,
								title : \'<h3 class="text-center">\' + obj.nom_lieux + \'</h3>\',
								content: \'<div class="list-group">\\
											' .
BOUCLE_groupes_mots_auteurshtml_2affb1faea87cab8e441a4c2fcb0a65e($Cache, $Pile, $doublons, $Numrows, $SP) .
'</div>\',
								placement: \'top\',
								trigger: \'focus\'
							}); 
						}
					}
				);
			})
		}

    </script>
   	</body>

</html>');

	return analyse_resultat_skel('html_2affb1faea87cab8e441a4c2fcb0a65e', $Cache, $page, 'squelettes/atlas.html');
}
?>