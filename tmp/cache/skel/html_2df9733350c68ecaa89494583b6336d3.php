<?php

/*
 * Squelette : ../plugins-dist/forum/prive/squelettes/contenu/configurer_forum.html
 * Date :      Tue, 21 Jan 2020 17:16:37 GMT
 * Compile :   Wed, 22 Jan 2020 22:24:34 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette ../plugins-dist/forum/prive/squelettes/contenu/configurer_forum.html
// Temps de compilation total: 23.049 ms
//

function html_2df9733350c68ecaa89494583b6336d3($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
invalideur_session($Cache, sinon_interdire_acces(((function_exists("autoriser")||include_spip("inc/autoriser"))&&autoriser('configurer', '_forum')?" ":""))) .
'
<h1 class="grostitre">' .
_T('forum:titre_forum') .
'</h1>

<h2>' .
_T('public|spip|ecrire:titre_config_contenu_public') .
'</h2>

<div class=\'ajax\'>
' .
executer_balise_dynamique('FORMULAIRE_CONFIGURER_FORUMS_PARTICIPANTS',
	array(),
	array('../plugins-dist/forum/prive/squelettes/contenu/configurer_forum.html','html_2df9733350c68ecaa89494583b6336d3','',7,$GLOBALS['spip_lang'])) .
'
</div>

<div class=\'ajax\'>
' .
executer_balise_dynamique('FORMULAIRE_CONFIGURER_FORUMS_CONTENU',
	array(),
	array('../plugins-dist/forum/prive/squelettes/contenu/configurer_forum.html','html_2df9733350c68ecaa89494583b6336d3','',11,$GLOBALS['spip_lang'])) .
'
</div>

<h2>' .
_T('public|spip|ecrire:titre_config_contenu_prive') .
'</h2>

<div class=\'ajax\'>
' .
executer_balise_dynamique('FORMULAIRE_CONFIGURER_FORUMS_PRIVES',
	array(),
	array('../plugins-dist/forum/prive/squelettes/contenu/configurer_forum.html','html_2df9733350c68ecaa89494583b6336d3','',17,$GLOBALS['spip_lang'])) .
'
</div>

<h2>' .
_T('public|spip|ecrire:titre_config_contenu_notifications') .
'</h2>
<div class=\'ajax\'>
' .
executer_balise_dynamique('FORMULAIRE_CONFIGURER_FORUMS_NOTIFICATIONS',
	array(),
	array('../plugins-dist/forum/prive/squelettes/contenu/configurer_forum.html','html_2df9733350c68ecaa89494583b6336d3','',22,$GLOBALS['spip_lang'])) .
'
</div>');

	return analyse_resultat_skel('html_2df9733350c68ecaa89494583b6336d3', $Cache, $page, '../plugins-dist/forum/prive/squelettes/contenu/configurer_forum.html');
}
?>