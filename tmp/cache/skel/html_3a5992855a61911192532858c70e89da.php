<?php

/*
 * Squelette : plugins/auto/player/v3.1.0/modeles/doc_player.html
 * Date :      Fri, 20 Dec 2019 12:26:28 GMT
 * Compile :   Sun, 09 Feb 2020 20:43:13 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette plugins/auto/player/v3.1.0/modeles/doc_player.html
// Temps de compilation total: 0.173 ms
//

function html_3a5992855a61911192532858c70e89da($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = recuperer_fond( 'modeles/emb' , array_merge($Pile[0],array()), array('etoile'=>true,'compil'=>array('plugins/auto/player/v3.1.0/modeles/doc_player.html','html_3a5992855a61911192532858c70e89da','',1,$GLOBALS['spip_lang'])), _request('connect'));

	return analyse_resultat_skel('html_3a5992855a61911192532858c70e89da', $Cache, $page, 'plugins/auto/player/v3.1.0/modeles/doc_player.html');
}
?>