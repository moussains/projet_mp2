<?php

/*
 * Squelette : ../prive/squelettes/extra/dist.html
 * Date :      Tue, 21 Jan 2020 17:16:32 GMT
 * Compile :   Tue, 21 Jan 2020 17:22:23 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette ../prive/squelettes/extra/dist.html
// Temps de compilation total: 0.096 ms
//

function html_c6a85311a6a2052e5d5914b9f5c4b39a($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = '
<!-- extra -->';

	return analyse_resultat_skel('html_c6a85311a6a2052e5d5914b9f5c4b39a', $Cache, $page, '../prive/squelettes/extra/dist.html');
}
?>