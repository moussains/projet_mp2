<?php

/*
 * Squelette : ../prive/squelettes/contenu/navigation.html
 * Date :      Tue, 21 Jan 2020 17:16:32 GMT
 * Compile :   Tue, 21 Jan 2020 17:42:44 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette ../prive/squelettes/contenu/navigation.html
// Temps de compilation total: 0.205 ms
//

function html_b91f1104de5becf03f20ecc4bf0872ba($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (

'<'.'?php echo recuperer_fond( ' . argumenter_squelette('prive/squelettes/inclure/menu-navigation') . ', array_merge('.var_export($Pile[0],1).',array(\'menu\' => ' . argumenter_squelette(@$Pile[0]['menu']) . ',
	\'bloc\' => ' . argumenter_squelette('contenu') . ',
	\'lang\' => ' . argumenter_squelette($GLOBALS["spip_lang"]) . ')), array("compil"=>array(\'../prive/squelettes/contenu/navigation.html\',\'html_b91f1104de5becf03f20ecc4bf0872ba\',\'\',1,$GLOBALS[\'spip_lang\'])), _request("connect"));
?'.'>
');

	return analyse_resultat_skel('html_b91f1104de5becf03f20ecc4bf0872ba', $Cache, $page, '../prive/squelettes/contenu/navigation.html');
}
?>