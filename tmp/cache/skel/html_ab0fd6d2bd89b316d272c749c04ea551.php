<?php

/*
 * Squelette : ../plugins-dist/mots/prive/objets/liste/mots_associer-select-unseul.html
 * Date :      Tue, 21 Jan 2020 17:16:51 GMT
 * Compile :   Thu, 30 Jan 2020 15:07:50 GMT
 * Boucles :   _remplacer, _mots
 */ 

function BOUCLE_remplacerhtml_ab0fd6d2bd89b316d272c749c04ea551(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	$in = array();
	if (!(is_array($a = (interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'exclus', null),true))))))
		$in[]= $a;
	else $in = array_merge($in, $a);
	if (!isset($command['table'])) {
		$command['table'] = 'mots';
		$command['id'] = '_remplacer';
		$command['from'] = array('mots' => 'spip_mots');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("mots.id_mot",
		"mots.id_groupe");
		$command['join'] = array();
		$command['limit'] = '0,1';
		$command['having'] = 
			array();
	}
	$command['orderby'] = array(((!sql_quote($in) OR sql_quote($in)==="''") ? 0 : ('FIELD(mots.id_mot,' . sql_quote($in) . ')')));
	$command['where'] = 
			array(
			array('=', 'mots.id_groupe', sql_quote(@$Pile[0]['id_groupe'], '','bigint(21) NOT NULL DEFAULT 0')), sql_in('mots.id_mot',sql_quote($in)));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('../plugins-dist/mots/prive/objets/liste/mots_associer-select-unseul.html','html_ab0fd6d2bd89b316d272c749c04ea551','_remplacer',3,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$t0 .= (
'
	' .
vide($Pile['vars'][$_zzz=(string)'expose'] = (	'mot-' .
	$Pile[$SP]['id_mot'] .
	'-' .
	interdire_scripts(@$Pile[0]['objet']) .
	'-' .
	@$Pile[0]['id_objet'])) .
'name="remplacer_lien[mot-' .
$Pile[$SP]['id_mot'] .
'-' .
interdire_scripts(@$Pile[0]['objet']) .
'-' .
@$Pile[0]['id_objet'] .
'][groupe' .
$Pile[$SP]['id_groupe'] .
']"
');
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_remplacer @ ../plugins-dist/mots/prive/objets/liste/mots_associer-select-unseul.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}


function BOUCLE_motshtml_ab0fd6d2bd89b316d272c749c04ea551(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($command['table'])) {
		$command['table'] = 'mots';
		$command['id'] = '_mots';
		$command['from'] = array('mots' => 'spip_mots');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['orderby'] = array('num', 'multi');
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['select'] = array("0+mots.titre AS num",
		"".sql_multi('mots.titre', $GLOBALS['spip_lang'])."",
		"mots.id_mot",
		"mots.titre AS titre_rang",
		"mots.titre");
	$command['where'] = 
			array(
			array('=', 'mots.id_groupe', sql_quote(@$Pile[0]['id_groupe'], '','bigint(21) NOT NULL DEFAULT 0')));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('../plugins-dist/mots/prive/objets/liste/mots_associer-select-unseul.html','html_ab0fd6d2bd89b316d272c749c04ea551','_mots',15,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$t0 .= (
'
	' .
vide($Pile['vars'][$_zzz=(string)'value'] = (	'mot-' .
	$Pile[$SP]['id_mot'] .
	'-' .
	interdire_scripts(@$Pile[0]['objet']) .
	'-' .
	@$Pile[0]['id_objet'])) .
'<option value="' .
table_valeur($Pile["vars"], (string)'value', null) .
'"' .
(((table_valeur($Pile["vars"], (string)'value', null) == table_valeur($Pile["vars"], (string)'expose', null)))  ?
		(' ' . 'selected="selected" class="on"') :
		'') .
'>' .
(($t1 = strval(recuperer_numero($Pile[$SP]['titre_rang'])))!=='' ?
		($t1 . '. ') :
		'') .
interdire_scripts(supprimer_numero(typo($Pile[$SP]['titre']), "TYPO", $connect, $Pile[0])) .
'</option>
');
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_mots @ ../plugins-dist/mots/prive/objets/liste/mots_associer-select-unseul.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}

//
// Fonction principale du squelette ../plugins-dist/mots/prive/objets/liste/mots_associer-select-unseul.html
// Temps de compilation total: 36.105 ms
//

function html_ab0fd6d2bd89b316d272c749c04ea551($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
vide($Pile['vars'][$_zzz=(string)'selected'] = '') .
'<select
' .
(($t1 = BOUCLE_remplacerhtml_ab0fd6d2bd89b316d272c749c04ea551($Cache, $Pile, $doublons, $Numrows, $SP))!=='' ?
		$t1 :
		((	'
	' .
	vide($Pile['vars'][$_zzz=(string)'expose'] = interdire_scripts(table_valeur(entites_html(table_valeur(@$Pile[0], (string)'ajouter_lien', null),true),(	'groupe' .
			@$Pile[0]['id_groupe'])))) .
	'}
	' .
	vide($Pile['vars'][$_zzz=(string)'selected'] = ((table_valeur($Pile["vars"], (string)'expose', null)) ?' ' :'')) .
	'name="ajouter_lien[groupe' .
	@$Pile[0]['id_groupe'] .
	']"
'))) .
'
id="ajouter_lien-groupe' .
@$Pile[0]['id_groupe'] .
'"
	onchange="jQuery(this).siblings(\'input.submit\').css(\'visibility\',\'visible\');"
>
<option value="x">&nbsp;</option>
' .
BOUCLE_motshtml_ab0fd6d2bd89b316d272c749c04ea551($Cache, $Pile, $doublons, $Numrows, $SP) .
'
</select>
<input type="submit" class="submit" name="groupe' .
@$Pile[0]['id_groupe'] .
'" value="' .
_T('public|spip|ecrire:bouton_changer') .
'"' .
(!(table_valeur($Pile["vars"], (string)'selected', null))  ?
		(' ' . 'style="visibility:hidden;"') :
		'') .
'/>
');

	return analyse_resultat_skel('html_ab0fd6d2bd89b316d272c749c04ea551', $Cache, $page, '../plugins-dist/mots/prive/objets/liste/mots_associer-select-unseul.html');
}
?>