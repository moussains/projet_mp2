<?php

/*
 * Squelette : ../plugins-dist/breves/prive/squelettes/contenu/breve_edit.html
 * Date :      Wed, 26 Feb 2020 10:55:57 GMT
 * Compile :   Mon, 09 Mar 2020 18:14:10 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette ../plugins-dist/breves/prive/squelettes/contenu/breve_edit.html
// Temps de compilation total: 9.702 ms
//

function html_eba6067c474cc6e2661d04e8e3f3580a($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = 
'<'.'?php echo recuperer_fond( ' . argumenter_squelette('prive/echafaudage/contenu/objet_edit') . ', array_merge('.var_export($Pile[0],1).',array(\'objet\' => ' . argumenter_squelette('breve') . ',
	\'id_objet\' => ' . argumenter_squelette(@$Pile[0]['id_breve']) . ',
	\'lang\' => ' . argumenter_squelette($GLOBALS["spip_lang"]) . ')), array("compil"=>array(\'../plugins-dist/breves/prive/squelettes/contenu/breve_edit.html\',\'html_eba6067c474cc6e2661d04e8e3f3580a\',\'\',1,$GLOBALS[\'spip_lang\'])), _request("connect"));
?'.'>';

	return analyse_resultat_skel('html_eba6067c474cc6e2661d04e8e3f3580a', $Cache, $page, '../plugins-dist/breves/prive/squelettes/contenu/breve_edit.html');
}
?>