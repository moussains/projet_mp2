<?php

/*
 * Squelette : ../plugins-dist/mots/prive/objets/liste/mots_associer-select.html
 * Date :      Tue, 21 Jan 2020 17:16:51 GMT
 * Compile :   Fri, 31 Jan 2020 14:59:19 GMT
 * Boucles :   _mots
 */ 

function BOUCLE_motshtml_23b101099785c7d25f207d6a37ceaf10(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	$in = array();
	if (!(is_array($a = (interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'exclus', null),true))))))
		$in[]= $a;
	else $in = array_merge($in, $a);
	if (!isset($command['table'])) {
		$command['table'] = 'mots';
		$command['id'] = '_mots';
		$command['from'] = array('mots' => 'spip_mots');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['orderby'] = array('num', 'multi');
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['select'] = array("0+mots.titre AS num",
		"".sql_multi('mots.titre', $GLOBALS['spip_lang'])."",
		"mots.id_mot",
		"mots.id_groupe",
		"mots.titre AS titre_rang",
		"mots.titre");
	$command['where'] = 
			array(
			array('=', 'mots.id_groupe', sql_quote(@$Pile[0]['id_groupe'], '','bigint(21) NOT NULL DEFAULT 0')), sql_in('mots.id_mot',sql_quote($in),'NOT'));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('../plugins-dist/mots/prive/objets/liste/mots_associer-select.html','html_23b101099785c7d25f207d6a37ceaf10','_mots',2,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$t0 .= (
'
		' .
vide($Pile['vars'][$_zzz=(string)'value'] = (	'mot-' .
	$Pile[$SP]['id_mot'] .
	'-' .
	interdire_scripts(@$Pile[0]['objet']) .
	'-' .
	@$Pile[0]['id_objet'])) .
'<option value="' .
table_valeur($Pile["vars"], (string)'value', null) .
'"' .
(($t1 = strval((((table_valeur($Pile["vars"], (string)'value', null) == interdire_scripts(table_valeur(entites_html(table_valeur(@$Pile[0], (string)'ajouter_lien', null),true),(	'groupe' .
		$Pile[$SP]['id_groupe']))))) ?' ' :'')))!=='' ?
		($t1 . (	'selected="selected"' .
	vide($Pile['vars'][$_zzz=(string)'selected'] = ' '))) :
		'') .
'>' .
(($t1 = strval(recuperer_numero($Pile[$SP]['titre_rang'])))!=='' ?
		($t1 . '. ') :
		'') .
interdire_scripts(supprimer_numero(typo($Pile[$SP]['titre']), "TYPO", $connect, $Pile[0])) .
'</option>
	');
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_mots @ ../plugins-dist/mots/prive/objets/liste/mots_associer-select.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}

//
// Fonction principale du squelette ../plugins-dist/mots/prive/objets/liste/mots_associer-select.html
// Temps de compilation total: 15.939 ms
//

function html_23b101099785c7d25f207d6a37ceaf10($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
vide($Pile['vars'][$_zzz=(string)'selected'] = '') .
(($t1 = BOUCLE_motshtml_23b101099785c7d25f207d6a37ceaf10($Cache, $Pile, $doublons, $Numrows, $SP))!=='' ?
		((	'
	<select name="ajouter_lien[groupe' .
		@$Pile[0]['id_groupe'] .
		']" id="ajouter_lien-groupe' .
		@$Pile[0]['id_groupe'] .
		'"
	  onchange="jQuery(this).siblings(\'input.submit\').css(\'visibility\',\'visible\');"
	>
	<option value="x">&nbsp;</option>
	') . $t1 . (	'
	</select>
	<input type="submit" class="submit" name="groupe' .
		@$Pile[0]['id_groupe'] .
		'" value="' .
		_T('public|spip|ecrire:bouton_ajouter') .
		'"' .
		(!(table_valeur($Pile["vars"], (string)'selected', null))  ?
				(' ' . 'style="visibility:hidden;"') :
				'') .
		'/>
')) :
		''));

	return analyse_resultat_skel('html_23b101099785c7d25f207d6a37ceaf10', $Cache, $page, '../plugins-dist/mots/prive/objets/liste/mots_associer-select.html');
}
?>