<?php

/*
 * Squelette : ../prive/formulaires/selecteur/inc-sel-articles.html
 * Date :      Tue, 21 Jan 2020 17:16:31 GMT
 * Compile :   Sat, 01 Feb 2020 20:26:21 GMT
 * Boucles :   _sel
 */ 

function BOUCLE_selhtml_de9bdc964b7b8eae5d25b6be6d829a2e(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	$in = array();
	if (!(is_array($a = (interdire_scripts(picker_selected(entites_html(table_valeur(@$Pile[0], (string)'selected', null),true),'article'))))))
		$in[]= $a;
	else $in = array_merge($in, $a);
	$in1 = array();
	if (!(is_array($a = (@$Pile[0]['statut']))))
		$in1[]= $a;
	else $in1 = array_merge($in1, $a);
	if (!isset($command['table'])) {
		$command['table'] = 'articles';
		$command['id'] = '_sel';
		$command['from'] = array('articles' => 'spip_articles');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("articles.id_article",
		"articles.lang",
		"articles.titre");
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['orderby'] = array(((!sql_quote($in) OR sql_quote($in)==="''") ? 0 : ('FIELD(articles.id_article,' . sql_quote($in) . ')')));
	$command['where'] = 
			array(sql_in('articles.id_article',sql_quote($in)), (!(is_array(@$Pile[0]['statut'])?count(@$Pile[0]['statut']):strlen(@$Pile[0]['statut'])) ? '' : ((is_array(@$Pile[0]['statut'])) ? sql_in('articles.statut',sql_quote($in1)) : 
			array('=', 'articles.statut', sql_quote(@$Pile[0]['statut'], '','varchar(10) NOT NULL DEFAULT \'0\'')))));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('../prive/formulaires/selecteur/inc-sel-articles.html','html_de9bdc964b7b8eae5d25b6be6d829a2e','_sel',1,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	lang_select($GLOBALS['spip_lang']);
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		lang_select_public($Pile[$SP]['lang'], '', $Pile[$SP]['titre']);
		$t0 .= (
'<li
	class=\'article\'><input
	type="hidden"
	name="' .
interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'name', null), 'id_item'),true)) .
'[]"
	value="article|' .
$Pile[$SP]['id_article'] .
'"
	/>' .
(($t1 = strval(interdire_scripts(((entites_html(sinon(table_valeur(@$Pile[0], (string)'afficher_langue', null), '0'),true)) ?' ' :''))))!=='' ?
		($t1 . (	(($t2 = strval(spip_htmlentities($Pile[$SP]['lang'] ? $Pile[$SP]['lang'] : $GLOBALS['spip_lang'])))!=='' ?
			('&#91;' . $t2 . '&#93;') :
			'') .
	' ')) :
		'') .
interdire_scripts(supprimer_numero(typo($Pile[$SP]['titre']), "TYPO", $connect, $Pile[0])) .
(($t1 = strval(interdire_scripts(((entites_html(sinon(table_valeur(@$Pile[0], (string)'select', null), ''),true)) ?'' :' '))))!=='' ?
		($t1 . (	'
<a
	href=\'#\'
	onclick=\'jQuery(this).item_unpick();return false;\'><img
	src=\'' .
	interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'img_unpick', null),true)) .
	'\' alt=\'\' /></a>')) :
		'') .
'<span
	class="sep">, </span></li>');
		lang_select();
	}
	lang_select();
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_sel @ ../prive/formulaires/selecteur/inc-sel-articles.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}

//
// Fonction principale du squelette ../prive/formulaires/selecteur/inc-sel-articles.html
// Temps de compilation total: 5.895 ms
//

function html_de9bdc964b7b8eae5d25b6be6d829a2e($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
BOUCLE_selhtml_de9bdc964b7b8eae5d25b6be6d829a2e($Cache, $Pile, $doublons, $Numrows, $SP) .
'
');

	return analyse_resultat_skel('html_de9bdc964b7b8eae5d25b6be6d829a2e', $Cache, $page, '../prive/formulaires/selecteur/inc-sel-articles.html');
}
?>