<?php

/*
 * Squelette : ../plugins-dist/revisions/prive/squelettes/contenu/configurer_revisions.html
 * Date :      Sun, 19 Jan 2020 16:26:49 GMT
 * Compile :   Sun, 19 Jan 2020 17:12:29 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette ../plugins-dist/revisions/prive/squelettes/contenu/configurer_revisions.html
// Temps de compilation total: 8.527 ms
//

function html_f697e38db6aa2d461fa210a0ce7afd33($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
invalideur_session($Cache, sinon_interdire_acces(((function_exists("autoriser")||include_spip("inc/autoriser"))&&autoriser('configurer', '_revisions')?" ":""))) .
'
<h1 class="grostitre">' .
_T('revisions:titre_revisions') .
'</h1>
<div class=\'ajax\'>
	' .
executer_balise_dynamique('FORMULAIRE_CONFIGURER_REVISIONS_OBJETS',
	array(),
	array('../plugins-dist/revisions/prive/squelettes/contenu/configurer_revisions.html','html_f697e38db6aa2d461fa210a0ce7afd33','',4,$GLOBALS['spip_lang'])) .
'
</div>');

	return analyse_resultat_skel('html_f697e38db6aa2d461fa210a0ce7afd33', $Cache, $page, '../plugins-dist/revisions/prive/squelettes/contenu/configurer_revisions.html');
}
?>