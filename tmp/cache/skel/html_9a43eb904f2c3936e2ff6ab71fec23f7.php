<?php

/*
 * Squelette : ../plugins-dist/mots/prive/squelettes/inclure/administrer_mot.html
 * Date :      Tue, 21 Jan 2020 17:16:51 GMT
 * Compile :   Wed, 29 Jan 2020 14:30:21 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette ../plugins-dist/mots/prive/squelettes/inclure/administrer_mot.html
// Temps de compilation total: 4.686 ms
//

function html_9a43eb904f2c3936e2ff6ab71fec23f7($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (($t1 = strval(invalideur_session($Cache, ((function_exists("autoriser")||include_spip("inc/autoriser"))&&autoriser('administrer', 'mot', invalideur_session($Cache, @$Pile[0]['id_mot']))?" ":""))))!=='' ?
		($t1 . (	'

	' .
	(($t2 = strval(interdire_scripts((((entites_html(table_valeur(@$Pile[0], (string)'administrer', null),true) == 'oui')) ?' ' :''))))!=='' ?
			($t2 . (	'
	<div class=\'nettoyeur\'></div>
	<a href=\'' .
		parametre_url(self(),'administrer','non') .
		'\' class=\'ajax bouton_fermer\'>' .
		interdire_scripts(filtre_balise_img_dist(chemin_image('fermer-16.png'))) .
		'</a>
	<div class="ajax">
		' .
		executer_balise_dynamique('FORMULAIRE_ADMINISTRER_MOT',
	array(@$Pile[0]['id_mot']),
	array('../plugins-dist/mots/prive/squelettes/inclure/administrer_mot.html','html_9a43eb904f2c3936e2ff6ab71fec23f7','',7,$GLOBALS['spip_lang'])) .
		'</div>
	<div class=\'nettoyeur\'></div>
	')) :
			'') .
	'
	' .
	(($t2 = strval(interdire_scripts((((entites_html(table_valeur(@$Pile[0], (string)'administrer', null),true) == 'oui')) ?'' :' '))))!=='' ?
			($t2 . (	'
		' .
		filtre_icone_dist(parametre_url(self(),'administrer','oui'),_T('adminmots:icone_administrer_mot'),'mot-24.png',lang_dir(@$Pile[0]['lang'], 'right','left'),'edit','ajax') .
		'
	')) :
			'') .
	'

')) :
		'');

	return analyse_resultat_skel('html_9a43eb904f2c3936e2ff6ab71fec23f7', $Cache, $page, '../plugins-dist/mots/prive/squelettes/inclure/administrer_mot.html');
}
?>