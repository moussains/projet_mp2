<?php

/*
 * Squelette : ../prive/formulaires/selecteur/inc-nav-articles.html
 * Date :      Tue, 21 Jan 2020 17:16:31 GMT
 * Compile :   Sat, 01 Feb 2020 20:27:00 GMT
 * Boucles :   _articles
 */ 

function BOUCLE_articleshtml_773d5d10f8ca780427c3cfeb64bd31b9(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	$in = array();
	if (!(is_array($a = (@$Pile[0]['statut']))))
		$in[]= $a;
	else $in = array_merge($in, $a);
	$command['pagination'] = array((isset($Pile[0]['debut'.table_valeur($Pile["vars"], (string)'p', null)]) ? $Pile[0]['debut'.table_valeur($Pile["vars"], (string)'p', null)] : null), 100);
	if (!isset($command['table'])) {
		$command['table'] = 'articles';
		$command['id'] = '_articles';
		$command['from'] = array('articles' => 'spip_articles');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("articles.id_article",
		"articles.id_rubrique",
		"articles.titre",
		"articles.lang");
		$command['orderby'] = array();
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array(
			array('=', 'articles.id_rubrique', sql_quote(@$Pile[0]['id_rubrique'], '','bigint(21) NOT NULL DEFAULT 0')), (!(is_array(@$Pile[0]['statut'])?count(@$Pile[0]['statut']):strlen(@$Pile[0]['statut'])) ? '' : ((is_array(@$Pile[0]['statut'])) ? sql_in('articles.statut',sql_quote($in)) : 
			array('=', 'articles.statut', sql_quote(@$Pile[0]['statut'], '','varchar(10) NOT NULL DEFAULT \'0\'')))));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('../prive/formulaires/selecteur/inc-nav-articles.html','html_773d5d10f8ca780427c3cfeb64bd31b9','_articles',1,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	
	// COMPTEUR
	$Numrows['_articles']['compteur_boucle'] = 0;
	$Numrows['_articles']['total'] = @intval($iter->count());
	$debut_boucle = isset($Pile[0]['debut'.table_valeur($Pile["vars"], (string)'p', null)]) ? $Pile[0]['debut'.table_valeur($Pile["vars"], (string)'p', null)] : _request('debut'.table_valeur($Pile["vars"], (string)'p', null));
	if(substr($debut_boucle,0,1)=='@'){
		$debut_boucle = $Pile[0]['debut'.table_valeur($Pile["vars"], (string)'p', null)] = quete_debut_pagination('id_article',$Pile[0]['@id_article'] = substr($debut_boucle,1),100,$iter);
		$iter->seek(0);
	}
	$debut_boucle = intval($debut_boucle);
	$debut_boucle = (($tout=($debut_boucle == -1))?0:($debut_boucle));
	$debut_boucle = max(0,min($debut_boucle,floor(($Numrows['_articles']['total']-1)/(100))*(100)));
	$debut_boucle = intval($debut_boucle);
	$fin_boucle = min(($tout ? $Numrows['_articles']['total'] : $debut_boucle + 99), $Numrows['_articles']['total'] - 1);
	$Numrows['_articles']['grand_total'] = $Numrows['_articles']['total'];
	$Numrows['_articles']["total"] = max(0,$fin_boucle - $debut_boucle + 1);
	if ($debut_boucle>0 AND $debut_boucle < $Numrows['_articles']['grand_total'] AND $iter->seek($debut_boucle,'continue'))
		$Numrows['_articles']['compteur_boucle'] = $debut_boucle;
	
	lang_select($GLOBALS['spip_lang']);
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$Numrows['_articles']['compteur_boucle']++;
		if ($Numrows['_articles']['compteur_boucle'] <= $debut_boucle) continue;
		if ($Numrows['_articles']['compteur_boucle']-1 > $fin_boucle) break;
		lang_select_public($Pile[$SP]['lang'], '', $Pile[$SP]['titre']);
		$t0 .= (
'<li
	class=\'article' .
(calcul_exposer($Pile[$SP]['id_article'], 'id_article', $Pile[0], $Pile[$SP]['id_rubrique'], 'id_article', '')  ?
		(' ' . 'on') :
		'') .
'\'>' .
(($t1 = strval(interdire_scripts(((entites_html(sinon(table_valeur(@$Pile[0], (string)'picker_article', null), '1'),true)) ?' ' :''))))!=='' ?
		($t1 . (	'<a
	href=\'#\'
	onclick="jQuery(this).item_pick(\'article|' .
	$Pile[$SP]['id_article'] .
	'\',\'' .
	interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'name', null), 'id_item'),true)) .
	'\',\'' .
	interdire_scripts(replace(texte_script(attribut_html($Pile[$SP]['titre'])),'&#039;',concat('\\\\',interdire_scripts(eval('return '.'chr(39)'.';'))))) .
	'\',\'article\');return false;"
><img
	class=\'add\'
	src=\'' .
	interdire_scripts(chemin_image('ajouter-16.png')) .
	'\'
	alt=\'ajouter\'
	width="16"
	height="16"
	/>')) :
		'') .
(($t1 = strval(interdire_scripts(((entites_html(sinon(table_valeur(@$Pile[0], (string)'picker_article', null), '1'),true)) ?'' :' '))))!=='' ?
		($t1 . (	'<a
	href=\'' .
	generer_url_entite($Pile[$SP]['id_article'],'article') .
	'\'>')) :
		'') .
interdire_scripts(supprimer_numero(typo($Pile[$SP]['titre']), "TYPO", $connect, $Pile[0])) .
'</a></li>
');
		lang_select();
	}
	lang_select();
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_articles @ ../prive/formulaires/selecteur/inc-nav-articles.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}

//
// Fonction principale du squelette ../prive/formulaires/selecteur/inc-nav-articles.html
// Temps de compilation total: 4.643 ms
//

function html_773d5d10f8ca780427c3cfeb64bd31b9($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
vide($Pile['vars'][$_zzz=(string)'p'] = (	'pa_' .
	@$Pile[0]['id_rubrique'])) .
(($t1 = BOUCLE_articleshtml_773d5d10f8ca780427c3cfeb64bd31b9($Cache, $Pile, $doublons, $Numrows, $SP))!=='' ?
		((	'
' .
		(($t3 = strval(filtre_pagination_dist($Numrows["_articles"]["grand_total"],
 		table_valeur($Pile["vars"], (string)'p', null),
		isset($Pile[0]['debut'.table_valeur($Pile["vars"], (string)'p', null)])?$Pile[0]['debut'.table_valeur($Pile["vars"], (string)'p', null)]:intval(_request('debut'.table_valeur($Pile["vars"], (string)'p', null))),
		100, true, 'prive', '', array())))!=='' ?
				('<p class=\'pagination\'>' . $t3 . '</p>') :
				'') .
		'
<div class="editer-groupe">
') . $t1 . '
</div>
') :
		'') .
'
');

	return analyse_resultat_skel('html_773d5d10f8ca780427c3cfeb64bd31b9', $Cache, $page, '../prive/formulaires/selecteur/inc-nav-articles.html');
}
?>