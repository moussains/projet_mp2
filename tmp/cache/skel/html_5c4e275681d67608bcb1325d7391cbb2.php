<?php

/*
 * Squelette : ../plugins-dist/svp/prive/objets/contenu/plugin.html
 * Date :      Mon, 20 Jan 2020 15:01:07 GMT
 * Compile :   Mon, 20 Jan 2020 15:03:45 GMT
 * Boucles :   _branches, _contenu_plugin
 */ 

function BOUCLE_brancheshtml_5c4e275681d67608bcb1325d7391cbb2(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	$command['liste'] = array();
	$command['liste'][] = interdire_scripts($Pile[$SP]['branches_spip']);

	if (!isset($command['table'])) {
		$command['table'] = '';
		$command['id'] = '_branches';
		$command['from'] = array();
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array(".valeur");
		$command['orderby'] = array();
		$command['where'] = 
			array();
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"DATA",
		$command,
		array('../plugins-dist/svp/prive/objets/contenu/plugin.html','html_5c4e275681d67608bcb1325d7391cbb2','_branches',21,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$t1 = (
'
			SPIP ' .
interdire_scripts(safehtml($Pile[$SP]['valeur'])) .
'
	');
		$t0 .= ((strlen($t1) && strlen($t0)) ? ', ' : '') . $t1;
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_branches @ ../plugins-dist/svp/prive/objets/contenu/plugin.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}


function BOUCLE_contenu_pluginhtml_5c4e275681d67608bcb1325d7391cbb2(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($command['table'])) {
		$command['table'] = 'plugins';
		$command['id'] = '_contenu_plugin';
		$command['from'] = array('plugins' => 'spip_plugins','depots_plugins' => 'spip_depots_plugins');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("plugins.branches_spip",
		"plugins.nom",
		"plugins.prefixe",
		"plugins.slogan",
		"plugins.categorie",
		"plugins.tags");
		$command['orderby'] = array();
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array(
			array('=', 'plugins.id_plugin', sql_quote(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'id', null),true)), '', 'bigint(21) NOT NULL AUTO_INCREMENT')), 
			array('=', 'depots_plugins.id_plugin', 'plugins.id_plugin'), 
			array('>', 'depots_plugins.id_depot', '"0"'));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('../plugins-dist/svp/prive/objets/contenu/plugin.html','html_5c4e275681d67608bcb1325d7391cbb2','_contenu_plugin',1,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	
	$l1 = _T('public|spip|ecrire:info_nom');
	$l2 = _T('public|spip|ecrire:label_prefixe');
	$l3 = _T('public|spip|ecrire:info_descriptif');
	$l4 = _T('svp:label_categorie');
	$l5 = _T('svp:label_tags');
	$l6 = _T('svp:label_branches_spip');$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$t0 .= (
'
<div class="champ contenu_titre' .
(($t1 = strval(interdire_scripts((strlen($Pile[$SP]['nom']) ? '':'vide'))))!=='' ?
		(' ' . $t1) :
		'') .
'">
	<div class=\'label\'>' .
$l1 .
'</div>
	<div dir=\'' .
lang_dir(@$Pile[0]['lang'], 'ltr','rtl') .
'\' class=\'titre\'>' .
interdire_scripts(supprimer_numero(typo($Pile[$SP]['nom']), "TYPO", $connect, $Pile[0])) .
'</div>
</div>
<div class="champ contenu_soustitre' .
(($t1 = strval(interdire_scripts((strlen($Pile[$SP]['prefixe']) ? '':'vide'))))!=='' ?
		(' ' . $t1) :
		'') .
'">
	<div class=\'label\'>' .
$l2 .
'</div>
	<div dir=\'' .
lang_dir(@$Pile[0]['lang'], 'ltr','rtl') .
'\' class=\'soustitre\'>' .
interdire_scripts(strtolower($Pile[$SP]['prefixe'])) .
'</div>
</div>
<div class="champ contenu_texte' .
(($t1 = strval(interdire_scripts((strlen($Pile[$SP]['slogan']) ? '':'vide'))))!=='' ?
		(' ' . $t1) :
		'') .
'">
	<div class=\'label\'>' .
$l3 .
'</div>
	<div dir=\'' .
lang_dir(@$Pile[0]['lang'], 'ltr','rtl') .
'\' class=\'texte\'>' .
interdire_scripts(((($a = propre($Pile[$SP]['slogan'], $connect, $Pile[0])) OR (is_string($a) AND strlen($a))) ? $a : interdire_scripts(couper(@$Pile[0]['description'],'80')))) .
'</div>
</div>
<div class="champ contenu_ps">
	<div class=\'label\'>' .
$l4 .
'&nbsp;:</div>
	<div dir=\'' .
lang_dir(@$Pile[0]['lang'], 'ltr','rtl') .
'\' class=\'ps\'><p>' .
interdire_scripts(svp_traduire_categorie($Pile[$SP]['categorie'])) .
'</p></div>
	' .
(($t1 = strval(interdire_scripts($Pile[$SP]['tags'])))!=='' ?
		((	'<div class=\'label\'>' .
	$l5 .
	'&nbsp;:</div>
	<div dir=\'' .
	lang_dir(@$Pile[0]['lang'], 'ltr','rtl') .
	'\' class=\'ps\'><p>') . $t1 . '</p></div>') :
		'') .
'
</div>
<div class="champ contenu_ps">
	' .
(($t1 = BOUCLE_brancheshtml_5c4e275681d67608bcb1325d7391cbb2($Cache, $Pile, $doublons, $Numrows, $SP))!=='' ?
		((	'
	<div class=\'label\'>' .
		$l6 .
		'&nbsp;:</div>
		<div class="texte">
	') . $t1 . '
		</div>
	') :
		'') .
'
</div>
');
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_contenu_plugin @ ../plugins-dist/svp/prive/objets/contenu/plugin.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}

//
// Fonction principale du squelette ../plugins-dist/svp/prive/objets/contenu/plugin.html
// Temps de compilation total: 5.652 ms
//

function html_5c4e275681d67608bcb1325d7391cbb2($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
BOUCLE_contenu_pluginhtml_5c4e275681d67608bcb1325d7391cbb2($Cache, $Pile, $doublons, $Numrows, $SP) .
'
');

	return analyse_resultat_skel('html_5c4e275681d67608bcb1325d7391cbb2', $Cache, $page, '../plugins-dist/svp/prive/objets/contenu/plugin.html');
}
?>