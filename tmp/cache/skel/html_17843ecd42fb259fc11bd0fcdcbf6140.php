<?php

/*
 * Squelette : ../plugins-dist/medias/prive/objets/infos/document.html
 * Date :      Tue, 21 Jan 2020 17:16:44 GMT
 * Compile :   Sat, 01 Feb 2020 20:26:20 GMT
 * Boucles :   _doc
 */ 

function BOUCLE_dochtml_17843ecd42fb259fc11bd0fcdcbf6140(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	$in = array();
	if (!(is_array($a = (@$Pile[0]['statut']))))
		$in[]= $a;
	else $in = array_merge($in, $a);
	if (!isset($command['table'])) {
		$command['table'] = 'documents';
		$command['id'] = '_doc';
		$command['from'] = array('documents' => 'spip_documents');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("documents.id_document",
		"documents.statut");
		$command['orderby'] = array();
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array(
			array('IN', 'documents.mode', '(\'image\',\'document\')'), 
			array('(documents.taille > 0 OR documents.distant=\'oui\')'), 
			array('=', 'documents.id_document', sql_quote(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'id', null),true)), '', 'bigint(21) NOT NULL AUTO_INCREMENT')), (!(is_array(@$Pile[0]['statut'])?count(@$Pile[0]['statut']):strlen(@$Pile[0]['statut'])) ? '' : ((is_array(@$Pile[0]['statut'])) ? sql_in('documents.statut',sql_quote($in)) : 
			array('=', 'documents.statut', sql_quote(@$Pile[0]['statut'], '','varchar(10) NOT NULL DEFAULT \'0\'')))));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('../plugins-dist/medias/prive/objets/infos/document.html','html_17843ecd42fb259fc11bd0fcdcbf6140','_doc',1,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	
	$l1 = _T('medias:info_gauche_numero_document');
	$l2 = _T('medias:info_statut_document');
	$l3 = _T('public|spip|ecrire:texte_statut_publie');
	$l4 = _T('medias:bouton_enlever_supprimer_document');$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$t0 .= (
'
<div class=\'infos\'>
<div class=\'numero\'>' .
$l1 .
'<p>' .
$Pile[$SP]['id_document'] .
'</p></div>


' .
(($t1 = strval(interdire_scripts(((($Pile[$SP]['statut'] == 'publie')) ?' ' :''))))!=='' ?
		($t1 . (	'
<ul class=\'instituer\'>
	<li>' .
	$l2 .
	'
		<ul>
			<li class=\'publie selected\'>' .
	$l3 .
	'</li>
		</ul>
	</li>
</ul>
')) :
		'') .
'

' .
(($t1 = strval(invalideur_session($Cache, ((((function_exists("autoriser")||include_spip("inc/autoriser"))&&autoriser('supprimer', 'document', invalideur_session($Cache, $Pile[$SP]['id_document']))?" ":"")) ?' ' :''))))!=='' ?
		($t1 . (	'
	' .
	invalideur_session($Cache, filtre_icone_horizontale_dist(generer_action_auteur('dissocier_document',(	invalideur_session($Cache, $Pile[$SP]['id_document']) .
		'-document-' .
		invalideur_session($Cache, $Pile[$SP]['id_document']) .
		'-suppr'),interdire_scripts(invalideur_session($Cache, entites_html(sinon(table_valeur(@$Pile[0], (string)'retour', null), invalideur_session($Cache, generer_url_ecrire('documents'))),true)))),$l4,'document','del')) .
	'
')) :
		'') .
'

</div>
');
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_doc @ ../plugins-dist/medias/prive/objets/infos/document.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}

//
// Fonction principale du squelette ../plugins-dist/medias/prive/objets/infos/document.html
// Temps de compilation total: 11.722 ms
//

function html_17843ecd42fb259fc11bd0fcdcbf6140($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = BOUCLE_dochtml_17843ecd42fb259fc11bd0fcdcbf6140($Cache, $Pile, $doublons, $Numrows, $SP);

	return analyse_resultat_skel('html_17843ecd42fb259fc11bd0fcdcbf6140', $Cache, $page, '../plugins-dist/medias/prive/objets/infos/document.html');
}
?>