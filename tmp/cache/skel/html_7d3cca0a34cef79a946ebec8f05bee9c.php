<?php

/*
 * Squelette : ../plugins-dist/svp/prive/objets/liste/paquets.html
 * Date :      Mon, 20 Jan 2020 15:01:07 GMT
 * Compile :   Mon, 20 Jan 2020 15:03:45 GMT
 * Boucles :   _depot_paquet, _liste_paquets
 */ 

function BOUCLE_depot_paquethtml_7d3cca0a34cef79a946ebec8f05bee9c(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($command['table'])) {
		$command['table'] = 'depots';
		$command['id'] = '_depot_paquet';
		$command['from'] = array('depots' => 'spip_depots');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("depots.url_archives",
		"depots.id_depot",
		"depots.titre");
		$command['orderby'] = array();
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array(
			array('=', 'depots.id_depot', sql_quote($Pile[$SP]['id_depot'], '','bigint(21) NOT NULL AUTO_INCREMENT')));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('../plugins-dist/svp/prive/objets/liste/paquets.html','html_7d3cca0a34cef79a946ebec8f05bee9c','_depot_paquet',40,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	
	$l1 = _T('svp:bulle_telecharger_archive');
	$l2 = _T('svp:bulle_afficher_xml_plugin');
	$l3 = _T('svp:bulle_aller_depot');$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$t0 .= (
'
				<td class="titre principale">
					<a' .
(($t1 = strval(interdire_scripts(basename($Pile[$SP-1]['nom_archive'],'.zip'))))!=='' ?
		(' id="' . $t1 . '"') :
		'') .
(($t1 = strval(concat(concat(vider_url($Pile[$SP]['url_archives']),'/'),interdire_scripts($Pile[$SP-1]['nom_archive']))))!=='' ?
		(' href="' . $t1 . '"') :
		'') .
' title="' .
$l1 .
(($t1 = strval(interdire_scripts($Pile[$SP-1]['nom_archive'])))!=='' ?
		(' ' . $t1) :
		'') .
(($t1 = strval(interdire_scripts(taille_en_octets($Pile[$SP-1]['nbo_archive']))))!=='' ?
		(' - ' . $t1) :
		'') .
'">
						' .
interdire_scripts($Pile[$SP-1]['nom_archive']) .
'
					</a>
				</td>
				<td class="date secondaire">' .
affdate(normaliser_date($Pile[$SP-1]['date_modif']),'d-m-y H:i') .
'</td>
				<td class="intervalle">' .
interdire_scripts(((($a = svp_afficher_intervalle($Pile[$SP-1]['compatibilite_spip'],'SPIP')) OR (is_string($a) AND strlen($a))) ? $a : '--')) .
'</td>
				<td class="numero">' .
interdire_scripts(denormaliser_version($Pile[$SP-1]['version'])) .
'</td>
				<td class="lien">
					<a class="mediabox boxIframe boxWidth-800px boxHeight-600px"' .
(($t1 = strval(interdire_scripts(generer_url_public('paquet_xml', (	'id_paquet=' .
	$Pile[$SP-1]['id_paquet'])))))!=='' ?
		(' href="' . $t1 . '"') :
		'') .
' title="' .
$l2 .
'">
						' .
interdire_scripts(filtre_balise_img_dist(chemin_image('plugin-16.png'),'','icone')) .
'
					</a>
				</td>
				<td class="lien">
					<a' .
(($t1 = strval(generer_url_entite($Pile[$SP]['id_depot'],'depot')))!=='' ?
		(' href="' . $t1 . '"') :
		'') .
' title="' .
$l3 .
'">
						' .
interdire_scripts(supprimer_numero(typo($Pile[$SP]['titre']), "TYPO", $connect, $Pile[0])) .
'
					</a>
				</td>
				<td class="id">' .
$Pile[$SP-1]['id_paquet'] .
'</td>
			');
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_depot_paquet @ ../plugins-dist/svp/prive/objets/liste/paquets.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}


function BOUCLE_liste_paquetshtml_7d3cca0a34cef79a946ebec8f05bee9c(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	$in = array();
	if (!(is_array($a = (@$Pile[0]['id_depot']))))
		$in[]= $a;
	else $in = array_merge($in, $a);
	$in1 = array();
	if (!(is_array($a = (@$Pile[0]['id_plugin']))))
		$in1[]= $a;
	else $in1 = array_merge($in1, $a);
	// RECHERCHE
	if (!strlen((isset($Pile[0]["recherche"])?$Pile[0]["recherche"]:(isset($GLOBALS["recherche"])?$GLOBALS["recherche"]:"")))){
		list($rech_select, $rech_where) = array("0 as points","");
	} else
	{
		$prepare_recherche = charger_fonction('prepare_recherche', 'inc');
		list($rech_select, $rech_where) = $prepare_recherche((isset($Pile[0]["recherche"])?$Pile[0]["recherche"]:(isset($GLOBALS["recherche"])?$GLOBALS["recherche"]:"")), "paquets", "?","",array (
  'criteres' => 
  array (
    'id_depot' => true,
    'id_plugin' => true,
  ),
),"id_paquet");
	}
	
	$senstri = '';
	$tri = (($t=(isset($Pile[0]['tri'.'_liste_paquets']))?$Pile[0]['tri'.'_liste_paquets']:((strncmp('_liste_paquets','session',7)==0 AND session_get('tri'.'_liste_paquets'))?session_get('tri'.'_liste_paquets'):interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'par', null), 'nom_archive'),true))))?tri_protege_champ($t):'');
	if ($tri){
		$senstri = ((intval($t=(isset($Pile[0]['sens'.'_liste_paquets']))?$Pile[0]['sens'.'_liste_paquets']:((strncmp('_liste_paquets','session',7)==0 AND session_get('sens'.'_liste_paquets'))?session_get('sens'.'_liste_paquets'):(is_array($s=table_valeur($Pile["vars"], (string)'defaut_tri', null))?(isset($s[$st=(($t=(isset($Pile[0]['tri'.'_liste_paquets']))?$Pile[0]['tri'.'_liste_paquets']:((strncmp('_liste_paquets','session',7)==0 AND session_get('tri'.'_liste_paquets'))?session_get('tri'.'_liste_paquets'):interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'par', null), 'nom_archive'),true))))?tri_protege_champ($t):'')])?$s[$st]:reset($s)):$s)))==-1 OR $t=='inverse')?-1:1);
		$senstri = ($senstri<0)?' DESC':'';
	};
	
	$command['pagination'] = array((isset($Pile[0]['debut_liste_paquets']) ? $Pile[0]['debut_liste_paquets'] : null), (($a = intval(interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'pas', null), '25'),true)))) ? $a : 10));
	if (!isset($command['table'])) {
		$command['table'] = 'paquets';
		$command['id'] = '_liste_paquets';
		$command['from'] = array('paquets' => 'spip_paquets','resultats' => 'spip_resultats');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['join'] = array('resultats' => array('paquets','id','id_paquet'));
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['select'] = array("paquets.id_depot",
		"paquets.nom_archive",
		"paquets.nbo_archive",
		"paquets.date_modif",
		"paquets.compatibilite_spip",
		"paquets.version",
		"paquets.id_paquet",
		"$rech_select",
		"".tri_champ_select($tri)."");
	$command['orderby'] = array(tri_champ_order($tri,$command['from']).$senstri);
	$command['where'] = 
			array(((@$Pile[0]["where"]) ? (@$Pile[0]["where"]) : ''), (!(is_array(@$Pile[0]['id_depot'])?count(@$Pile[0]['id_depot']):strlen(@$Pile[0]['id_depot'])) ? '' : ((is_array(@$Pile[0]['id_depot'])) ? sql_in('paquets.id_depot',sql_quote($in)) : 
			array('=', 'paquets.id_depot', sql_quote(@$Pile[0]['id_depot'], '','bigint(21) NOT NULL DEFAULT 0')))), (!(is_array(@$Pile[0]['id_plugin'])?count(@$Pile[0]['id_plugin']):strlen(@$Pile[0]['id_plugin'])) ? '' : ((is_array(@$Pile[0]['id_plugin'])) ? sql_in('paquets.id_plugin',sql_quote($in1)) : 
			array('=', 'paquets.id_plugin', sql_quote(@$Pile[0]['id_plugin'], '','bigint(21) NOT NULL')))), $rech_where?$rech_where:'', 
			array('>', 'paquets.id_depot', '"0"'));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('../plugins-dist/svp/prive/objets/liste/paquets.html','html_7d3cca0a34cef79a946ebec8f05bee9c','_liste_paquets',21,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	
	// COMPTEUR
	$Numrows['_liste_paquets']['compteur_boucle'] = 0;
	$Numrows['_liste_paquets']['total'] = @intval($iter->count());
	$debut_boucle = isset($Pile[0]['debut_liste_paquets']) ? $Pile[0]['debut_liste_paquets'] : _request('debut_liste_paquets');
	if(substr($debut_boucle,0,1)=='@'){
		$debut_boucle = $Pile[0]['debut_liste_paquets'] = quete_debut_pagination('id_paquet',$Pile[0]['@id_paquet'] = substr($debut_boucle,1),(($a = intval(interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'pas', null), '25'),true)))) ? $a : 10),$iter);
		$iter->seek(0);
	}
	$debut_boucle = intval($debut_boucle);
	$debut_boucle = (($tout=($debut_boucle == -1))?0:($debut_boucle));
	$debut_boucle = max(0,min($debut_boucle,floor(($Numrows['_liste_paquets']['total']-1)/((($a = intval(interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'pas', null), '25'),true)))) ? $a : 10)))*((($a = intval(interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'pas', null), '25'),true)))) ? $a : 10))));
	$debut_boucle = intval($debut_boucle);
	$fin_boucle = min(($tout ? $Numrows['_liste_paquets']['total'] : $debut_boucle+(($a = intval(interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'pas', null), '25'),true)))) ? $a : 10) - 1), $Numrows['_liste_paquets']['total'] - 1);
	$Numrows['_liste_paquets']['grand_total'] = $Numrows['_liste_paquets']['total'];
	$Numrows['_liste_paquets']["total"] = max(0,$fin_boucle - $debut_boucle + 1);
	if ($debut_boucle>0 AND $debut_boucle < $Numrows['_liste_paquets']['grand_total'] AND $iter->seek($debut_boucle,'continue'))
		$Numrows['_liste_paquets']['compteur_boucle'] = $debut_boucle;
	
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$Numrows['_liste_paquets']['compteur_boucle']++;
		if ($Numrows['_liste_paquets']['compteur_boucle'] <= $debut_boucle) continue;
		if ($Numrows['_liste_paquets']['compteur_boucle']-1 > $fin_boucle) break;
		$t0 .= (
'
			<tr class="' .
alterner($Numrows['_liste_paquets']['compteur_boucle'],'row_odd','row_even') .
'">
			' .
BOUCLE_depot_paquethtml_7d3cca0a34cef79a946ebec8f05bee9c($Cache, $Pile, $doublons, $Numrows, $SP) .
'
			</tr>
');
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_liste_paquets @ ../plugins-dist/svp/prive/objets/liste/paquets.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}

//
// Fonction principale du squelette ../plugins-dist/svp/prive/objets/liste/paquets.html
// Temps de compilation total: 12.662 ms
//

function html_7d3cca0a34cef79a946ebec8f05bee9c($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'

' .
vide($Pile['vars'][$_zzz=(string)'defaut_tri'] = array('nom_archive' => '1', 'date_modif' => '-1', 'version' => '-1', 'id_paquet' => '1')) .
'

' .
(($t1 = BOUCLE_liste_paquetshtml_7d3cca0a34cef79a946ebec8f05bee9c($Cache, $Pile, $doublons, $Numrows, $SP))!=='' ?
		((	'
' .
		filtre_pagination_dist($Numrows["_liste_paquets"]["grand_total"],
 		'_liste_paquets',
		isset($Pile[0]['debut_liste_paquets'])?$Pile[0]['debut_liste_paquets']:intval(_request('debut_liste_paquets')),
		(($a = intval(interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'pas', null), '25'),true)))) ? $a : 10), false, '', '', array()) .
		'
<div class="liste-objets paquets">
	<table class="spip liste" summary="' .
		_T('svp:resume_table_paquets') .
		'">
	' .
		(($t3 = strval(interdire_scripts(sinon(table_valeur(@$Pile[0], (string)'titre', null), singulier_ou_pluriel((isset($Numrows['_liste_paquets']['grand_total'])
			? $Numrows['_liste_paquets']['grand_total'] : $Numrows['_liste_paquets']['total']),'svp:info_1_paquet','svp:info_nb_paquets')))))!=='' ?
				('<caption><strong class="caption">' . $t3 . '</strong></caption>') :
				'') .
		'
		<thead>
			<tr class="first_row">
				<th class="titre principale">' .
		lien_ou_expose(parametre_url(parametre_url(self(),(($s=in_array('nom_archive',array('>','<')))?'sens':'tri').'_liste_paquets',$s?(strpos('< >','nom_archive')-1):'nom_archive'),'var_memotri',strncmp('_liste_paquets','session',7)==0?(($s=in_array('nom_archive',array('>','<')))?'sens':'tri').'_liste_paquets':''),_T('public|spip|ecrire:info_titre'),$s?(((intval($t=(isset($Pile[0]['sens'.'_liste_paquets']))?$Pile[0]['sens'.'_liste_paquets']:((strncmp('_liste_paquets','session',7)==0 AND session_get('sens'.'_liste_paquets'))?session_get('sens'.'_liste_paquets'):(is_array($s=table_valeur($Pile["vars"], (string)'defaut_tri', null))?(isset($s[$st=(($t=(isset($Pile[0]['tri'.'_liste_paquets']))?$Pile[0]['tri'.'_liste_paquets']:((strncmp('_liste_paquets','session',7)==0 AND session_get('tri'.'_liste_paquets'))?session_get('tri'.'_liste_paquets'):interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'par', null), 'nom_archive'),true))))?tri_protege_champ($t):'')])?$s[$st]:reset($s)):$s)))==-1 OR $t=='inverse')?-1:1)==(strpos('< >','nom_archive')-1)):((($t=(isset($Pile[0]['tri'.'_liste_paquets']))?$Pile[0]['tri'.'_liste_paquets']:((strncmp('_liste_paquets','session',7)==0 AND session_get('tri'.'_liste_paquets'))?session_get('tri'.'_liste_paquets'):interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'par', null), 'nom_archive'),true))))?tri_protege_champ($t):'')=='nom_archive'),'ajax') .
		'</th>
				<th class="date secondaire">' .
		lien_ou_expose(parametre_url(parametre_url(self(),(($s=in_array('date_modif',array('>','<')))?'sens':'tri').'_liste_paquets',$s?(strpos('< >','date_modif')-1):'date_modif'),'var_memotri',strncmp('_liste_paquets','session',7)==0?(($s=in_array('date_modif',array('>','<')))?'sens':'tri').'_liste_paquets':''),_T('svp:label_modifie_le'),$s?(((intval($t=(isset($Pile[0]['sens'.'_liste_paquets']))?$Pile[0]['sens'.'_liste_paquets']:((strncmp('_liste_paquets','session',7)==0 AND session_get('sens'.'_liste_paquets'))?session_get('sens'.'_liste_paquets'):(is_array($s=table_valeur($Pile["vars"], (string)'defaut_tri', null))?(isset($s[$st=(($t=(isset($Pile[0]['tri'.'_liste_paquets']))?$Pile[0]['tri'.'_liste_paquets']:((strncmp('_liste_paquets','session',7)==0 AND session_get('tri'.'_liste_paquets'))?session_get('tri'.'_liste_paquets'):interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'par', null), 'nom_archive'),true))))?tri_protege_champ($t):'')])?$s[$st]:reset($s)):$s)))==-1 OR $t=='inverse')?-1:1)==(strpos('< >','date_modif')-1)):((($t=(isset($Pile[0]['tri'.'_liste_paquets']))?$Pile[0]['tri'.'_liste_paquets']:((strncmp('_liste_paquets','session',7)==0 AND session_get('tri'.'_liste_paquets'))?session_get('tri'.'_liste_paquets'):interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'par', null), 'nom_archive'),true))))?tri_protege_champ($t):'')=='date_modif'),'ajax') .
		'</th>
				<th class="intervalle">' .
		_T('svp:label_compatibilite_spip') .
		'</th>
				<th class="numero">' .
		lien_ou_expose(parametre_url(parametre_url(self(),(($s=in_array('version',array('>','<')))?'sens':'tri').'_liste_paquets',$s?(strpos('< >','version')-1):'version'),'var_memotri',strncmp('_liste_paquets','session',7)==0?(($s=in_array('version',array('>','<')))?'sens':'tri').'_liste_paquets':''),_T('svp:label_version'),$s?(((intval($t=(isset($Pile[0]['sens'.'_liste_paquets']))?$Pile[0]['sens'.'_liste_paquets']:((strncmp('_liste_paquets','session',7)==0 AND session_get('sens'.'_liste_paquets'))?session_get('sens'.'_liste_paquets'):(is_array($s=table_valeur($Pile["vars"], (string)'defaut_tri', null))?(isset($s[$st=(($t=(isset($Pile[0]['tri'.'_liste_paquets']))?$Pile[0]['tri'.'_liste_paquets']:((strncmp('_liste_paquets','session',7)==0 AND session_get('tri'.'_liste_paquets'))?session_get('tri'.'_liste_paquets'):interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'par', null), 'nom_archive'),true))))?tri_protege_champ($t):'')])?$s[$st]:reset($s)):$s)))==-1 OR $t=='inverse')?-1:1)==(strpos('< >','version')-1)):((($t=(isset($Pile[0]['tri'.'_liste_paquets']))?$Pile[0]['tri'.'_liste_paquets']:((strncmp('_liste_paquets','session',7)==0 AND session_get('tri'.'_liste_paquets'))?session_get('tri'.'_liste_paquets'):interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'par', null), 'nom_archive'),true))))?tri_protege_champ($t):'')=='version'),'ajax') .
		'</th>
				<th class="lien">' .
		_T('svp:label_xml_plugin') .
		'</th>
				<th class="lien">' .
		_T('svp:titre_depot') .
		'</th>
				<th class="id">' .
		lien_ou_expose(parametre_url(parametre_url(self(),(($s=in_array('id_paquet',array('>','<')))?'sens':'tri').'_liste_paquets',$s?(strpos('< >','id_paquet')-1):'id_paquet'),'var_memotri',strncmp('_liste_paquets','session',7)==0?(($s=in_array('id_paquet',array('>','<')))?'sens':'tri').'_liste_paquets':''),_T('public|spip|ecrire:info_numero_abbreviation'),$s?(((intval($t=(isset($Pile[0]['sens'.'_liste_paquets']))?$Pile[0]['sens'.'_liste_paquets']:((strncmp('_liste_paquets','session',7)==0 AND session_get('sens'.'_liste_paquets'))?session_get('sens'.'_liste_paquets'):(is_array($s=table_valeur($Pile["vars"], (string)'defaut_tri', null))?(isset($s[$st=(($t=(isset($Pile[0]['tri'.'_liste_paquets']))?$Pile[0]['tri'.'_liste_paquets']:((strncmp('_liste_paquets','session',7)==0 AND session_get('tri'.'_liste_paquets'))?session_get('tri'.'_liste_paquets'):interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'par', null), 'nom_archive'),true))))?tri_protege_champ($t):'')])?$s[$st]:reset($s)):$s)))==-1 OR $t=='inverse')?-1:1)==(strpos('< >','id_paquet')-1)):((($t=(isset($Pile[0]['tri'.'_liste_paquets']))?$Pile[0]['tri'.'_liste_paquets']:((strncmp('_liste_paquets','session',7)==0 AND session_get('tri'.'_liste_paquets'))?session_get('tri'.'_liste_paquets'):interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'par', null), 'nom_archive'),true))))?tri_protege_champ($t):'')=='id_paquet'),'ajax') .
		'</th>
			</tr>
		</thead>
		<tbody>
') . $t1 . (	'
		</tbody>
	</table>
	' .
		(($t3 = strval(filtre_pagination_dist($Numrows["_liste_paquets"]["grand_total"],
 		'_liste_paquets',
		isset($Pile[0]['debut_liste_paquets'])?$Pile[0]['debut_liste_paquets']:intval(_request('debut_liste_paquets')),
		(($a = intval(interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'pas', null), '25'),true)))) ? $a : 10), true, 'prive', '', array())))!=='' ?
				('<p class=\'pagination\'>' . $t3 . '</p>') :
				'') .
		'
</div>
')) :
		('
')) .
'
');

	return analyse_resultat_skel('html_7d3cca0a34cef79a946ebec8f05bee9c', $Cache, $page, '../plugins-dist/svp/prive/objets/liste/paquets.html');
}
?>