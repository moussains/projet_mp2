<?php

/*
 * Squelette : ../plugins-dist/breves/prive/squelettes/navigation/breve_edit.html
 * Date :      Wed, 26 Feb 2020 10:55:57 GMT
 * Compile :   Mon, 09 Mar 2020 18:14:10 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette ../plugins-dist/breves/prive/squelettes/navigation/breve_edit.html
// Temps de compilation total: 0.098 ms
//

function html_1fbe4ddf2e1426688a561a5cc961bfde($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = '';

	return analyse_resultat_skel('html_1fbe4ddf2e1426688a561a5cc961bfde', $Cache, $page, '../plugins-dist/breves/prive/squelettes/navigation/breve_edit.html');
}
?>