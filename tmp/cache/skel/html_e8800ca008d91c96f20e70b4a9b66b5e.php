<?php

/*
 * Squelette : ../plugins/auto/player/v3.1.0/prive/squelettes/contenu/configurer_player.html
 * Date :      Fri, 20 Dec 2019 12:26:28 GMT
 * Compile :   Tue, 04 Feb 2020 13:18:53 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette ../plugins/auto/player/v3.1.0/prive/squelettes/contenu/configurer_player.html
// Temps de compilation total: 4.627 ms
//

function html_e8800ca008d91c96f20e70b4a9b66b5e($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'<h1 class="grostitre">' .
_T('player:configuration_player') .
'</h1>
<div class="ajax">
	' .
executer_balise_dynamique('FORMULAIRE_CONFIGURER_PLAYER',
	array(),
	array('../plugins/auto/player/v3.1.0/prive/squelettes/contenu/configurer_player.html','html_e8800ca008d91c96f20e70b4a9b66b5e','',3,$GLOBALS['spip_lang'])) .
'
</div>');

	return analyse_resultat_skel('html_e8800ca008d91c96f20e70b4a9b66b5e', $Cache, $page, '../plugins/auto/player/v3.1.0/prive/squelettes/contenu/configurer_player.html');
}
?>