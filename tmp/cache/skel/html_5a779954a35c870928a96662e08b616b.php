<?php

/*
 * Squelette : ../plugins-dist/svp/prive/squelettes/navigation/plugin.html
 * Date :      Mon, 20 Jan 2020 15:01:07 GMT
 * Compile :   Mon, 20 Jan 2020 15:03:44 GMT
 * Boucles :   _logo_plugin, _plugin_navigation
 */ 

function BOUCLE_logo_pluginhtml_5a779954a35c870928a96662e08b616b(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($command['table'])) {
		$command['table'] = 'paquets';
		$command['id'] = '_logo_plugin';
		$command['from'] = array('paquets' => 'spip_paquets');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("paquets.version",
		"paquets.logo");
		$command['orderby'] = array('paquets.version DESC');
		$command['join'] = array();
		$command['limit'] = '0,1';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array(
			array('=', 'paquets.id_plugin', sql_quote($Pile[$SP]['id_plugin'], '','bigint(21) NOT NULL')), 
			array('>', 'paquets.id_depot', '"0"'));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('../plugins-dist/svp/prive/squelettes/navigation/plugin.html','html_5a779954a35c870928a96662e08b616b','_logo_plugin',11,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$t0 .= (
'
<div class="ajax">
	<div class="formulaire_spip formulaire_editer formulaire_editer_logo formulaire_editer_logo_plugin">
		<h3 class="titrem">
			' .
interdire_scripts(filtre_balise_img_dist(chemin_image('image-24.png'),'','cadre-icone')) .
'
			' .
_T(objet_info('plugin','texte_logo_objet')) .
'
		</h3>
		' .

'<'.'?php echo recuperer_fond( ' . argumenter_squelette('formulaires/inc-apercu-logo') . ', array_merge('.var_export($Pile[0],1).',array(\'logo\' => ' . argumenter_squelette(interdire_scripts($Pile[$SP]['logo'])) . ',
	\'quoi\' => ' . argumenter_squelette('logo_on') . ',
	\'editable\' => ' . argumenter_squelette('') . ',
	\'lang\' => ' . argumenter_squelette($GLOBALS["spip_lang"]) . ')), array("compil"=>array(\'../plugins-dist/svp/prive/squelettes/navigation/plugin.html\',\'html_5a779954a35c870928a96662e08b616b\',\'\',18,$GLOBALS[\'spip_lang\'])), _request("connect"));
?'.'>
	</div>
</div>
');
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_logo_plugin @ ../plugins-dist/svp/prive/squelettes/navigation/plugin.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}


function BOUCLE_plugin_navigationhtml_5a779954a35c870928a96662e08b616b(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($si_init)) { $command['si'] = array(); $si_init = true; }
	$command['si'][] = interdire_scripts((entites_html(table_valeur(@$Pile[0], (string)'exec', null),true) == 'plugin'));

	if (!isset($command['table'])) {
		$command['table'] = 'plugins';
		$command['id'] = '_plugin_navigation';
		$command['from'] = array('plugins' => 'spip_plugins','depots_plugins' => 'spip_depots_plugins');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("plugins.id_plugin");
		$command['orderby'] = array();
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array(
			array('=', 'plugins.id_plugin', sql_quote(@$Pile[0]['id_plugin'], '','bigint(21) NOT NULL AUTO_INCREMENT')), 
			array('=', 'depots_plugins.id_plugin', 'plugins.id_plugin'), 
			array('>', 'depots_plugins.id_depot', '"0"'));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('../plugins-dist/svp/prive/squelettes/navigation/plugin.html','html_5a779954a35c870928a96662e08b616b','_plugin_navigation',1,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$t0 .= (
'

' .
boite_ouvrir('', 'info') .

'<'.'?php echo recuperer_fond( ' . argumenter_squelette('prive/objets/infos/plugin') . ', array_merge('.var_export($Pile[0],1).',array(\'type\' => ' . argumenter_squelette(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'type-page', null),true))) . ',
	\'id\' => ' . argumenter_squelette($Pile[$SP]['id_plugin']) . ',
	\'lang\' => ' . argumenter_squelette($GLOBALS["spip_lang"]) . ')), array("compil"=>array(\'../plugins-dist/svp/prive/squelettes/navigation/plugin.html\',\'html_5a779954a35c870928a96662e08b616b\',\'\',4,$GLOBALS[\'spip_lang\'])), _request("connect"));
?'.'>
' .
boite_fermer() .
'


' .
BOUCLE_logo_pluginhtml_5a779954a35c870928a96662e08b616b($Cache, $Pile, $doublons, $Numrows, $SP) .
'

' .
pipeline( 'afficher_config_objet' , array('args' => array('type' => interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'type-page', null),true)), 'id' => $Pile[$SP]['id_plugin']), 'data' => '') ));
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_plugin_navigation @ ../plugins-dist/svp/prive/squelettes/navigation/plugin.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}

//
// Fonction principale du squelette ../plugins-dist/svp/prive/squelettes/navigation/plugin.html
// Temps de compilation total: 7.312 ms
//

function html_5a779954a35c870928a96662e08b616b($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
BOUCLE_plugin_navigationhtml_5a779954a35c870928a96662e08b616b($Cache, $Pile, $doublons, $Numrows, $SP) .
'
');

	return analyse_resultat_skel('html_5a779954a35c870928a96662e08b616b', $Cache, $page, '../plugins-dist/svp/prive/squelettes/navigation/plugin.html');
}
?>