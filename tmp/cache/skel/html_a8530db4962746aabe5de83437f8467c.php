<?php

/*
 * Squelette : plugins/auto/menus_1/menus/recherche.html
 * Date :      Fri, 20 Dec 2019 12:26:26 GMT
 * Compile :   Mon, 20 Jan 2020 15:25:16 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette plugins/auto/menus_1/menus/recherche.html
// Temps de compilation total: 0.541 ms
//

function html_a8530db4962746aabe5de83437f8467c($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
(($t1 = strval(interdire_scripts(((entites_html(table_valeur(@$Pile[0], (string)'appel_formulaire', null),true)) ?' ' :''))))!=='' ?
		($t1 . '
	<div class="titre">Moteur de recherche</div>
	<div class="infos">Formulaire de recherche SPIP</div>
') :
		'') .
'

' .
(($t1 = strval(interdire_scripts(((entites_html(table_valeur(@$Pile[0], (string)'appel_menu', null),true)) ?' ' :''))))!=='' ?
		($t1 . (	'
	<li class="menu-entree item menu-items__item menu-items__item_recherche' .
	(($t2 = strval(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'css', null),true))))!=='' ?
			(' ' . $t2) :
			'') .
	'">
		' .
	executer_balise_dynamique('FORMULAIRE_RECHERCHE',
	array(),
	array('plugins/auto/menus_1/menus/recherche.html','html_a8530db4962746aabe5de83437f8467c','',2,$GLOBALS['spip_lang'])) .
	'
')) :
		'') .
'
');

	return analyse_resultat_skel('html_a8530db4962746aabe5de83437f8467c', $Cache, $page, 'plugins/auto/menus_1/menus/recherche.html');
}
?>