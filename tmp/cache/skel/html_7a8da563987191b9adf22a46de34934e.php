<?php

/*
 * Squelette : squelettes-dist/aaa.html
 * Date :      Tue, 21 Jan 2020 13:29:11 GMT
 * Compile :   Tue, 21 Jan 2020 13:57:07 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette squelettes-dist/aaa.html
// Temps de compilation total: 0.340 ms
//

function html_7a8da563987191b9adf22a46de34934e($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Bootstrap Example</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
		<style>
			/* Remove the navbar\'s default margin-bottom and rounded borders */
			.navbar
			{
				margin-bottom: 0;
				border-radius: 0;
			}
			.section-logo{
				padding-bottom: 50px;
			}

			/* Set height of the grid so .sidenav can be 100% (adjust as needed) */
			.row.content
			{height: 450px}

			/* Set gray background color and 100% height */
			.sidenav
			{
				border: 1px solid black;
				padding-top: 20px;
				height: 100%;
			}


			/*le bouton retour*/
			.btn-retour{
				margin-top: 40px;
			}


			/*La barre de menu, le bouton retour à la playlist, et la frise*/
			.list-group-horizontal, .btn-retour, .lafrise{
				width: 100%;
			}


			/*les boutons de la frise*/
			.btn-frise{
				width: 13.5%;
				background-color: #cddeaa;
				border: 1px solid black;
			} .btn-frise:hover{color: #cddeaa; background-color: #fff;}

			.list-group-item
			{
				display: inline-block;
				width: 25%;
			}
			.list-group-horizontal .list-group-item
			{
				margin-bottom: 0;
				margin-left:-3px;
				margin-right: 0;
			 	border-right-width: 0;
			}
			.list-group-horizontal .list-group-item:first-child
			{
				border-top-right-radius:0;
				border-bottom-left-radius:4px;
			}
			.list-group-horizontal .list-group-item:last-child
			{
				border-top-right-radius:4px;
				border-bottom-left-radius:0;
				border-right-width: 1px;
			}


			/*Emplacement des flèches*/
			.arrow{
				float: right;
			}


			/*Le dernier button de frise chrono*/
			.f-chrono{
				border-radius: 0% 50% 50% 0%;
			}

			/* On small screens, set height to \'auto\' for sidenav and grid */
			@media screen and (max-width: 767px)
			{
				.sidenav
				{
					height: auto;
					padding: 15px;
				}

				.row.content
				{
					height:auto;
				}
			}

		</style>
	</head>
	<body>
		<section>
			<nav class="navbar navbar-default section-logo">
				<div class="container-fluid">
					<div class="navbar-header">
						<a class="navbar-brand" href="#"><h1>GÉOSEINE (Logo)</h1></a>
					</div>
				</div>
			</nav>
		</section>
		<section>
			<!-- <nav class="navbar">
				<ul class="list-group list-group-horizontal">
					<a class="list-group-item" href="#">GéoSeine</a>
					<a class="list-group-item" href="#">Actualités</a>
					<a class="list-group-item" href="#">Notre Seine</a>
					<a class="list-group-item" href="#">Espace Pédagogique</a>
				</ul>
			</nav> -->
			' .

'<'.'?php echo recuperer_fond( ' . argumenter_squelette('inclure/nav') . ', array_merge('.var_export($Pile[0],1).',array(\'lang\' => ' . argumenter_squelette($GLOBALS["spip_lang"]) . ')), array("compil"=>array(\'squelettes-dist/aaa.html\',\'html_7a8da563987191b9adf22a46de34934e\',\'\',125,$GLOBALS[\'spip_lang\'])), _request("connect"));
?'.'>
		</section>

		<div class="container-fluid text-center">
			<div class="row content">
				<div class="col-sm-2">
					<div class="sidenav">
						<h2 class="text-center">Catégories</h2>
						<hr>
						<!-- Le filtrage -->
						<div class="panel-group" id="accordion">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">Genres<a class="arrow" data-toggle="collapse" data-parent="#accordion" href="#collapse1">        <span class="glyphicon glyphicon-circle-arrow-right"></span></a></h4>
								</div>
								<div id="collapse1" class="panel-collapse collapse in">
									<div class="panel-body">
										<div class="checkbox">
											<label><input type="checkbox" value="">Option 1</label>
										</div>
										<div class="checkbox">
											<label><input type="checkbox" value="">Option 2</label>
										</div>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">Strates<a class="arrow" data-toggle="collapse" data-parent="#accordion" href="#collapse2">        <span class="glyphicon glyphicon-circle-arrow-right"></span></a></h4>
								</div>
								<div id="collapse2" class="panel-collapse collapse">
									<div class="panel-body">
										<div class="checkbox">
											<label><input type="checkbox" value="">Option 1</label>
										</div>
										<div class="checkbox">
											<label><input type="checkbox" value="">Option 2</label>
										</div>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">Thematiques<a class="arrow" data-toggle="collapse" data-parent="#accordion" href="#collapse3">        <span class="glyphicon glyphicon-circle-arrow-right"></span></a></h4>
								</div>
								<div id="collapse3" class="panel-collapse collapse">
									<div class="panel-body">
										<div class="checkbox">
											<label><input type="checkbox" value="">Option 1</label>
										</div>
										<div class="checkbox">
											<label><input type="checkbox" value="">Option 2</label>
										</div>
									</div>
								</div>
							</div>
						</div>	
					</div>

					<!-- Fin de filtrage -->
					<div class="panel-group btn-retour">
						<button type="button" class="btn btn-default btn-sm btn-block">Retour à la playlist</button>
					</div>
				</div>
				<div class="col-sm-10 text-left">
					<div class="btn-group lafrise">
					  	<button type="button" class="btn btn-frise" >ANTIQUITÉ</button>
					  	<button type="button" class="btn btn-frise" >MOYEN ÂGE</button>
					  	<button type="button" class="btn btn-frise">XVI</button>
					  	<button type="button" class="btn btn-frise" >XVII-XVIII</button>
					  	<button type="button" class="btn btn-frise" >XIX</button>
					  	<button type="button" class="btn btn-frise" >XX</button>
					  	<button type="button" class="btn btn-frise f-chrono" style="width: 19%;">XXI</button>
					</div>
					<hr>
					<p>
						<img src="squelettes-dist/img/maps1.png" class="img-responsive">
					</p>
				</div>
			</div>
		</div>
	</body>
</html>');

	return analyse_resultat_skel('html_7a8da563987191b9adf22a46de34934e', $Cache, $page, 'squelettes-dist/aaa.html');
}
?>