<?php

/*
 * Squelette : ../plugins-dist/breves/prive/squelettes/contenu/breves.html
 * Date :      Wed, 26 Feb 2020 10:55:57 GMT
 * Compile :   Mon, 09 Mar 2020 18:37:23 GMT
 * Boucles :   _secteurs
 */ 

function BOUCLE_secteurshtml_54afc488683be2acf3b96f4d9118952a(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	$in = array();
	if (!(is_array($a = (@$Pile[0]['statut']))))
		$in[]= $a;
	else $in = array_merge($in, $a);
	if (!isset($command['table'])) {
		$command['table'] = 'rubriques';
		$command['id'] = '_secteurs';
		$command['from'] = array('rubriques' => 'spip_rubriques');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("0+rubriques.titre AS num",
		"rubriques.titre",
		"rubriques.id_rubrique",
		"rubriques.lang");
		$command['orderby'] = array('num', 'rubriques.titre');
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array((!(is_array(@$Pile[0]['statut'])?count(@$Pile[0]['statut']):strlen(@$Pile[0]['statut'])) ? '' : ((is_array(@$Pile[0]['statut'])) ? sql_in('rubriques.statut',sql_quote($in)) : 
			array('=', 'rubriques.statut', sql_quote(@$Pile[0]['statut'], '','varchar(10) NOT NULL DEFAULT \'0\'')))), 
			array('=', 'rubriques.id_parent', 0));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('../plugins-dist/breves/prive/squelettes/contenu/breves.html','html_54afc488683be2acf3b96f4d9118952a','_secteurs',15,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	lang_select($GLOBALS['spip_lang']);
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		lang_select_public($Pile[$SP]['lang'], '', $Pile[$SP]['titre']);
		$t0 .= (
'
	' .
vide($Pile['vars'][$_zzz=(string)'editable'] = invalideur_session($Cache, ((function_exists("autoriser")||include_spip("inc/autoriser"))&&autoriser('creerbrevedans', 'rubrique', invalideur_session($Cache, $Pile[$SP]['id_rubrique']))?" ":""))) .

'<'.'?php echo recuperer_fond( ' . argumenter_squelette('prive/objets/liste/breves') . ', array(\'titre\' => ' . argumenter_squelette(interdire_scripts(concat(supprimer_numero(typo($Pile[$SP]['titre']), "TYPO", $connect, $Pile[0]),interdire_scripts((($aider=charger_fonction('aide','inc',true))?$aider('breves','../plugins-dist/breves/prive/squelettes/contenu/breves.html', $Pile[0]):''))))) . ',
	\'sinon\' => ' . argumenter_squelette(interdire_scripts(concat(supprimer_numero(typo($Pile[$SP]['titre']), "TYPO", $connect, $Pile[0]),interdire_scripts((($aider=charger_fonction('aide','inc',true))?$aider('breves','../plugins-dist/breves/prive/squelettes/contenu/breves.html', $Pile[0]):''))))) . ',
	\'status\' => ' . argumenter_squelette(array('0' => 'prop', '1' => 'publie', '2' => (table_valeur($Pile["vars"], (string)'editable', null) ? 'refuse':'xx'))) . ',
	\'id_rubrique\' => ' . argumenter_squelette($Pile[$SP]['id_rubrique']) . ',
	\'par\' => ' . argumenter_squelette('date_heure') . ',
	\'lang\' => ' . argumenter_squelette($GLOBALS["spip_lang"]) . '), array("compil"=>array(\'../plugins-dist/breves/prive/squelettes/contenu/breves.html\',\'html_54afc488683be2acf3b96f4d9118952a\',\'\',17,$GLOBALS[\'spip_lang\'])), _request("connect"));
?'.'>

	' .
(($t1 = strval(table_valeur($Pile["vars"], (string)'editable', null)))!=='' ?
		($t1 . (	'
	' .
	filtre_icone_verticale_dist(parametre_url(generer_url_ecrire('breve_edit','new=oui'),'id_rubrique',$Pile[$SP]['id_rubrique']),_T('breves:icone_nouvelle_breve'),'breve','new','right') .
	'
	')) :
		'') .
'
	<br class=\'nettoyeur\' />

');
		lang_select();
	}
	lang_select();
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_secteurs @ ../plugins-dist/breves/prive/squelettes/contenu/breves.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}

//
// Fonction principale du squelette ../plugins-dist/breves/prive/squelettes/contenu/breves.html
// Temps de compilation total: 31.259 ms
//

function html_54afc488683be2acf3b96f4d9118952a($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'

<h1>' .
_T('breves:titre_page_breves') .
'</h1>

' .
BOUCLE_secteurshtml_54afc488683be2acf3b96f4d9118952a($Cache, $Pile, $doublons, $Numrows, $SP) .
'
');

	return analyse_resultat_skel('html_54afc488683be2acf3b96f4d9118952a', $Cache, $page, '../plugins-dist/breves/prive/squelettes/contenu/breves.html');
}
?>