<?php

/*
 * Squelette : ../plugins-dist/svp/prive/squelettes/hierarchie/plugin.html
 * Date :      Mon, 20 Jan 2020 15:01:07 GMT
 * Compile :   Mon, 20 Jan 2020 15:03:44 GMT
 * Boucles :   _plugin
 */ 

function BOUCLE_pluginhtml_0da537847a0bce6a531bab7f6f343658(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($command['table'])) {
		$command['table'] = 'plugins';
		$command['id'] = '_plugin';
		$command['from'] = array('plugins' => 'spip_plugins','depots_plugins' => 'spip_depots_plugins');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("plugins.nom");
		$command['orderby'] = array();
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array(
			array('=', 'plugins.id_plugin', sql_quote(@$Pile[0]['id_plugin'], '','bigint(21) NOT NULL AUTO_INCREMENT')), 
			array('=', 'depots_plugins.id_plugin', 'plugins.id_plugin'), 
			array('>', 'depots_plugins.id_depot', '"0"'));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('../plugins-dist/svp/prive/squelettes/hierarchie/plugin.html','html_0da537847a0bce6a531bab7f6f343658','_plugin',6,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$t0 .= (
'
	&gt;
	<strong class="on">' .
interdire_scripts(supprimer_numero(typo($Pile[$SP]['nom']), "TYPO", $connect, $Pile[0])) .
'</strong>
');
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_plugin @ ../plugins-dist/svp/prive/squelettes/hierarchie/plugin.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}

//
// Fonction principale du squelette ../plugins-dist/svp/prive/squelettes/hierarchie/plugin.html
// Temps de compilation total: 1.476 ms
//

function html_0da537847a0bce6a531bab7f6f343658($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'

' .
(($t1 = BOUCLE_pluginhtml_0da537847a0bce6a531bab7f6f343658($Cache, $Pile, $doublons, $Numrows, $SP))!=='' ?
		((	'
	<a href="' .
		generer_url_ecrire(objet_info('depot','table_objet')) .
		'">
		' .
		_T(objet_info('depot','texte_objets')) .
		'
	</a>
') . $t1 . '
') :
		'') .
'
');

	return analyse_resultat_skel('html_0da537847a0bce6a531bab7f6f343658', $Cache, $page, '../plugins-dist/svp/prive/squelettes/hierarchie/plugin.html');
}
?>