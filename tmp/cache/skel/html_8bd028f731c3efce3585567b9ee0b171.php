<?php

/*
 * Squelette : plugins-dist/medias/modeles/emb.html
 * Date :      Tue, 21 Jan 2020 17:16:47 GMT
 * Compile :   Sun, 09 Feb 2020 20:43:13 GMT
 * Boucles :   _ext
 */ 

function BOUCLE_exthtml_8bd028f731c3efce3585567b9ee0b171(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($command['table'])) {
		$command['table'] = 'documents';
		$command['id'] = '_ext';
		$command['from'] = array('documents' => 'spip_documents','L1' => 'spip_types_documents');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("documents.id_document",
		"documents.extension",
		"L1.mime_type");
		$command['orderby'] = array();
		$command['join'] = array('L1' => array('documents','extension'));
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array(
			array('(documents.taille > 0 OR documents.distant=\'oui\')'), 
			array('=', 'documents.id_document', sql_quote(@$Pile[0]['id_document'], '','bigint(21) NOT NULL AUTO_INCREMENT')));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('plugins-dist/medias/modeles/emb.html','html_8bd028f731c3efce3585567b9ee0b171','_ext',1,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$t0 .= (
'
' .

'<'.'?php echo recuperer_fond( ' . argumenter_squelette((	'modeles/' .
	interdire_scripts(trouver_modele_emb($Pile[$SP]['extension'],interdire_scripts($Pile[$SP]['mime_type']))))) . ', array_merge('.var_export($Pile[0],1).',array(\'id\' => ' . argumenter_squelette($Pile[$SP]['id_document']) . ',
	\'emb\' => ' . argumenter_squelette(' ') . ',
	\'lang\' => ' . argumenter_squelette($GLOBALS["spip_lang"]) . ')), array("compil"=>array(\'plugins-dist/medias/modeles/emb.html\',\'html_8bd028f731c3efce3585567b9ee0b171\',\'\',2,$GLOBALS[\'spip_lang\'])), _request("connect"));
?'.'>
');
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_ext @ plugins-dist/medias/modeles/emb.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}

//
// Fonction principale du squelette plugins-dist/medias/modeles/emb.html
// Temps de compilation total: 6.419 ms
//

function html_8bd028f731c3efce3585567b9ee0b171($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
BOUCLE_exthtml_8bd028f731c3efce3585567b9ee0b171($Cache, $Pile, $doublons, $Numrows, $SP) .
'
');

	return analyse_resultat_skel('html_8bd028f731c3efce3585567b9ee0b171', $Cache, $page, 'plugins-dist/medias/modeles/emb.html');
}
?>