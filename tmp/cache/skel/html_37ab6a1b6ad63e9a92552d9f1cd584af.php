<?php

/*
 * Squelette : ../plugins-dist/forum/formulaires/configurer_forums_participants.html
 * Date :      Tue, 21 Jan 2020 17:16:37 GMT
 * Compile :   Wed, 22 Jan 2020 22:24:34 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette ../plugins-dist/forum/formulaires/configurer_forums_participants.html
// Temps de compilation total: 2.818 ms
//

function html_37ab6a1b6ad63e9a92552d9f1cd584af($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'<div class=\'formulaire_spip formulaire_configurer formulaire_configurer_forums_participants\' id=\'formulaire_configurer_forums_participants\'>
<h3 class=\'titrem\'>' .
interdire_scripts(filtre_balise_img_dist(chemin_image('forum-interne-24.png'),'','cadre-icone')) .
_T('forum:info_mode_fonctionnement_defaut_forum_public') .
'</h3>

' .
(($t1 = strval(interdire_scripts(table_valeur(@$Pile[0], (string)'message_ok', null))))!=='' ?
		('<p class="reponse_formulaire reponse_formulaire_ok">' . $t1 . '</p>') :
		'') .
'
' .
(($t1 = strval(interdire_scripts(table_valeur(@$Pile[0], (string)'message_erreur', null))))!=='' ?
		('<p class="reponse_formulaire reponse_formulaire_erreur">' . $t1 . '</p>') :
		'') .
'

	<form action="' .
interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'action', null),true)) .
'#formulaire_configurer_forums_participants" method="post"><div>
		' .
	'<div>' .
	form_hidden(@$Pile[0]['action']) .
	'<input name=\'formulaire_action\' type=\'hidden\'
		value=\'' . @$Pile[0]['form'] . '\' />' .
	'<input name=\'formulaire_action_args\' type=\'hidden\'
		value=\'' . @$Pile[0]['formulaire_args']. '\' />' .
	(!empty($Pile[0]['_hidden']) ? @$Pile[0]['_hidden'] : '') .
	'</div>' .
'
		<div class="editer-groupe">
			<div class=\'editer configurer_forums_publics' .
((table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),'forums_publics'))  ?
		(' ' . ' ' . 'erreur') :
		'') .
'\'>
				' .
(($t1 = strval(table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),'forums_publics')))!=='' ?
		('
				<span class=\'erreur_message\'>' . $t1 . '</span>
				') :
		'') .
'<div class=\'choix\'>
					<input class=\'radio\' type="radio" name="forums_publics" value=\'non\' ' .
(($t1 = strval(interdire_scripts(((entites_html(table_valeur(@$Pile[0], (string)'forums_publics', null),true) == 'non') ? 'checked':''))))!=='' ?
		('checked="' . $t1 . '"') :
		'') .
' id="forums_publics_1"/>
					<label for="forums_publics_1">' .
_T('forum:info_desactiver_forum_public') .
'</label>
				</div>
				<div class=\'explication\'>' .
_T('forum:info_activer_forum_public') .
'</div>
				<div class=\'choix\'>
					<input class=\'radio\' type="radio" name="forums_publics" value=\'posteriori\' ' .
(($t1 = strval(interdire_scripts(((entites_html(table_valeur(@$Pile[0], (string)'forums_publics', null),true) == 'posteriori') ? 'checked':''))))!=='' ?
		('checked="' . $t1 . '"') :
		'') .
' id="forums_publics_2"/>
					<label for="forums_publics_2">' .
_T('forum:bouton_radio_publication_immediate') .
'</label>
				</div>
				<div class=\'choix\'>
					<input class=\'radio\' type="radio" name="forums_publics" value=\'priori\' ' .
(($t1 = strval(interdire_scripts(((entites_html(table_valeur(@$Pile[0], (string)'forums_publics', null),true) == 'priori') ? 'checked':''))))!=='' ?
		('checked="' . $t1 . '"') :
		'') .
' id="forums_publics_3"/>
					<label for="forums_publics_3">' .
_T('forum:bouton_radio_moderation_priori') .
'</label>
				</div>
				<div class=\'choix\'>
					<input class=\'radio\' type="radio" name="forums_publics" value=\'abo\' ' .
(($t1 = strval(interdire_scripts(((entites_html(table_valeur(@$Pile[0], (string)'forums_publics', null),true) == 'abo') ? 'checked':''))))!=='' ?
		('checked="' . $t1 . '"') :
		'') .
' id="forums_publics_4"/>
					<label for="forums_publics_4">' .
_T('forum:bouton_radio_enregistrement_obligatoire') .
'</label>
				</div>
			</div>
			<div id=\'config-options-\' class=\'fieldset\'>
				<fieldset>
					<legend>' .
_T('public|spip|ecrire:info_options_avancees') .
'</legend>
					<div class="editer-groupe">
						<div class=\'editer configurer_forums_publics_appliquer' .
((table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),'forums_publics_appliquer'))  ?
		(' ' . ' ' . 'erreur') :
		'') .
'\'>
							<label>' .
_T('forum:info_appliquer_choix_moderation') .
'</label>' .
(($t1 = strval(table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),'forums_publics_appliquer')))!=='' ?
		('
							<span class=\'erreur_message\'>' . $t1 . '</span>
							') :
		'') .
'
							<div class=\'choix\'>
								<input class=\'radio\' type="radio" name="forums_publics_appliquer" value=\'futur\' checked=\'checked\' id="forums_appliquer_futur"/>
								<label for="forums_appliquer_futur">' .
_T('forum:bouton_radio_articles_futurs') .
'</label>
							</div>
							<div class=\'choix\'>
								<input class=\'radio\' type="radio" name="forums_publics_appliquer" value=\'saufnon\' id="forums_appliquer_saufnon"/>
								<label for="forums_appliquer_saufnon">' .
_T('forum:bouton_radio_articles_tous_sauf_forum_desactive') .
'</label>
							</div>
							<div class=\'choix\'>
								<input class=\'radio\' type="radio" name="forums_publics_appliquer" value=\'tous\' id="forums_appliquer_tous"/>
								<label for="forums_appliquer_tous">' .
_T('forum:bouton_radio_articles_tous') .
'</label>
							</div>
						</div>
					</div>
				</fieldset>
			</div>
		</div>
		<p class=\'boutons\'><input class=\'submit\' type="submit" name="ok" value="' .
_T('public|spip|ecrire:bouton_enregistrer') .
'"/></p>
	</div></form>

</div>
<script type=\'text/javascript\'>
jQuery(function($){
	$(\'.editer.configurer_forums_publics input\').each(function(){
		var checked = $(this).prop(\'checked\');
		$(this).on(\'change\', function(){
			if ($(this).prop(\'checked\')==checked) {
				$(\'#config-options-\').hide();
			} else {
				$(\'#config-options-\').show();
			}
		});
	});
	$(\'#config-options-\').hide();
});
</script>');

	return analyse_resultat_skel('html_37ab6a1b6ad63e9a92552d9f1cd584af', $Cache, $page, '../plugins-dist/forum/formulaires/configurer_forums_participants.html');
}
?>