<?php

/*
 * Squelette : ../plugins-dist/svp/prive/objets/infos/plugin.html
 * Date :      Mon, 20 Jan 2020 15:01:07 GMT
 * Compile :   Mon, 20 Jan 2020 15:03:44 GMT
 * Boucles :   _nbr_paquets, _infos_plugin
 */ 

function BOUCLE_nbr_paquetshtml_ab1e8e3edbe787305e2ce3a6ea1906a4(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($command['table'])) {
		$command['table'] = 'paquets';
		$command['id'] = '_nbr_paquets';
		$command['from'] = array('paquets' => 'spip_paquets');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("count(*)");
		$command['orderby'] = array();
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array(
			array('=', 'paquets.id_plugin', sql_quote($Pile[$SP]['id_plugin'], '','bigint(21) NOT NULL')), 
			array('>', 'paquets.id_depot', '"0"'));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('../plugins-dist/svp/prive/objets/infos/plugin.html','html_ab1e8e3edbe787305e2ce3a6ea1906a4','_nbr_paquets',22,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	$Numrows['_nbr_paquets']['total'] = @intval($iter->count());
	$SP++;
	// RESULTATS
	
	$t0 = str_repeat(' ', $Numrows['_nbr_paquets']['total']);
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_nbr_paquets @ ../plugins-dist/svp/prive/objets/infos/plugin.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}


function BOUCLE_infos_pluginhtml_ab1e8e3edbe787305e2ce3a6ea1906a4(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($command['table'])) {
		$command['table'] = 'plugins';
		$command['id'] = '_infos_plugin';
		$command['from'] = array('plugins' => 'spip_plugins','depots_plugins' => 'spip_depots_plugins');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("plugins.id_plugin",
		"plugins.prefixe",
		"plugins.date_modif");
		$command['orderby'] = array();
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array(
			array('=', 'plugins.id_plugin', sql_quote(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'id', null),true)), '', 'bigint(21) NOT NULL AUTO_INCREMENT')), 
			array('=', 'depots_plugins.id_plugin', 'plugins.id_plugin'), 
			array('>', 'depots_plugins.id_depot', '"0"'));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('../plugins-dist/svp/prive/objets/infos/plugin.html','html_ab1e8e3edbe787305e2ce3a6ea1906a4','_infos_plugin',1,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$t0 .= (
'
<div class="infos">
	
	' .
vide($Pile['vars'][$_zzz=(string)'titre_info'] = interdire_scripts(_T(objet_info(entites_html(table_valeur(@$Pile[0], (string)'type', null),true),'texte_objet')))) .
'<div class="numero">
		' .
_T('public|spip|ecrire:titre_cadre_numero_objet', array('objet' => table_valeur($Pile["vars"], (string)'titre_info', null))) .
'
		<p>' .
$Pile[$SP]['id_plugin'] .
'</p>
	</div>
	
	
	<div class="liste prefixe">
		<ul class="liste-items">
			<li>' .
(($t1 = strval(interdire_scripts(strtolower($Pile[$SP]['prefixe']))))!=='' ?
		((	_T('svp:label_prefixe') .
	' : <strong>') . $t1 . '</strong>') :
		'') .
'</li>
		</ul>
	</div>

	
	<div class="liste compteurs">
		<ul class="liste-items">
			' .
(($t1 = strval(affdate(normaliser_date($Pile[$SP]['date_modif']))))!=='' ?
		((	'<li>' .
	_T('svp:label_actualise_le') .
	' ') . $t1 . '</li>') :
		'') .
'

		' .
(($t1 = BOUCLE_nbr_paquetshtml_ab1e8e3edbe787305e2ce3a6ea1906a4($Cache, $Pile, $doublons, $Numrows, $SP))!=='' ?
		($t1 . (	'
			<li>' .
		singulier_ou_pluriel($Numrows['_nbr_paquets']['total'],'svp:info_1_paquet','svp:info_nb_paquets') .
		'</li>
		')) :
		'') .
'

		
		' .
(($t1 = strval(interdire_scripts(((filtre_info_plugin_dist("SVPSTATS", "est_actif")) ?' ' :''))))!=='' ?
		($t1 . (	'
		<li>
			' .
	interdire_scripts((@$Pile[0]['nbr_sites'] ? interdire_scripts(singulier_ou_pluriel(@$Pile[0]['nbr_sites'],'svpstats:info_nbr_sites_1','svpstats:info_nbr_sites_n')):(	_T('svpstats:info_nbr_sites_0') .
		' 
			'))) .
	'
		</li>')) :
		'') .
'

		</ul>
	</div>
	
	
	' .

'<'.'?php echo recuperer_fond( ' . argumenter_squelette('prive/squelettes/inclure/voir_en_ligne') . ', array(\'type\' => ' . argumenter_squelette(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'type', null),true))) . ',
	\'id\' => ' . argumenter_squelette($Pile[$SP]['id_plugin']) . ',
	\'lang\' => ' . argumenter_squelette($GLOBALS["spip_lang"]) . '), array("compil"=>array(\'../plugins-dist/svp/prive/objets/infos/plugin.html\',\'html_ab1e8e3edbe787305e2ce3a6ea1906a4\',\'\',33,$GLOBALS[\'spip_lang\'])), _request("connect"));
?'.'>
</div>
');
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_infos_plugin @ ../plugins-dist/svp/prive/objets/infos/plugin.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}

//
// Fonction principale du squelette ../plugins-dist/svp/prive/objets/infos/plugin.html
// Temps de compilation total: 3.289 ms
//

function html_ab1e8e3edbe787305e2ce3a6ea1906a4($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
BOUCLE_infos_pluginhtml_ab1e8e3edbe787305e2ce3a6ea1906a4($Cache, $Pile, $doublons, $Numrows, $SP) .
'
');

	return analyse_resultat_skel('html_ab1e8e3edbe787305e2ce3a6ea1906a4', $Cache, $page, '../plugins-dist/svp/prive/objets/infos/plugin.html');
}
?>