<?php

/*
 * Squelette : ../plugins-dist/medias/formulaires/editer_document.html
 * Date :      Sun, 19 Jan 2020 16:27:01 GMT
 * Compile :   Sun, 19 Jan 2020 17:21:05 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette ../plugins-dist/medias/formulaires/editer_document.html
// Temps de compilation total: 20.682 ms
//

function html_9e0eee8bf503692c76fa29f4fe5a45bd($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'<div class=\'formulaire_spip formulaire_editer formulaire_editer_document formulaire_editer_document-' .
interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'id_document', null), 'nouveau'),true)) .
'\'>
	' .
(($t1 = strval(table_valeur(@$Pile[0], (string)'message_ok', null)))!=='' ?
		('<p class="reponse_formulaire reponse_formulaire_ok">' . $t1 . '</p>') :
		'') .
'
	' .
(($t1 = strval(interdire_scripts(table_valeur(@$Pile[0], (string)'message_erreur', null))))!=='' ?
		('<p class="reponse_formulaire reponse_formulaire_erreur">' . $t1 . '</p>') :
		'') .
'
	' .
(($t1 = strval(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'editable', null),true))))!=='' ?
		($t1 . (	'
	<form method=\'post\' action=\'' .
	interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'action', null),true)) .
	'\' enctype=\'multipart/form-data\'><div>
		
		' .
		'<div>' .
	form_hidden(@$Pile[0]['action']) .
	'<input name=\'formulaire_action\' type=\'hidden\'
		value=\'' . @$Pile[0]['form'] . '\' />' .
	'<input name=\'formulaire_action_args\' type=\'hidden\'
		value=\'' . @$Pile[0]['formulaire_args']. '\' />' .
	(!empty($Pile[0]['_hidden']) ? @$Pile[0]['_hidden'] : '') .
	'</div>' .
	'
		<input type=\'hidden\' name=\'id_document\' value=\'' .
	interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'id_document', null),true)) .
	'\' />
		
		<div style="position:absolute;' .
	lang_dir(@$Pile[0]['lang'], 'left','right') .
	':-10000px;"><input type=\'submit\' class=\'submit\' value=\'' .
	_T('public|spip|ecrire:bouton_enregistrer') .
	'\' /></div>
		' .
	(($t2 = strval(interdire_scripts(((match(entites_html(table_valeur(@$Pile[0], (string)'action', null),true),'popin')) ?' ' :''))))!=='' ?
			($t2 . (	'<p class="boutons"><input type=\'submit\' class=\'submit\' value=\'' .
		_T('public|spip|ecrire:bouton_enregistrer') .
		'\' /></p>')) :
			'') .
	'
		<div class="editer-groupe">
			<div class="editer editer_titre' .
	((table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),'titre'))  ?
			(' ' . ' ' . 'erreur') :
			'') .
	'">
				<label for="titre">' .
	interdire_scripts(((entites_html(table_valeur(@$Pile[0], (string)'mode', null),true) == 'document') ? _T('medias:entree_titre_document'):_T('medias:entree_titre_image'))) .
	'</label>' .
	(($t2 = strval(table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),'titre')))!=='' ?
			('
				<span class=\'erreur_message\'>' . $t2 . '</span>
				') :
			'') .
	'<input type=\'text\' class=\'text\' name=\'titre\' id=\'titre\' value="' .
	sinon(table_valeur(@$Pile[0], (string)'titre', null), '') .
	'" />
			</div>
			<div class=\'editer editer_parent' .
	((table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),'parents'))  ?
			(' ' . ' ' . 'erreur') :
			'') .
	'\'>
				<label for="parents">' .
	_T('medias:label_parents') .
	'</label>' .
	(($t2 = strval(table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),'parents')))!=='' ?
			('
				<span class=\'erreur_message\'>' . $t2 . '</span>
				') :
			'') .
	
'<'.'?php echo recuperer_fond( ' . argumenter_squelette('formulaires/selecteur/articles') . ', array(\'selected\' => ' . argumenter_squelette(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'parents', null),true))) . ',
	\'name\' => ' . argumenter_squelette('parents') . ',
	\'rubriques\' => ' . argumenter_squelette('1') . ',
	\'articles\' => ' . argumenter_squelette('1') . ',
	\'lang\' => ' . argumenter_squelette($GLOBALS["spip_lang"]) . '), array("compil"=>array(\'../plugins-dist/medias/formulaires/editer_document.html\',\'html_9e0eee8bf503692c76fa29f4fe5a45bd\',\'\',17,$GLOBALS[\'spip_lang\'])), _request("connect"));
?'.'>
			</div>
			' .
	(($t2 = strval(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'fichier', null),true))))!=='' ?
			((	'<div class="editer editer_fichier' .
		((table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),'fichier'))  ?
				(' ' . ' ' . 'erreur') :
				'') .
		'">
				<label for="fichier">' .
		_T('medias:label_fichier') .
		'</label>' .
		(($t3 = strval(table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),'fichier')))!=='' ?
				('
				<span class=\'erreur_message\'>' . $t3 . '</span>
				') :
				'') .
		'
				') . $t2 . (	'
				<p class=\'actions\'>
				' .
		(($t3 = strval(interdire_scripts(((((((entites_html(table_valeur(@$Pile[0], (string)'distant', null),true) == 'oui')) AND (interdire_scripts((@$Pile[0]['taille'] < (defined('_COPIE_LOCALE_MAX_SIZE')?constant('_COPIE_LOCALE_MAX_SIZE'):''))))) ?' ' :'')) ?' ' :''))))!=='' ?
				($t3 . (	'<input type=\'submit\' class=\'submit\' name=\'copier_local\' value=\'' .
			attribut_html(_T('medias:bouton_copier_local')) .
			'\' />')) :
				'') .
		'
				' .
		pipeline( 'editer_document_actions' , array('args' => array('id_document' => interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'id_document', null), 'nouveau'),true))), 'data' => '') ) .
		'
				&#91;<a href=\'#\' onclick=\'jQuery("#changer_fichier_document").toggle("fast");return false;\'>' .
		_T('public|spip|ecrire:bouton_changer') .
		'</a>&#93;
				</p>
				' .
		vide($Pile['vars'][$_zzz=(string)'upload'] = recuperer_fond( 'formulaires/inc-upload_document' , array_merge($Pile[0],array('joindre_upload' => 'oui' ,
	'multi' => 'non' )), array('compil'=>array('../plugins-dist/medias/formulaires/editer_document.html','html_9e0eee8bf503692c76fa29f4fe5a45bd','',0,$GLOBALS['spip_lang'])), _request('connect'))) .
		'<div id=\'changer_fichier_document\' class="' .
		(!(match(table_valeur($Pile["vars"], (string)'upload', null),'erreur'))  ?
				(' ' . 'none-js') :
				'') .
		'">
					' .
		table_valeur($Pile["vars"], (string)'upload', null) .
		'</div>
			</div>')) :
			'') .
	'
			<div class="editer editer_apercu">
				<label>' .
	_T('medias:label_apercu') .
	'</label>
				' .
	(($t2 = strval(interdire_scripts(((entites_html(table_valeur(@$Pile[0], (string)'apercu', null),true)) ?' ' :''))))!=='' ?
			($t2 . (	'
				<div class="tourner">
					<input class="image" type="image" name="tournerL90" src="' .
		interdire_scripts(chemin_image('tourner-gauche-12.png')) .
		'" alt="' .
		_T('medias:image_tourner_gauche') .
		'" />
					<input class="image" type="image" name="tournerR90" src="' .
		interdire_scripts(chemin_image('tourner-droite-12.png')) .
		'" alt="' .
		_T('medias:image_tourner_droite') .
		'" />
					<input class="image" type="image" name="tourner180" src="' .
		interdire_scripts(chemin_image('tourner-180-12.png')) .
		'" alt="' .
		_T('medias:image_tourner_180') .
		'" />
				</div>
				' .
		(($t3 = strval(interdire_scripts(filtrer('image_graver',filtrer('image_reduire',entites_html(table_valeur(@$Pile[0], (string)'apercu', null),true),'320','200')))))!=='' ?
				((	'<a href=\'' .
			interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'apercu', null),true)) .
			'\'>') . $t3 . '</a>') :
				'') .
		'
				')) :
			'') .
	'
				' .
	(($t2 = strval(interdire_scripts(((entites_html(table_valeur(@$Pile[0], (string)'apercu', null),true)) ?'' :' '))))!=='' ?
			($t2 . (	'
					' .
		vide($Pile['vars'][$_zzz=(string)'hauteur'] = interdire_scripts(((((@$Pile[0]['hauteur']) AND (interdire_scripts(@$Pile[0]['largeur']))) ?' ' :'') ? interdire_scripts(round(mult(div(@$Pile[0]['hauteur'],interdire_scripts(@$Pile[0]['largeur'])),'320'),'0')):'200'))) .
		(($t3 = strval(interdire_scripts((((@$Pile[0]['media'] == 'audio')) ?' ' :''))))!=='' ?
				($t3 . (	' ' .
			vide($Pile['vars'][$_zzz=(string)'hauteur'] = '0'))) :
				'') .
		'
					' .
		((table_valeur(@$Pile[0], (string)'_inclus', null) == 'embed') ? (	
	((($recurs=(isset($Pile[0]['recurs'])?$Pile[0]['recurs']:0))>=5)? '' :
	recuperer_fond('modeles/emb', array('id_document' => interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'id_document', null), ''),true)) ,
	'largeur' => '320' ,
	'hauteur' => table_valeur($Pile["vars"], (string)'hauteur', null) ,
	'lang' => $GLOBALS["spip_lang"] ,
	'recurs'=>(++$recurs)), array('compil'=>array('../plugins-dist/medias/formulaires/editer_document.html','html_9e0eee8bf503692c76fa29f4fe5a45bd','',47,$GLOBALS['spip_lang']), 'trim'=>true), ''))
 .
			'
						'):(	extraire_balise(
	((($recurs=(isset($Pile[0]['recurs'])?$Pile[0]['recurs']:0))>=5)? '' :
	recuperer_fond('modeles/doc', array('id_document' => interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'id_document', null), ''),true)) ,
	'largeur' => '320' ,
	'hauteur' => table_valeur($Pile["vars"], (string)'hauteur', null) ,
	'lang' => $GLOBALS["spip_lang"] ,
	'recurs'=>(++$recurs)), array('compil'=>array('../plugins-dist/medias/formulaires/editer_document.html','html_9e0eee8bf503692c76fa29f4fe5a45bd','',47,$GLOBALS['spip_lang']), 'trim'=>true), ''))
,'a') .
			'
						')) .
		'
				')) :
			'') .
	'
			</div>
			<div class="editer editer_infos">
				<label>' .
	_T('medias:label_caracteristiques') .
	'</label>
				' .
	(($t2 = strval(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'type_document', null),true))))!=='' ?
			('<span class=\'type\'>' . $t2 . ' - </span>') :
			'') .
	'
				<span class=\'taille\'>' .
	(($t2 = strval(interdire_scripts((((((@$Pile[0]['largeur']) OR (interdire_scripts(@$Pile[0]['hauteur']))) ?' ' :'')) ?' ' :''))))!=='' ?
			($t2 . (	_T('info_largeur_vignette',array('largeur_vignette' => interdire_scripts(@$Pile[0]['largeur']), 'hauteur_vignette' => interdire_scripts(@$Pile[0]['hauteur']))) .
		' - 
				')) :
			'') .
	interdire_scripts(texte_backend(taille_en_octets(@$Pile[0]['taille']))) .
	'</span>
				' .
	(($t2 = strval(interdire_scripts(((entites_html(table_valeur(@$Pile[0], (string)'_taille_modif', null),true)) ?' ' :''))))!=='' ?
			($t2 . (	'<div class=\'taille_modifiee\'>' .
		_T('medias:fichier_modifie') .
		'<br />' .
		(($t3 = strval(interdire_scripts((((((entites_html(table_valeur(@$Pile[0], (string)'_largeur_modif', null),true)) OR (interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'_hauteur_modif', null),true)))) ?' ' :'')) ?' ' :''))))!=='' ?
				($t3 . (	_T('info_largeur_vignette',array('largeur_vignette' => interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'_largeur_modif', null),true)), 'hauteur_vignette' => interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'_hauteur_modif', null),true)))) .
			' -
				')) :
				'') .
		interdire_scripts(texte_backend(taille_en_octets(entites_html(table_valeur(@$Pile[0], (string)'_taille_modif', null),true)))) .
		'</div>')) :
			'') .
	'
				' .
	pipeline( 'afficher_metas_document' , array('args' => array('quoi' => 'editer_document', 'id_document' => interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'id_document', null),true))), 'data' => '') ) .
	'
			</div>
			<div class="editer editer_descriptif' .
	((table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),'descriptif'))  ?
			(' ' . ' ' . 'erreur') :
			'') .
	'">
				<label for="descriptif">' .
	_T('public|spip|ecrire:info_description') .
	'</label>' .
	(($t2 = strval(table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),'descriptif')))!=='' ?
			('
				<span class=\'erreur_message\'>' . $t2 . '</span>
				') :
			'') .
	'<textarea name=\'descriptif\' id=\'descriptif\'' .
	(($t2 = strval(interdire_scripts(@$Pile[0]['langue'])))!=='' ?
			(' lang=\'' . $t2 . '\'') :
			'') .
	' rows=\'2\' cols=\'40\'>' .
	table_valeur(@$Pile[0], (string)'descriptif', null) .
	'</textarea>
			</div>
			' .
	(($t2 = strval(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'_editer_date', null),true))))!=='' ?
			($t2 . (	'
			<div class="editer editer_date' .
		((table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),'saisie_date'))  ?
				(' ' . ' ' . 'erreur') :
				'') .
		((table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),'saisie_heure'))  ?
				(' ' . ' ' . 'erreur') :
				'') .
		'">
				<label for="saisie_date" class=\'date\'>' .
		_T('public|spip|ecrire:date') .
		'</label>' .
		(($t3 = strval(table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),'saisie_date')))!=='' ?
				('
				<span class=\'erreur_message\'>' . $t3 . '</span>
				') :
				'') .
		'<input type=\'text\' class=\'text date\' name=\'saisie_date\' id=\'saisie_date\' size="10" maxlength="10" value="' .
		interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'saisie_date', null),true)) .
		'" />
				<span class=\'choix heure\'>
					<label for=\'saisie_heure\' class=\'heure\'>' .
		_T('medias:info_heure') .
		'</label>' .
		(($t3 = strval(table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),'saisie_heure')))!=='' ?
				('
					<span class=\'erreur_message\'>' . $t3 . '</span>') :
				'') .
		'
					<input type=\'text\' class=\'text heure\' name=\'saisie_heure\' id=\'heure\' size="5" maxlength="5" value="' .
		interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'saisie_heure', null),true)) .
		'" />
				</span>
			</div>')) :
			'') .
	'
			<div class="editer editer_credits' .
	((table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),'credits'))  ?
			(' ' . ' ' . 'erreur') :
			'') .
	'">
				<label for="credits">' .
	_T('medias:label_credits') .
	'</label>' .
	(($t2 = strval(table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),'credits')))!=='' ?
			('
				<span class=\'erreur_message\'>' . $t2 . '</span>
				') :
			'') .
	'<input type=\'text\' class=\'text\' name=\'credits\' id=\'credits\' value="' .
	sinon(table_valeur(@$Pile[0], (string)'credits', null), '') .
	'" />
			</div>
			' .
	(($t2 = strval(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'_editer_dimension', null),true))))!=='' ?
			($t2 . (	'
			<div class="editer editer_dimensions' .
		((table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),'dimensions'))  ?
				(' ' . ' ' . 'erreur') :
				'') .
		'">
				<label>' .
		_T('medias:entree_dimensions') .
		'</label>' .
		(($t3 = strval(table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),'dimensions')))!=='' ?
				('
				<span class=\'erreur_message\'>' . $t3 . '</span>') :
				'') .
		'
				<span class=\'choix largeur\'>
					<label for=\'largeur\' class=\'largeur\'>' .
		_T('medias:info_largeur') .
		'</label>
					<input type=\'text\' class=\'text\' name=\'largeur\' id=\'largeur\' value="' .
		interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'largeur', null),true)) .
		'" />
				</span>
				<span class=\'choix hauteur\'>
					<label for=\'hauteur\' class=\'hauteur\'>' .
		_T('medias:info_hauteur') .
		'</label>
					<input type=\'text\' class=\'text\' name=\'hauteur\' id=\'hauteur\' value="' .
		interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'hauteur', null),true)) .
		'" />
				</span>
			</div>')) :
			'') .
	'
		</div>
		' .
	'
		<!--extra-->
		<p class="boutons"><span class=\'image_loading\'>&nbsp;</span><input type=\'submit\' class=\'submit\' value=\'' .
	_T('public|spip|ecrire:bouton_enregistrer') .
	'\' /></p>
	</div></form>
	' .
	recuperer_fond( 'formulaires/dateur/inc-dateur' , array(), array('compil'=>array('../plugins-dist/medias/formulaires/editer_document.html','html_9e0eee8bf503692c76fa29f4fe5a45bd','',45,$GLOBALS['spip_lang'])), _request('connect')))) :
		'') .
'
</div>
');

	return analyse_resultat_skel('html_9e0eee8bf503692c76fa29f4fe5a45bd', $Cache, $page, '../plugins-dist/medias/formulaires/editer_document.html');
}
?>