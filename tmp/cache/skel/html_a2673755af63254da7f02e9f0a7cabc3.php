<?php

/*
 * Squelette : plugins/auto/menus_1/menus/accueil.html
 * Date :      Fri, 20 Dec 2019 12:26:26 GMT
 * Compile :   Tue, 28 Jan 2020 15:24:57 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette plugins/auto/menus_1/menus/accueil.html
// Temps de compilation total: 1.494 ms
//

function html_a2673755af63254da7f02e9f0a7cabc3($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
vide($Pile['vars'][$_zzz=(string)'titre'] = interdire_scripts(((($a = typo(table_valeur(@$Pile[0], (string)'titre', null))) OR (is_string($a) AND strlen($a))) ? $a : _T('public|spip|ecrire:accueil_site')))) .
'
' .
(($t1 = strval(interdire_scripts(((entites_html(table_valeur(@$Pile[0], (string)'appel_formulaire', null),true)) ?' ' :''))))!=='' ?
		($t1 . (	'
	<div class="titre">' .
	table_valeur($Pile["vars"], (string)'titre', null) .
	'</div>
	<div class="infos">' .
	_T('public|spip|ecrire:info_racine_site') .
	'</div>
')) :
		'') .
'
' .
(($t1 = strval(interdire_scripts(((entites_html(table_valeur(@$Pile[0], (string)'appel_menu', null),true)) ?' ' :''))))!=='' ?
		($t1 . (	'
	<li class="menu-entree item menu-items__item menu-items__item_accueil' .
	(($t2 = strval(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'css', null),true))))!=='' ?
			(' ' . $t2) :
			'') .
	(($t2 = strval(interdire_scripts((((((((((entites_html(table_valeur(@$Pile[0], (string)'env/type', null),true) == 'page')) AND (interdire_scripts((entites_html(table_valeur(@$Pile[0], (string)'env/composition', null),true) == 'sommaire')))) ?' ' :'')) OR (interdire_scripts((((entites_html(table_valeur(@$Pile[0], (string)'env/type-page', null),true) == 'sommaire')) ?' ' :'')))) ?' ' :'')) ?' ' :''))))!=='' ?
			($t2 . 'on active') :
			'') .
	'">
		<a href="' .
	spip_htmlspecialchars(sinon($GLOBALS['meta']['adresse_site'],'.')) .
	'/" class="menu-items__lien">' .
	table_valeur($Pile["vars"], (string)'titre', null) .
	'</a>
')) :
		'') .
'
');

	return analyse_resultat_skel('html_a2673755af63254da7f02e9f0a7cabc3', $Cache, $page, 'plugins/auto/menus_1/menus/accueil.html');
}
?>