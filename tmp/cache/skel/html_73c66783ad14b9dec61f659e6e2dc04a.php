<?php

/*
 * Squelette : squelettes/article_proposer.html
 * Date :      Sat, 01 Feb 2020 19:41:28 GMT
 * Compile :   Sat, 01 Feb 2020 19:46:30 GMT
 * Boucles :   _rubriques
 */ 

function BOUCLE_rubriqueshtml_73c66783ad14b9dec61f659e6e2dc04a(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($command['table'])) {
		$command['table'] = 'rubriques';
		$command['id'] = '_rubriques';
		$command['from'] = array('rubriques' => 'spip_rubriques');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("rubriques.titre",
		"rubriques.id_rubrique",
		"rubriques.lang");
		$command['orderby'] = array('rubriques.titre');
		$command['where'] = 
			array();
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('squelettes/article_proposer.html','html_73c66783ad14b9dec61f659e6e2dc04a','_rubriques',65,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	lang_select($GLOBALS['spip_lang']);
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		lang_select_public($Pile[$SP]['lang'], '', $Pile[$SP]['titre']);
		$t0 .= (
'
              <option value="' .
$Pile[$SP]['id_rubrique'] .
'">' .
interdire_scripts(supprimer_numero(typo($Pile[$SP]['titre']), "TYPO", $connect, $Pile[0])) .
'</option>
              ');
		lang_select();
	}
	lang_select();
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_rubriques @ squelettes/article_proposer.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}

//
// Fonction principale du squelette squelettes/article_proposer.html
// Temps de compilation total: 15.881 ms
//

function html_73c66783ad14b9dec61f659e6e2dc04a($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'<'.'?php header("X-Spip-Cache: 3600"); ?'.'>' .
'<? 
if (!$auteur_session){
//Si pas loggé, page blanche\'
?>

	<?php 
	exit;
	} 
	?>
<html dir="' .
lang_dir(@$Pile[0]['lang'], 'ltr','rtl') .
'" lang="' .
spip_htmlentities(@$Pile[0]['lang'] ? @$Pile[0]['lang'] : $GLOBALS['spip_lang']) .
'">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=' .
interdire_scripts($GLOBALS['meta']['charset']) .
'" />
<title>Document sans titre</title>
<link href="spip_style.css" rel="stylesheet" type="text/css" />
<SCRIPT LANGUAGE="JavaScript">

var max=1700;  		// 1700 caractères maximum

function compter(f) {
	var txt=f.texte.value;
	var nb=txt.length;
	if (nb>max) { 
		alert("Pas plus de "+max+" caractères dans ce champ");
		f.texte.value=txt.substring(0,max);
		nb=max;
	}
	f.nbcar.value=nb;
}

function timer() {
	compter(document.forms["form1"]);
	setTimeout("timer()",100);
}

</SCRIPT>
</head>

<body>
<h3 align="center"><u><font face="Arial, Helvetica, sans-serif">A LIRE AVANT DE POSTER</font></u></h3>
<p align="center">Remplir obligatoirement les champs Pseudo (si vide), Email (si 
  vide), <font color="#FF0000"><u><strong>Rubrique</strong></u></font> , Titre 
  et Texte.</p>
<p align="center"> Attention Vous &ecirc;tes limit&eacute;s &agrave; 1700 caract&egrave;res 
  !</p>
                          <p align="center"><strong> NE PAS RAPPELER LE TITRE 
                            ET VOTRE NOM DANS LE CORPS DU TEXTE!</strong></p>
<form action="spip.php?page=article_envoyer" method="post">
<div align="center">
  <table border="0">
    <tr> 
        <td width="0">&nbsp;</td>
      <td width="448"> <input name="mail" type="text" value="<? echo $auteur_session[\'nom\']; ?>" size="30">
          <tt><em>Nom</em></tt> </td>
    </tr>
    <tr> 
        <td>&nbsp;</td>
      <td> <input name="mail" type="text" value="<? echo $auteur_session[\'email\']; ?>" size="30">
          <tt><em>Adresse courriel</em></tt></td>
    </tr>
    <tr> 
        <td>&nbsp; </td>
          <td> <select name="rubrique">
              <option value="">---Choisissez la rubrique---</option>
              ' .
BOUCLE_rubriqueshtml_73c66783ad14b9dec61f659e6e2dc04a($Cache, $Pile, $doublons, $Numrows, $SP) .
'
            </select>
            <br> <font color="red" size="1">N\'oubliez pas de 
        choisir une rubrique !</font> </td>
    </tr>
    <tr> 
        <td>&nbsp;</td>
      <td><input type="text" name="titre" size="60">
          <em><tt>Titre</tt></em> </td>
    </tr>
    <tr> 
        <td>&nbsp;</td>
      <td><input type="text" name="soustitre" size="50">
          <tt><em>Sous-titre </em></tt></td>
    </tr>
    <tr> 
        <td>&nbsp; </td>
      <td><textarea name="chapo" cols="50" rows="3"></textarea>
                                  <em><tt>Intro</tt></em> <em></em></td>
    </tr>
    <tr> 
        <td>&nbsp;</td>
        <td><textarea name="texte" cols="55" rows="30" onkeypress="compter(this.form)"></textarea><br />Nombre de caractères : <INPUT type="text" name="nbcar" size=3></td>
    </tr>
  </table>
</div>
 <input type="button" value="Envoyer" onClick="verifForm(this.form)"> 
    </form>
 
  (Gardez une copie de vos textes, relancer en cas de non publication, merci !) 
  <br>
                        <script language="JavaScript"> 
function verifForm(formulaire) 
{ 
if(formulaire.rubrique.value == "") 
{
alert(\'Désolé mais vous devez choisir une rubrique!\'); 
}
else
{
formulaire.submit(); 
}
}
</script>
</body>
</html>
');

	return analyse_resultat_skel('html_73c66783ad14b9dec61f659e6e2dc04a', $Cache, $page, 'squelettes/article_proposer.html');
}
?>