<?php

/*
 * Squelette : ../plugins-dist/breves/prive/squelettes/navigation/breve.html
 * Date :      Wed, 26 Feb 2020 10:55:57 GMT
 * Compile :   Mon, 09 Mar 2020 18:39:17 GMT
 * Boucles :   _nav
 */ 

function BOUCLE_navhtml_2840735b5fcb2d80dcf0b8ff34a096da(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	$in = array();
	if (!(is_array($a = (@$Pile[0]['statut']))))
		$in[]= $a;
	else $in = array_merge($in, $a);
	if (!isset($si_init)) { $command['si'] = array(); $si_init = true; }
	$command['si'][] = interdire_scripts((entites_html(table_valeur(@$Pile[0], (string)'exec', null),true) == 'breve'));

	if (!isset($command['table'])) {
		$command['table'] = 'breves';
		$command['id'] = '_nav';
		$command['from'] = array('breves' => 'spip_breves');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("breves.id_breve",
		"breves.lang",
		"breves.titre");
		$command['orderby'] = array();
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array(
			array('=', 'breves.id_breve', sql_quote(@$Pile[0]['id_breve'], '','bigint(21) NOT NULL AUTO_INCREMENT')), (!(is_array(@$Pile[0]['statut'])?count(@$Pile[0]['statut']):strlen(@$Pile[0]['statut'])) ? '' : ((is_array(@$Pile[0]['statut'])) ? sql_in('breves.statut',sql_quote($in)) : 
			array('=', 'breves.statut', sql_quote(@$Pile[0]['statut'], '','varchar(6) NOT NULL DEFAULT \'0\'')))));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('../plugins-dist/breves/prive/squelettes/navigation/breve.html','html_2840735b5fcb2d80dcf0b8ff34a096da','_nav',1,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	lang_select($GLOBALS['spip_lang']);
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		lang_select_public($Pile[$SP]['lang'], '', $Pile[$SP]['titre']);
		$t0 .= (
'
' .
boite_ouvrir('', 'info') .
pipeline( 'boite_infos' , array('data' => '', 'args' => array('type' => 'breve', 'id' => interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'id_breve', null),true)))) ) .
boite_fermer() .
'

<div class="ajax">
' .
executer_balise_dynamique('FORMULAIRE_EDITER_LOGO',
	array('breve',$Pile[$SP]['id_breve'],'',@serialize($Pile[0])),
	array('../plugins-dist/breves/prive/squelettes/navigation/breve.html','html_2840735b5fcb2d80dcf0b8ff34a096da','_nav',5,$GLOBALS['spip_lang'])) .
'</div>

' .
pipeline( 'afficher_config_objet' , array('args' => array('type' => 'breve', 'id' => $Pile[$SP]['id_breve']), 'data' => '') ));
		lang_select();
	}
	lang_select();
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_nav @ ../plugins-dist/breves/prive/squelettes/navigation/breve.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}

//
// Fonction principale du squelette ../plugins-dist/breves/prive/squelettes/navigation/breve.html
// Temps de compilation total: 25.196 ms
//

function html_2840735b5fcb2d80dcf0b8ff34a096da($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (($t1 = BOUCLE_navhtml_2840735b5fcb2d80dcf0b8ff34a096da($Cache, $Pile, $doublons, $Numrows, $SP))!=='' ?
		$t1 :
		((	'
' .
	(($t2 = strval(interdire_scripts((((entites_html(table_valeur(@$Pile[0], (string)'exec', null),true) == 'breve_edit')) ?' ' :''))))!=='' ?
			($t2 . 
'<'.'?php echo recuperer_fond( ' . argumenter_squelette('prive/squelettes/navigation/breve_edit') . ', array_merge('.var_export($Pile[0],1).',array(\'lang\' => ' . argumenter_squelette($GLOBALS["spip_lang"]) . ')), array("compil"=>array(\'../plugins-dist/breves/prive/squelettes/navigation/breve.html\',\'html_2840735b5fcb2d80dcf0b8ff34a096da\',\'\',12,$GLOBALS[\'spip_lang\'])), _request("connect"));
?'.'>') :
			'') .
	'
')));

	return analyse_resultat_skel('html_2840735b5fcb2d80dcf0b8ff34a096da', $Cache, $page, '../plugins-dist/breves/prive/squelettes/navigation/breve.html');
}
?>