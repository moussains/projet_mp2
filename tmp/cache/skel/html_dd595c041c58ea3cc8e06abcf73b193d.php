<?php

/*
 * Squelette : plugins/auto/scssphp/v2.2.0/prive/bouton/calculer_css.html
 * Date :      Thu, 16 Apr 2020 16:56:42 GMT
 * Compile :   Thu, 16 Apr 2020 20:44:01 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette plugins/auto/scssphp/v2.2.0/prive/bouton/calculer_css.html
// Temps de compilation total: 0.083 ms
//

function html_dd595c041c58ea3cc8e06abcf73b193d($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'<'.'?php header("X-Spip-Cache: 0"); ?'.'>'.'<'.'?php header("Cache-Control: no-cache, must-revalidate"); ?'.'><'.'?php header("Pragma: no-cache"); ?'.'>' .
(($t1 = strval(interdire_scripts(((filtre_info_plugin_dist("minibando", "est_actif")) ?'' :' '))))!=='' ?
		($t1 . (	'
	' .
	(($t2 = strval(parametre_url(self(),'var_mode','css')))!=='' ?
			('<a href="' . $t2 . (	'" class="spip-admin-boutons" id="scssphp_calculer_css">' .
		_T('scssphp:bouton_calculer_css') .
		'</a>')) :
			'') .
	'
')) :
		'') .
'
');

	return analyse_resultat_skel('html_dd595c041c58ea3cc8e06abcf73b193d', $Cache, $page, 'plugins/auto/scssphp/v2.2.0/prive/bouton/calculer_css.html');
}
?>