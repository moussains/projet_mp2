<?php

/*
 * Squelette : plugins/auto/player/v3.1.0/players/mejs/player.html
 * Date :      Fri, 20 Dec 2019 12:26:28 GMT
 * Compile :   Sun, 09 Feb 2020 20:43:13 GMT
 * Boucles :   _doc, _feat
 */ 

function BOUCLE_dochtml_96e781dedcd5651cd666217359587d71(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($command['table'])) {
		$command['table'] = 'documents';
		$command['id'] = '_doc';
		$command['from'] = array('documents' => 'spip_documents');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("documents.id_document",
		"documents.largeur",
		"documents.duree");
		$command['orderby'] = array();
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array(
			array('(documents.taille > 0 OR documents.distant=\'oui\')'), 
			array('=', 'documents.id_document', sql_quote(@$Pile[0]['id_document'], '','bigint(21) NOT NULL AUTO_INCREMENT')));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('plugins/auto/player/v3.1.0/players/mejs/player.html','html_96e781dedcd5651cd666217359587d71','_doc',2,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$t0 .= (
vide($Pile['vars'][$_zzz=(string)'url'] = vider_url(urlencode_1738(generer_url_entite($Pile[$SP]['id_document'], 'document', '', '', true)))) .
vide($Pile['vars'][$_zzz=(string)'id_document'] = $Pile[$SP]['id_document']) .
vide($Pile['vars'][$_zzz=(string)'largeur'] = (	interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'largeur', null), interdire_scripts($Pile[$SP]['largeur'])),true)) .
	vide($Pile['vars'][$_zzz=(string)'duree'] = interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'duree', null), interdire_scripts($Pile[$SP]['duree'])),true))))));
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_doc @ plugins/auto/player/v3.1.0/players/mejs/player.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}


function BOUCLE_feathtml_96e781dedcd5651cd666217359587d71(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	$command['source'] = array(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'features', null),true)));
	$command['sourcemode'] = 'table';
	if (!isset($command['table'])) {
		$command['table'] = '';
		$command['id'] = '_feat';
		$command['from'] = array();
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array(".valeur");
		$command['orderby'] = array();
		$command['where'] = 
			array();
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"DATA",
		$command,
		array('plugins/auto/player/v3.1.0/players/mejs/player.html','html_96e781dedcd5651cd666217359587d71','_feat',11,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$t1 = (
'"' .
interdire_scripts($Pile[$SP]['valeur']) .
'"');
		$t0 .= ((strlen($t1) && strlen($t0)) ? ',' : '') . $t1;
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_feat @ plugins/auto/player/v3.1.0/players/mejs/player.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}

//
// Fonction principale du squelette plugins/auto/player/v3.1.0/players/mejs/player.html
// Temps de compilation total: 26.436 ms
//

function html_96e781dedcd5651cd666217359587d71($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
vide($Pile['vars'][$_zzz=(string)'url'] = interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'url_document', null), ''),true))) .
vide($Pile['vars'][$_zzz=(string)'id_document'] = '') .
vide($Pile['vars'][$_zzz=(string)'largeur'] = '0') .
vide($Pile['vars'][$_zzz=(string)'duree'] = interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'duree', null), ''),true))) .
BOUCLE_dochtml_96e781dedcd5651cd666217359587d71($Cache, $Pile, $doublons, $Numrows, $SP) .
'
' .
vide($Pile['vars'][$_zzz=(string)'duree'] = (intval(table_valeur($Pile["vars"], (string)'duree', null)) ? intval(table_valeur($Pile["vars"], (string)'duree', null)):'')) .
'
' .
vide($Pile['vars'][$_zzz=(string)'largeur'] = interdire_scripts(max(entites_html(sinon(table_valeur(@$Pile[0], (string)'largeur', null), (table_valeur($Pile["vars"], (string)'largeur', null) ? table_valeur($Pile["vars"], (string)'largeur', null):'400')),true),'120'))) .
'<dt class=\'spip_doc_titre player ' .
interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'player', null),true)) .
'\'>
	<div class="mejs-audio-wrapper audio-wrapper"' .
(($t1 = strval(table_valeur($Pile["vars"], (string)'largeur', null)))!=='' ?
		(' style=\'width:' . $t1 . 'px;max-width:100%;margin:0 auto;\'') :
		'') .
'>
		<audio class="mejs ' .
(($t1 = strval(table_valeur($Pile["vars"], (string)'id_document', null)))!=='' ?
		(' mejs-' . $t1) :
		'') .
(($t1 = strval(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'skin', null),true))))!=='' ?
		(' mejs__' . $t1) :
		'') .
(($t1 = strval(interdire_scripts(((entites_html(table_valeur(@$Pile[0], (string)'playlist', null),true)) ?' ' :''))))!=='' ?
		($t1 . 'mejs__playlist') :
		'') .
'"
		       data-id="' .
md5(concat((	'mejs-' .
	table_valeur($Pile["vars"], (string)'id_document', null)),'-',table_valeur($Pile["vars"], (string)'url', null),'-',interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'skin', null),true)))) .
'"
		       src="' .
table_valeur($Pile["vars"], (string)'url', null) .
'"
		       type="' .
interdire_scripts(@$Pile[0]['mime_type']) .
'"
		       data-mejsoptions=\'{"alwaysShowControls": true' .
(($t1 = strval(interdire_scripts((entites_html(table_valeur(@$Pile[0], (string)'loop', null),true) ? 'true':'false'))))!=='' ?
		(',"loop":' . $t1) :
		'') .
',"audioWidth":"100%"' .
(($t1 = strval(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'hauteur', null),true))))!=='' ?
		(',"audioHeight":"' . $t1 . '"') :
		'') .
(($t1 = strval(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'volume', null),true))))!=='' ?
		(',"startVolume":"' . $t1 . '"') :
		'') .
(($t1 = strval(table_valeur($Pile["vars"], (string)'duree', null)))!=='' ?
		(',"duration":' . $t1) :
		'') .
(($t1 = strval(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'playlist', null),true))))!=='' ?
		(',"playlistSelector":"' . $t1 . '"') :
		'') .
(($t1 = BOUCLE_feathtml_96e781dedcd5651cd666217359587d71($Cache, $Pile, $doublons, $Numrows, $SP))!=='' ?
		(',"features":[' . $t1 . ']') :
		'') .
'}\'
		       ' .
(($t1 = strval(interdire_scripts(json_encode(entites_html(table_valeur(@$Pile[0], (string)'plugins', null),true)))))!=='' ?
		('data-mejsplugins=\'' . $t1 . '\'') :
		'') .
'
		       controls="controls"
					 ' .
(($t1 = strval(interdire_scripts(((entites_html(table_valeur(@$Pile[0], (string)'autoplay', null),true)) ?' ' :''))))!=='' ?
		('autoplay="autoplay"' . $t1) :
		'') .
'></audio>
	</div><span style="display: none;">
	' .
(($t1 = strval(charge_scripts('javascript/mejs-init.min.js',false)))!=='' ?
		((	'<script>/*<![CDATA[*/var mejspath=\'' .
	timestamp(find_in_path('lib/mejs/mediaelement-and-player.min.js')) .
	'\',mejscss=\'' .
	timestamp(find_in_path('lib/mejs/mediaelementplayer.min.css')) .
	'\';
	') . $t1 . '/*]]>*/</script>') :
		'') .
'
	' .
(($t1 = strval(interdire_scripts(((entites_html(table_valeur(@$Pile[0], (string)'skin', null),true)) ?' ' :''))))!=='' ?
		($t1 . (($t2 = strval(charge_scripts(url_absolue_css(((($a = find_in_path((	'css/mejs-skin-' .
			interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'skin', null),true)) .
			'.css'))) OR (is_string($a) AND strlen($a))) ? $a : find_in_path('lib/mejs/mejs-skins.css'))),false)))!=='' ?
			('<style>' . $t2 . '</style>') :
			'')) :
		'') .
'</span>
</dt>

');

	return analyse_resultat_skel('html_96e781dedcd5651cd666217359587d71', $Cache, $page, 'plugins/auto/player/v3.1.0/players/mejs/player.html');
}
?>