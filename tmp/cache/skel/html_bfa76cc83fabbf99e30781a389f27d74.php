<?php

/*
 * Squelette : ../plugins-dist/mots/prive/squelettes/hierarchie/groupe_mots_edit.html
 * Date :      Tue, 21 Jan 2020 17:16:51 GMT
 * Compile :   Fri, 31 Jan 2020 15:11:33 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette ../plugins-dist/mots/prive/squelettes/hierarchie/groupe_mots_edit.html
// Temps de compilation total: 0.499 ms
//

function html_bfa76cc83fabbf99e30781a389f27d74($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = 
'<'.'?php echo recuperer_fond( ' . argumenter_squelette('prive/squelettes/hierarchie/groupe_mots') . ', array_merge('.var_export($Pile[0],1).',array(\'lang\' => ' . argumenter_squelette($GLOBALS["spip_lang"]) . ')), array("compil"=>array(\'../plugins-dist/mots/prive/squelettes/hierarchie/groupe_mots_edit.html\',\'html_bfa76cc83fabbf99e30781a389f27d74\',\'\',1,$GLOBALS[\'spip_lang\'])), _request("connect"));
?'.'>';

	return analyse_resultat_skel('html_bfa76cc83fabbf99e30781a389f27d74', $Cache, $page, '../plugins-dist/mots/prive/squelettes/hierarchie/groupe_mots_edit.html');
}
?>