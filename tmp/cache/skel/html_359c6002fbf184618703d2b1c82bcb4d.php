<?php

/*
 * Squelette : plugins/auto/menus_1/menus/page_speciale.html
 * Date :      Fri, 20 Dec 2019 12:26:26 GMT
 * Compile :   Wed, 22 Jan 2020 13:55:51 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette plugins/auto/menus_1/menus/page_speciale.html
// Temps de compilation total: 3.164 ms
//

function html_359c6002fbf184618703d2b1c82bcb4d($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
vide($Pile['vars'][$_zzz=(string)'titre'] = interdire_scripts(typo(table_valeur(@$Pile[0], (string)'titre', null)))) .
'
' .
vide($Pile['vars'][$_zzz=(string)'afficher_entree'] = 'oui') .
'
' .
(($t1 = strval(interdire_scripts(((entites_html(table_valeur(@$Pile[0], (string)'suivant_connexion', null),true)) ?' ' :''))))!=='' ?
		($t1 . (	'
	' .
	(($t2 = strval(interdire_scripts(((((entites_html(table_valeur(@$Pile[0], (string)'suivant_connexion', null),true) == 'connecte')) AND (interdire_scripts(invalideur_session($Cache, ((table_valeur($GLOBALS["visiteur_session"], (string)'id_auteur', null)) ?'' :' '))))) ?' ' :''))))!=='' ?
			($t2 . (	'
		' .
		vide($Pile['vars'][$_zzz=(string)'afficher_entree'] = ''))) :
			'') .
	'
	' .
	(($t2 = strval(interdire_scripts(((((entites_html(table_valeur(@$Pile[0], (string)'suivant_connexion', null),true) == 'deconnecte')) AND (interdire_scripts(invalideur_session($Cache, ((table_valeur($GLOBALS["visiteur_session"], (string)'id_auteur', null)) ?' ' :''))))) ?' ' :''))))!=='' ?
			($t2 . (	'
		' .
		vide($Pile['vars'][$_zzz=(string)'afficher_entree'] = ''))) :
			'') .
	'
	' .
	(($t2 = strval(interdire_scripts(((((entites_html(table_valeur(@$Pile[0], (string)'suivant_connexion', null),true) == 'admin')) AND (interdire_scripts(invalideur_session($Cache, (table_valeur($GLOBALS["visiteur_session"], (string)'statut', null) != '0minirezo'))))) ?' ' :''))))!=='' ?
			($t2 . (	'
		' .
		vide($Pile['vars'][$_zzz=(string)'afficher_entree'] = ''))) :
			'') .
	'
')) :
		'') .
'

' .
(($t1 = strval(interdire_scripts(((entites_html(table_valeur(@$Pile[0], (string)'appel_formulaire', null),true)) ?' ' :''))))!=='' ?
		($t1 . (	'
	<div class="titre">' .
	table_valeur($Pile["vars"], (string)'titre', null) .
	'</div>
	<div class="infos">' .
	_T('menus:info_page_speciale', array('page' => interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'nom', null),true)))) .
	(($t2 = strval(interdire_scripts((entites_html(table_valeur(@$Pile[0], (string)'suivant_connexion', null),true) ? _T(concat('menus:entree_suivant_connexion_',interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'suivant_connexion', null),true)))):''))))!=='' ?
			(' (' . $t2 . ')') :
			'') .
	'</div>
')) :
		'') .
'
' .
(($t1 = strval(interdire_scripts((((entites_html(table_valeur(@$Pile[0], (string)'appel_menu', null),true)) AND (table_valeur($Pile["vars"], (string)'afficher_entree', null))) ?' ' :''))))!=='' ?
		($t1 . (	'
	<li class="menu-entree item menu-items__item menu-items__item_page-speciale' .
	(($t2 = strval(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'nom', null),true))))!=='' ?
			(' ' . $t2) :
			'') .
	(($t2 = strval(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'css', null),true))))!=='' ?
			(' ' . $t2) :
			'') .
	(($t2 = strval(interdire_scripts(((((entites_html(table_valeur(@$Pile[0], (string)'env/type-page', null),true) == interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'nom', null),true)))) OR (interdire_scripts(((((entites_html(table_valeur(@$Pile[0], (string)'env/type', null),true) == 'page')) AND (interdire_scripts((entites_html(table_valeur(@$Pile[0], (string)'env/composition', null),true) == interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'nom', null),true)))))) ?' ' :'')))) ?' ' :''))))!=='' ?
			($t2 . ' on active') :
			'') .
	'">
		<a href="' .
	interdire_scripts(ancre_url(generer_url_public(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'nom', null),true)), interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'parametres', null),true))),interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'ancre', null),true)))) .
	'" class="menu-items__lien' .
	(($t2 = strval(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'css_lien', null),true))))!=='' ?
			(' ' . $t2) :
			'') .
	'">' .
	table_valeur($Pile["vars"], (string)'titre', null) .
	'</a>
')) :
		'') .
'
');

	return analyse_resultat_skel('html_359c6002fbf184618703d2b1c82bcb4d', $Cache, $page, 'plugins/auto/menus_1/menus/page_speciale.html');
}
?>